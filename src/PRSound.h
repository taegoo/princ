/*************************************************************************
 *  PRSound.h
 *
 *  Taegoo : Migration from praat/fon/Sound.h
 *
 **************************************************************************/
#ifndef __PRSOUND_H__
#define __PRSOUND_H__

#include "PRVector.h"		// Use memory
#include "PRSndManager.h"	// Use files

/* Levels for Sampled_getValueAtSample (me, index, level, unit) */
#define Sound_LEVEL_MONO  0
#define Sound_LEVEL_LEFT  1
#define Sound_LEVEL_RIGHT  2

/* Values for `phase' parameter: */
#define Sound_TONE_COMPLEX_SINE  0
#define Sound_TONE_COMPLEX_COSINE  1

class PRSound : public PRVector 
{
public:
	PRSound();
	~PRSound();

	static void appendSoundToSound(PRSound *src, PRSound *dest);

	void init(long numberOfChannels, double xmin, double xmax, long nx, double dx, double x1);

	PRSndManager *m_blockManager;

	// Wrapper to m_z[]
	double getZ(long ch, long idx);
	void releaseMemoryToFile(char *rootDir);

	void removeTail(double lenSec);

	/*
	virtual void init(double xmin, double xmax, long nx, double dx, double x1,
							double ymin, double ymax, long ny, double dy, double y1);
	virtual long xToLowIndex (double x);
	virtual long xToHighIndex(double x);
	//PRVector:
	virtual double getAbsoluteExtremum(double xmin, double xmax, int interpolation);
	virtual long xToNearestIndex(double x);
	virtual void subtractMean(); // class PRVector
	virtual int shortTermAnalysis(double windowDuration, double timeStep, long *numberOfFrames, double *firstTime);
	*/

/*
	Function:
		return a new silent Sound, or NULL if out of memory.
	Preconditions:
		xmax > xmin;
		nx >= 1;
		dx > 0.0;
	Postconditions:
		thy xmin == xmin;
		thy xmax == xmax;
		thy nx == nx;
		thy dx == dx;
		thy x1 == x1;
		thy ymin = 1.0;
		thy ymax = numberOfChannels;
		thy ny = numberOfChannels;
		thy dx = 1.0;
		thy x1 = 1.0;
		thy z [i] [1..nx] == 0.0;
*/

	/*
	double m_xmin;		// Start time (sec)
	double m_xmax;		// End time (sec)
	long m_nx;		// Number of samples
	double m_dx;		// Sampling period (sec)
	double m_x1;		// Time of the first sample (sec)
	double m_ymin;		// (==1) Left or only channel	
	double m_ymax;		// Right or only channel
	long m_ny;		// Number of channels
	double m_dy; double m_y1;		// (==1)  y is channel number (1=left, 2=right)
	double **m_z;		// Amplitude
	*/

	// DEPRECATED
	//PRSound* convertToMono();
	//PRSound* duplicate();
	PRSound* copyPart(double tmin, double tmax);

#if 0
	PRSound* convertToStereo();

private:

	PRSound* extractChannel(long channel);
	PRSound* extractLeftChannel();
	PRSound* extractRightChannel();
	PRSound* combineToStereo(PRSound* thee);

	PRSound* upsample();   /* By a factor 2. */
	PRSound* resample(double samplingFrequency, long precision);
	/*
		Method:
			precision <= 1: linear interpolation.
			precision >= 2: sinx/x interpolation with maximum depth equal to 'precision'.
	*/

	PRSound* append(double silenceDuration, PRSound* thee);
	/*
		Function:
			append two Sounds.
		Failure:
			my dx != thy dx: "Sampling frequencies do not match."
		Postconditions:
			result -> xmin == 0;
			result -> xmax == result -> nx * my dx;
			result -> nx == my nx + thy nx + round (silenceDuration / my dx);
			result -> dx == my dx;
			result -> x1 == 0.5 * my dx;
			for (i = 1..my nx)
				result -> z [1] [i] == my z [1] [i]
			for (i = 1..thy nx)
			result -> z [1] [i + my nx + round (silenceDuration / my dx)] == thy z [1] [i]
	*/
 
	PRSound* convolve(PRSound* thee);
	/*
		Function:
			convolve two Sounds.
		Failure:
			my dx != thy dx: "Sampling frequencies do not match."
		Postconditions:
			result -> xmin == my xmin + thy xmin;
			result -> xmax == my xmax + thy xmax;
			result -> nx == my nx + thy nx - 1;
			result -> dx == my dx;
			result -> x1 == my x1 + thy x1;
			for (i = 1..result -> nx)
				result -> z [1] [i] == result -> dx *
					sum (j = 1..i, my z [1] [j] * thy z [1] [i - j + 1])
	*/
	PRSound* crossCorrelate(PRSound* thee, double tmin, double tmax, int normalize);

	double getRootMeanSquare(double xmin, double xmax);
	double getEnergy(double xmin, double xmax);
	double getPower(double xmin, double xmax);
	double getEnergyInAir();
	double getPowerInAir();
	double getIntensity_dB();

	double getNearestZeroCrossing(double position, long channel);
	void setZero(double tmin, double tmax, int roundTimesToNearestZeroCrossing);

	PRSound* createFromToneComplex(double startingTime, double endTime,
							double sampleRate, int phase, double frequencyStep,
							double firstFrequency, double ceiling, long numberOfComponents);
	void multiplyByWindow(enum kSound_windowShape windowShape);
	void scaleIntensity(double newAverageIntensity);
	void overrideSamplingFrequency(double newSamplingFrequency);
	PRSound* extractPart(double t1, double t2, enum kSound_windowShape windowShape, 
								double relativeWidth, bool preserveTimes);
	int filterWithFormants(double tmin, double tmax,
							int numberOfFormants, double *formant, double *bandwidth);
							//int numberOfFormants, double formant [], double bandwidth []);
	PRSound* filter_oneFormant(double frequency, double bandwidth);
	int filterWithOneFormantInline(double frequency, double bandwidth);
	PRSound* filter_preemphasis(double frequency);
	PRSound* filter_deemphasis(double frequency);

	void reverse(double tmin, double tmax);

	// Data_copy()
	//PRSound* copy();

int Sound_playPart (Sound me, double tmin, double tmax,
	int (*playCallback) (void *playClosure, int phase, double tmin, double tmax, double t), void *playClosure);
/*
 * Play a sound. The playing can be interrupted with the Escape key (also Command-period on the Mac).
 * If playCallback is not NULL, Sound_play will call it repeatedly, with five parameters:
 *    1. playClosure: the same value as was supplied as the last argument to Sound_playPart.
 *    2. phase: 1 at the start, 2 while playing, 3 at the end.
 *    3. tmin: the same tmin that was supplied as the second argument to Sound_playPart.
 *    4. tmax: the same tmax that was supplied as the second argument to Sound_playPart.
 *    5. t: the time (probably between tmin and tmax) at which the sound is playing.
 * The usage of playCallback is as follows. Suppose we are an editor that wants to show a moving
 * cursor while we play a sound:
 *    Sound_playPart (my sound, my startSelection, my endSelection, thePlayCallback, me);
 * We gave ourselves as the playClosure, so that the message will arrive with us:
 *    int thePlayCallback (void *playClosure, int phase, double tmin, double tmax, double t) {
 *       SoundEditor me = (SoundEditor) playClosure;
 *       if (phase == 1) {
 *          Melder_assert (t == tmin);
 *          drawPlayCursor (me, my playCursor = t);
 *       } else if (phase == 2) {
 *          undrawPlayCursor (me, my playCursor);
 *          drawPlayCursor (me, my playCursor = t);
 *       } else {
 *          undrawPlayCursor (me, t);
 *          moveCursor (me, t);
 *          if (t < tmax) { Melder_casual ("Sound play interrupted."); }
 *       }
 *       return 1;
 *    }
 * The playCallback procedure usually returns 1, because the sound should continue playing.
 * If playCallback returns 0 instead, Sound_play will interrupt the play and return.
 * Sound_playPart returns the time at which playing stopped. If playing was not interrupted, it returns tmax.
 *
 * Sound_playPart () usually runs asynchronously, and kills an already playing sound.
 */
int Sound_play (Sound me,
	int (*playCallback) (void *playClosure, int phase, double tmin, double tmax, double t), void *playClosure);
	/* The same as Sound_playPart (me, my xmin, my xmax, playCallback, playClosure); */
#endif
};

#endif
