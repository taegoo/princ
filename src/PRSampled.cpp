/*************************************************************************
 *  PRSampled.cpp
 *
 *  Taegoo : Migration from praat/fon/Sampled.c
 *
 *		- Parent class of 'PRSound' and 'PRPitch'
 *
 **************************************************************************/
#include "PRSampled.h"
#include "NUM.h"

PRSampled::PRSampled()
{
}

PRSampled::~PRSampled()
{
}

void PRSampled::initSampled(double xmin, double xmax, long nx, double dx, double x1) 
{
	/*
	_pincmsg1("PRSampled::init");
	_pincmsg6("    xmin=%f, xmax=%f, nx=%d, dx=%f, x1=%f", xmin, xmax, nx, dx, x1);
	*/

	m_nx = nx; 
	m_dx = dx; 
	m_x1 = x1;

	m_xmin = xmin; 
	m_xmax = xmax; 
}

double PRSampled::getNx() 
{ 
	return m_nx; 
}

double PRSampled::getDx() 
{ 
	return m_dx; 
}

double PRSampled::getX(long ix) 
{ 
	return m_x1 + (ix - 1) * m_dx; 
}

// Will be replaced by children classes, if needed 
// For PRSound, simply use this which will always return 'NUMundefined'
/*
double PRSampled::getValueAtSample (long isamp, long ilevel, int unit) 
{
	(void) isamp;
	(void) ilevel;
	(void) unit;
	return NUMundefined;
}
*/

double PRSampled::indexToX(long i)
{
	//_pincmsg4("    ---- indexToX: i=%d, m_x1=%f, m_dx=%f", i, m_x1, m_dx);
	return m_x1 + (i - 1) * m_dx;
}

double PRSampled::xToIndex (double x)
{
	return (x - m_x1) / m_dx + 1;
}

long PRSampled::xToLowIndex (double x)
{
	//_pincmsg4("    ---- xToLowIndex: x=%f, m_x1=%f, m_dx=%f", x, m_x1, m_dx);
	return (long) floor((x - m_x1) / m_dx) + 1;
}

long PRSampled::xToHighIndex(double x)
{
	return (long)ceil((x - m_x1) / m_dx) + 1;
}

long PRSampled::xToNearestIndex(double x)
{
	return (long) floor((x - m_x1) / m_dx + 1.5);
}


int PRSampled::shortTermAnalysis(double windowDuration, 
											double timeStep, 
											long *numberOfFrames, 
											double *firstTime)
{
	double myDuration, thyDuration, ourMidTime;
	/*
	Melder_assert (windowDuration > 0.0);
	Melder_assert (timeStep > 0.0);
	*/
	myDuration = m_dx * m_nx;
	/*
	if (windowDuration > myDuration) {
		printf("[ERROR] shortTermAnalysis: windowDuration=%f, myDuration=%f\n",
								windowDuration, myDuration);
		//return Melder_error2 (Thing_className (me), L" shorter than window length."); 
		//_pincmsg1("[ERROR] shortTermAnalysis:  shorter than window length"); 
		return 0;
	}
	*/
	if (windowDuration > myDuration) {
		// Don't return
		printf("[WARNING] shortTermAnalysis: windowDuration=%f, myDuration=%f\n",
								windowDuration, myDuration);
		windowDuration = myDuration;
	}
	*numberOfFrames = floor((myDuration - windowDuration) / timeStep) + 1;
	//Melder_assert (*numberOfFrames >= 1);
	ourMidTime = m_x1 - 0.5 * m_dx + 0.5 * myDuration;
	thyDuration = *numberOfFrames * timeStep;
	//_pincmsg5("    ---- shortTermAnalysis: m_x1=%f, m_dx=%f, myDuration=%f, ourMidTime=%f", m_x1, m_dx, myDuration, ourMidTime);
	*firstTime = ourMidTime - 0.5 * thyDuration + 0.5 * timeStep;
	return 1;
}

long PRSampled::getWindowSamples(double xmin, double xmax, long *ixmin, long *ixmax)
{
	double rixmin = 1.0 + ceil((xmin - m_x1) / m_dx);
	double rixmax = 1.0 + floor((xmax - m_x1) / m_dx);
	*ixmin = rixmin < 1.0 ? 1 : (long) rixmin;
	*ixmax = rixmax > (double) m_nx ? m_nx : (long) rixmax;
	if (*ixmin > *ixmax) 
		return 0;
	return *ixmax - *ixmin + 1;
}

// TODO:?
double PRSampled::getValueAtSample(long isamp, long ilevel, int unit)
{
	return 0.0;
}
// TODO?
double PRSampled::getValueAtX(double x, long ilevel, int unit, int interpolate)
{
	return 0.0;
}
#if 0
double PRSampled::getValueAtSample(long isamp, long ilevel, int unit)
{
	return 0.0;
}


long PRSampled::countDefinedSamples(long ilevel, int unit)
{
	return 0;
}

double* PRSampled::getSortedValues (long ilevel, int unit, long *numberOfValues)
{
	return NULL;
}

double PRSampled::getQuantile(double xmin, double xmax, double quantile, long ilevel, int unit)
{
	return 0.0;
}

double PRSampled::getMean(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

double PRSampled::getMean_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate)
{
	return 0.0;
}

double PRSampled::getIntegral(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

double PRSampled::getIntegral_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate)
{
	return 0.0;
}

double PRSampled::getStandardDeviation(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

double PRSampled::getStandardDeviation_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate)
{
	return 0.0;
}

void PRSampled::getMinimumAndX(double xmin, double xmax, long ilevel, int unit, int interpolate,
							double* return_minimum, double *return_xOfMinimum)
{
}

double PRSampled::getMinimum(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

double PRSampled::getXOfMinimum(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

void PRSampled::getMaximumAndX(double xmin, double xmax, long ilevel, int unit, int interpolate,
						double *return_maximum, double *return_xOfMaximum)
{
}

double PRSampled::getMaximum(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}

double PRSampled::getXOfMaximum(double xmin, double xmax, long ilevel, int unit, int interpolate)
{
	return 0.0;
}
#endif


