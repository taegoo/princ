#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PRSndBlock.h"

#define FILE_PATH_LEN	200

PRSndBlock::PRSndBlock(double *buf, long len, char *path, long pos)
{
	m_pos = pos;
	m_len = len;
	m_buffer = NULL;	// Must NULL
	m_file = NULL;	// Must NULL

	createFile(buf, len, path);
}

PRSndBlock::~PRSndBlock()
{
	printf("~PRSndBlock\n");
	if (m_buffer)
		free(m_buffer);
	if (m_file) {
		if (remove(m_file) != 0)
			printf("[WARNING] Couldn't delete m_file\n");
		free(m_file);
	}
}

// private
void PRSndBlock::createFile(double *buf, long len, char *path)
{
	FILE *pFile = NULL;

	// Delete the file, if any;
	remove(path);

	// Open
	pFile = fopen(path, "wb");

	if (pFile) {
		// Write
		//fwrite((char*)buf, 1, len*sizeof(double), pFile);
		fwrite((char*)buf, sizeof(double), len, pFile);
		fflush(pFile);	// need it?

		// DEBUG:
		/*
		fseek(pFile, 0, SEEK_END);
		size_t _size = ftell(pFile);
		printf ("  _size=%ld, len(bytes)=%ld \n",_size, len*sizeof(double));
		*/

		// Close the file
		fclose(pFile);

		// Update member
		m_len = len;
		m_file = (char*)malloc(FILE_PATH_LEN);
		strcpy(m_file, path);
	} else {
		printf("[ERROR] PRSndBlock::createBuffer - can't open file\n");
		m_len = 0;
		m_file = NULL;
	}
}

double PRSndBlock::getSample(long at)
{
	double res;
	if (at >= m_len) {
		printf("[ERROR] PRSndBlock::getSample() at=%ld, m_len=%ld\n", at, m_len);
		return 0.0;
	}
	// Add samples to memory
	if (!m_buffer) {
		//printf("PRSndBlock::getSample() - creating memory buffer\n");
		if (!m_file) {
			printf("[ERROR] No m_file\n");
			return 0.0;
		}
		FILE *pFile = NULL;
		// Open
		pFile = fopen(m_file, "rb");
		if (pFile) {
			// Read all content of file
			m_buffer = (double*)malloc(m_len*sizeof(double));

			long numRead = fread(m_buffer, sizeof(double), m_len, pFile);

			if (numRead > 0) {
				if (numRead != m_len) {
					printf("[WARNING] m_len=%ld, numRead=%ld\n", m_len, numRead);
				}
				//memcpy(buf, m_buffer, numRead*sizeof(double));

				// double check
				m_len = numRead;
			} else {
				printf("[ERROR] PRSndBlock::getSample() - Empty output\n");
				return 0.0;
			}
		} else {
			printf("[ERROR] PRSndBlock::getSample() - can't open file\n");
			return 0.0;
		}

		// Close the file
		fclose(pFile);
	}

	//printf("PRSndBlock::getSample at=%ld\n", at);

	// Read directly from memory buffer
	if (m_buffer)
		res = m_buffer[at];
	else {
		printf("[ERROR] PRSndBlock::getSample - No m_buffer\n");
		res = 0.0;
	}

	return res;
}

// Releases buffer and copy into file, instead
void PRSndBlock::releaseMemory()
{
	if (m_buffer) {
		free(m_buffer);
		m_buffer = NULL;
	}
}

