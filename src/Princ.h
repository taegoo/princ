/*************************************************************************
 *  Princ.h
 *
 *  Taegoo : The main gateway from Pinc engine to Praat
 *
 **************************************************************************/

#ifndef __PRINC__
#define __PRINC__

#include "PRSound.h"
#include "PRPitch.h"
#include "PRPitchTier.h"
#include "PRManipulation.h"
//#include "ChordNode.h"
//#define MAX_MIDI_NOTE 128


class Princ
{
public:
	Princ(double rate, princ_quality_t quality);
	virtual ~Princ();

	void init(float* inputBuf, 
				long numSamples, 
				long tailLen, 
				char *sndRootDir,
				bool releaseSndMemory,
				double playbackSpeed, // new parameter
				__progress_callback_t callback, float progOffset, float progChunk);

	void init(float* inputBuf, 
				long numSamples, 
				long tailLen, 
				char *sndRootDir,
				bool releaseSndMemory,
				__progress_callback_t callback, float progOffset, float progChunk);

	void addSound(float* inputBuf, long numSamples, char *sndRootDir);

	void initWithAnalyticFiles(
							//float* inputBuf, 
							//long numSamples, 
							char *pathToPulses, 
							char *pathToOrigTier,
							__progress_callback_t callback, float progOffset, float progChunk);

	void changePlaybackSpeed(double speed);
	//bool updateActivePitchTier(char *tierStr, int lenStr);
	//bool updateActivePitchTier(TierNode *nodeHead, bool correctTime);
	bool updateActivePitchTier(TierNode *nodeHead0, TierNode *nodeHead1);

	void addBuffer(float* inputBuf, long numSamples, long tailLen);
	void shortenLengthTo(long numSamples);

	//////////////////////////////////////////////////////////////////////////

	void updatePlan(char *chordList, int dSemitones, int option);
	//float* processBuffer(double startSec, double lenSec, long *retBufLen, int tierIndex, bool getOrig);
	float* processBuffer(double startSec, 
								double lenSec, 
								long *retBufLen, 
								int tierIndex,
								int origDiffSemitones);
	float* initAndProcessBuffer(float* inputBuf, long numSamples,
										long *retLen, float *freq, double paddingT);
	double getFreqAt(double sec);
	double getOrigFreqAt(double sec);
	void origPitchTierToStr(char *outStr, int *numNotes, double minNoteLenSec); //DEPRECATED
	TierNode* getOrigPitchTier();
	//bool saveAnalyticToFiles(char *pathToPulses, char *pathToOrigTier);
	bool saveAnalyticToBuffers(char *bufPulses, char *bufOrigTier);
	char* getPulsesAnalytic();
	char* getOrigTierAnalytic(); // DEPRECATED
	char* getPitchTierBinary(bool origOrActive);

	double getTimeStepSec();
	double getTrackLenSec();

private:
	princ_quality_t m_quality;

	double m_playbackSpeed; // 1.0: original

	PRManipulation* m_manipulation;
	//PRPitch* m_pitchObj;
	//PRPitchTier* m_origPitchTier;
	//PRPitchTier* m_activePitchTier;
	double m_sndEndSec;
	//double m_pitchTimeStep;
	double m_rate;
	//long m_cellLen; // number of frames per cell
	double m_addHeadSec;
	double m_addTailSec;

	/*
	int m_scaleNoteList[MAX_MIDI_NOTE];

	ChordName m_key;
	ChordListNode	*m_chordList;
	*/

	void deinit();
	//void createMainPitchObj();
	/*
	void setupChordList(char* str);
	ChordName getChordNameFromStr(char *str);
	void setupScaleNoteList();
	*/
	double getPitch();
	//PRPitchTier* createOrigPitchTier(double timeStep);
	//void putPitch(double pitch);
	//void putPitchTier(PRPitchTier* tier);

	/*
	int getNearestScaleNoteByFreq(double freq, int dSemitones);
	int getNearestChordNoteByFreq(double freq, ChordName chord, int dSemitones);
	int getHighChordNoteByFreq(double freq, ChordName chord, int dSemitones);
	int getLowChordNoteByFreq(double freq, ChordName chord, int dSemitones);
	bool isChordNote(int note, ChordName chord, bool seventh);

	static double midiToFreq(int midi);
	static int freqToMidi(double f);
	*/
	double getMatchingTimeFromOrigPitchTier(double origTime);


};

#endif
