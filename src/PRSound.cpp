/*************************************************************************
 *  PRSound.cpp
 *
 *  Taegoo : Migration from 
 *					fon/Sound.c 
 *
 **************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "PRSound.h"
#include "NUM.h"

PRSound::PRSound()
{
}

PRSound::~PRSound()
{
	if (m_z)
		NUMdmatrix_free(m_z, 1, 1);
		//NUMdmatrix_free(m_z, m_ny, m_nx); // error!
	m_z = NULL;

	if (m_blockManager)
		delete m_blockManager;
	m_blockManager = NULL;
}

void PRSound::init(long numberOfChannels, double xmin, double xmax, long nx, double dx, double x1)
{
	initVector(xmin, xmax, nx, dx, x1, 1, numberOfChannels, numberOfChannels, 1, 1);
	m_blockManager = NULL;
}

//
// Release m_z and allocate m_blockManager
//
void PRSound::releaseMemoryToFile(char *rootDir)
{
	if (!m_z) {
		printf("[ERROR] PRSound::releaseMemoryToFile - No m_z\n");
		return;
	}

	m_blockManager = new PRSndManager(rootDir);
	m_blockManager->addBuffer(m_z[1]+1, m_nx);	// m_z is base-1

	// Free memory
	NUMdmatrix_free(m_z, 1, 1);
	m_z = NULL;
}

double PRSound::getZ(long ch, long idx)
{
	double val;
	if (m_z)
		val = m_z[ch][idx];
	else if (m_blockManager) {
		// 'idx' is base-1
		// mono onl
		val = m_blockManager->getSample(idx-1);
	} else {
		printf("[ERROR] PRSound::getZ - Not available\n");
	}
	return val;
}

//
// static
// New for v3.0
//
// Assumption: 
//		src has sound on memory
//
void PRSound::appendSoundToSound(PRSound *src, PRSound *dest)
{
	if (!src || !dest)
		return;
	long i;
	long new_nx = dest->m_nx + src->m_nx; 
	double new_xmax = dest->m_xmax + src->m_xmax; 

	// Append m_z
	if (dest->m_z) {
		// dest has sound on memory
		double **new_z = NUMdmatrix(1, dest->m_ny, 1, new_nx);
		//printf("appendSoundToSound: Creating new m_z\n");
		for (i = 1; i <= dest->m_ny; i++) {
			/*
			memcpy((char*)(new_z[i]+1), (char*)(dest->m_z[i]+1), dest->m_nx*sizeof(double));
			memcpy((char*)(new_z[i]+dest->m_nx+1), (char*)(src->m_z[i]+1), src->m_nx*sizeof(double));
			*/
			for (long j = 1; j <= new_nx; j++) {
				if (j <= dest->m_nx)
					new_z[i][j] = dest->m_z[i][j];
				else
					new_z[i][j] = src->m_z[i][j-dest->m_nx];
			} 
		}

		//printf("appendSoundToSound: Freeing old m_z\n");

		// Assign new m_z
		double **tempPtr = dest->m_z;
		dest->m_z = new_z;
		// Free
		NUMdmatrix_free(tempPtr, 1, 1);

	} else if (dest->m_blockManager) {
		// dest has sound on files (m_blockManager)
		dest->m_blockManager->addBuffer(src->m_z[1]+1, src->m_nx); // m_z is base-1
		// 
	} else {
		// Error!!
		printf("[ERROR] PRSound::appendSoundToSound - No dest->m_blockManager\n");
	}

	// Update m_nx , m_xmax
	dest->m_nx = new_nx;
	dest->m_xmax = new_xmax;
}

PRSound* PRSound::copyPart(double tmin, double tmax)
{
	long i, ch, cnt;
	long imin, imax;

	imin = xToHighIndex(tmin);
	imax = xToHighIndex(tmax)-1;

	if (imin < 1) 
		imin = 1;
	if (imax > m_nx) 
		imax = m_nx;
	if (imax < imin) 
		return NULL;

	PRSound* thee = new PRSound();
	thee->init(m_ny, 0, tmax-tmin, imax-imin, m_dx, m_x1);

	for (ch = 1; ch <= m_ny; ch++) {
		cnt = 1;	// 1-based
		for (i = imin; i <= imax; i++) {
			if (cnt > thee->m_nx)
				 break;
			//thee->m_z[ch][cnt++] = m_z[ch][i];
			thee->m_z[ch][cnt++] = getZ(ch, i);
		}
	}
	return thee;
}

void PRSound::removeTail(double lenSec)
{
	long i;

	long numSamples = lenSec / (double)m_dx;

	long new_nx = m_nx - numSamples;
	double new_xmax = m_xmax - lenSec;

	if (m_z) {
		// Append m_z
		double **new_z = NUMdmatrix(1, m_ny, 1, new_nx);
		for (i = 1; i <= m_ny; i++) {
			for (long j = 1; j <= new_nx; j++) {
				new_z[i][j] = m_z[i][j];
			} 
		}

		double **tempPtr = m_z;
		// Assign new m_z
		m_z = new_z;
		// Free
		NUMdmatrix_free(tempPtr, 1, 1);
	} else {
		// Do nothing
	}

	// Update m_nx , m_xmax
	m_nx = new_nx;
	m_xmax = new_xmax;

}

#if 0 // DEPRECATED
PRSound* PRSound::duplicate()
{
	long i, ch;
	PRSound* thee = new PRSound();
	thee->init(m_ny, m_xmin, m_xmax, m_nx, m_dx, m_x1);
	for (ch = 1; ch <= m_ny; ch++) {
		for (i = 1; i <= m_nx; i++) {
			thee->m_z[ch][i] = m_z[ch][i];
		}
	}
	return thee;
}

PRSound* PRSound::convertToMono()
{
	long i;
	PRSound* thee = new PRSound();
	thee->init(1, m_xmin, m_xmax, m_nx, m_dx, m_x1);
	if (m_ny == 1) {
		for (i = 1; i <= m_nx; i++) {
			thee->m_z[1][i] = m_z[1][i];
		}
	} else if (m_ny == 2) {   // Optimization.
		for (i = 1; i <= m_nx; i++) {
			thee->m_z[1][i] = 0.5*(m_z[1][i] + m_z[2][i]);
		}
	} else {
		for (i = 1; i <= m_nx; i++) {
			double sum = m_z[1][i] + m_z[2][i] + m_z[3][i];
			for (long channel = 4; channel <= m_ny; channel++) {
				sum += m_z[channel][i];
			}
			thee->m_z[1][i] = sum / m_ny;
		}
	}
	return thee;
}
#endif

#if 0
PRSound* PRSound::convertToStereo()
{
	if (m_ny == 2) 
		return copy();
	if (m_ny > 2) {
		_pincmsg2("[WARNING] Don't know how to convert a Sound with %ld channels to stereo.", m_ny);
		return NULL;
	}

	PRSound* thee = NULL;
	if (m_ny == 1) {
		_pincmsg1("[WARNING] PRSound::convertStereo");
	}

	thee = new PRSound(2, m_xmin, m_xmax, m_nx, m_dx, m_x1); 
	for (long i = 1; i <= m_nx; i ++) {
		thee->m_z[1][i] = thee->m_z[2][i] = m_z[1][i];
	}
	return thee;
}

PRSound* PRSound::extractChannel(long channel)
{
	if (channel <= 0 || channel > m_ny) {
		_pincmsg2("[WARNING] Cannot extract channel %d.", m_ny);
		return NULL;
	}
	PRSound* thee = new PRSound(1, m_xmin, m_xmax, m_nx, m_dx, m_x1);
	for (long i = 1; i <= m_nx; i ++) {
		thee->m_z[1][i] = m_z[channel][i];
	}
	return thee;
}

PRSound* PRSound::extractLeftChannel()
{
	if (m_ny != 2) { 
		_pincmsg1("[WARNING] Sound not stereo. Left channel not extracted.");
		return NULL;
	}
	PRSound* thee = new PRSound(1, m_xmin, m_xmax, m_nx, m_dx, m_x1);
	for (long i = 1; i <= m_nx; i++) {
		thee->m_z[1][i] = m_z[1][i];
	}
	return thee;
}

PRSound* PRSound::extractRightChannel()
{
	if (m_ny != 2) {
		_pincmsg1("[WARNING] Sound not stereo. Right channel not extracted.");
	}
	PRSound* thee = new PRSound(1, m_xmin, m_xmax, m_nx, m_dx, m_x1); 
	for (long i = 1; i <= m_nx; i ++) {
		thee->m_z[1][i] = m_z[2][i];
	}
	return thee;
}

PRSound* PRSound::combineToStereo(PRSound* thee)
{
	long i;
	if (m_ny != 1 || thee->m_ny != 1) {
		_pincmsg1("[WARNING] Can only combine mono sounds. Stereo sound not created.");
		return NULL;
	}
	if (m_dx != thee->m_dx) {
		_pincmsg1("[WARNING} Sampling frequencies do not match. Sounds not combined.");
		return NULL;
	}
	double dx = m_dx;   // or thee->m_dx, which is the same
	double xmin = m_xmin < thee->m_xmin ? m_xmin : thee->m_xmin;
	double xmax = m_xmax > thee->m_xmax ? m_xmax : thee->m_xmax;
	long myInitialZeroes = floor((m_xmin - xmin) / dx);
	long thyInitialZeroes = floor ((thee->m_xmin - xmin) / dx);
	double myx1 = m_x1 - m_dx * myInitialZeroes;
	double thyx1 = thee->m_x1 - thee->m_dx * thyInitialZeroes;
	double x1 = 0.5 * (myx1 + thyx1);
	long mynx = m_nx + myInitialZeroes;
	long thynx = thee->m_nx + thyInitialZeroes;
	long nx = mynx > thynx ? mynx : thynx;

	PRSound* him = new PRSound (2, xmin, xmax, nx, dx, x1);
	for (i = 1; i <= m_nx; i ++) {
		him->m_z[1][i + myInitialZeroes] = m_z[1][i];
	}
	for (i = 1; i <= thee->m_nx; i ++) {
		him->m_z[2][i + thyInitialZeroes] = thee->m_z[1][i];
	}
	return him;
}

PRSound* PRSound::upsample()
{
	return NULL;
}

PRSound* PRSound::resample(double samplingFrequency, long precision)
{
	return NULL;
}

PRSound* PRSound::append(double silenceDuration, PRSound* thee)
{
	return NULL;
}

PRSound* PRSound::convolve(PRSound* thee)
{
	return NULL;
}

PRSound* PRSound::crossCorrelate(PRSound* thee, double tmin, double tmax, int normalize)
{
	return NULL;
}

double PRSound::getRootMeanSquare(double xmin, double xmax)
{
	return 0.0;
}

double PRSound::getEnergy(double xmin, double xmax)
{
	return 0.0;
}

double PRSound::getPower(double xmin, double xmax)
{
	return 0.0;
}

double PRSound::getEnergyInAir()
{
	return 0.0;
}

double PRSound::getPowerInAir()
{
	return 0.0;
}

double PRSound::getIntensity_dB()
{
	return 0.0;
}

double PRSound::getNearestZeroCrossing(double position, long channel)
{
	return 0.0;
}

void PRSound::setZero(double tmin, double tmax, int roundTimesToNearestZeroCrossing)
{
}

PRSound* PRSound::createFromToneComplex(double startingTime, double endTime,
							double sampleRate, int phase, double frequencyStep,
							double firstFrequency, double ceiling, long numberOfComponents)
{
	return NULL;
}

void PRSound::multiplyByWindow(enum kSound_windowShape windowShape)
{
}

void PRSound::scaleIntensity(double newAverageIntensity)
{
}

void PRSound::overrideSamplingFrequency(double newSamplingFrequency)
{
}

PRSound* PRSound::extractPart(double t1, double t2, enum kSound_windowShape windowShape, 
										double relativeWidth, bool preserveTimes)
{
	return NULL;
}

int PRSound::filterWithFormants(double tmin, double tmax, int numberOfFormants, 
											double *formant, double *bandwidth)
{
	return 0;
}

PRSound* PRSound::filter_oneFormant(double frequency, double bandwidth)
{
	return NULL;
}

int PRSound::filterWithOneFormantInline(double frequency, double bandwidth)
{
	return 0;
}

PRSound* PRSound::filter_preemphasis(double frequency)
{
	return NULL;
}

PRSound* PRSound::filter_deemphasis(double frequency)
{
	return NULL;
}

void PRSound::reverse(double tmin, double tmax)
{
}
#endif



