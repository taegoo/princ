#ifndef __CHORD_NODE__
#define __CHORD_NODE__

// DEPRECATED:

typedef enum {
	// Majors
	C_maj,		// 0
	Csrp_maj,
	D_maj,
	Dsrp_maj,
	E_maj,
	F_maj,
	Fsrp_maj,
	G_maj,
	Gsrp_maj,
	A_maj,
	Asrp_maj,
	B_maj,

	// Minors
	C_min,		// 12
	Csrp_min,
	D_min,
	Dsrp_min,
	E_min,
	F_min,
	Fsrp_min,
	G_min,
	Gsrp_min,
	A_min,
	Asrp_min,
	B_min,

	_NONE
} ChordName;

struct ChordListNode{
	ChordName chord;
	int startPos;
	ChordListNode *next;
} ;

#endif
