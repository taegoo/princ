/*************************************************************************
 *  PRVector.h
 *
 *  Taegoo : Migration from praat/fon/Vector.h
 *
 *		- 'PRSound' inherits this
 *
 **************************************************************************/
#ifndef __PRVECTOR_H__
#define __PRVECTOR_H__

#include "PRSampled.h"

#define Vector_CHANNEL_AVERAGE  0
#define Vector_CHANNEL_1  1
#define Vector_CHANNEL_2  2
#define Vector_VALUE_INTERPOLATION_NEAREST  0
#define Vector_VALUE_INTERPOLATION_LINEAR  1
#define Vector_VALUE_INTERPOLATION_CUBIC  2
#define Vector_VALUE_INTERPOLATION_SINC70  3
#define Vector_VALUE_INTERPOLATION_SINC700  4

class PRVector : public PRSampled
{
public:
	PRVector();
	~PRVector();

	/*
	double m_xmin;	
	double m_xmax;
	long m_nx;	
	double m_dx;
	double m_x1;
	double m_ymin;
	double m_ymax;	
	long m_ny;	
	double m_dy;
	double m_y1;
	double **m_z;
	*/

	/*
	virtual void init(double xmin, double xmax, long nx, double dx, double x1);
	virtual long getWindowSamples(double xmin, double xmax, long *ixmin, long *ixmax);
	virtual int shortTermAnalysis(double windowDuration, double timeStep, long *numberOfFrames, double *firstTime);
	*/

	void initVector(double xmin, double xmax, long nx, double dx, double x1,
				double ymin, double ymax, long ny, double dy, double y1);

	double getAbsoluteExtremum(double xmin, double xmax, int interpolation);
	double getMinimum(double xmin, double xmax, int interpolation);
	double getMaximum(double xmin, double xmax, int interpolation);
	void getMinimumAndXAndChannel(double xmin, double xmax, int interpolation,
				double *return_minimum, double *return_xOfMinimum, long *return_channelOfMinimum);
	void getMaximumAndXAndChannel(double xmin, double xmax, int interpolation,
				double *return_maximum, double *return_xOfMaximum, long *return_channelOfMaximum);
	void getMaximumAndX(double xmin, double xmax, long channel, int interpolation,
								double *return_maximum, double *return_xOfMaximum);
	void getMinimumAndX(double xmin, double xmax, long channel, int interpolation,
								double *return_minimum, double *return_xOfMinimum);

	double getValueAtX(double x, long ilevel, int unit, int interpolation);
	double getValueAtX(double x, long ilevel, int interpolation);

	//double getValueAtSample (long isamp, long ilevel, int unit);
	void subtractMean();
#if 0
	double getXOfMinimum(double xmin, double xmax, int interpolation);
	double getXOfMaximum(double xmin, double xmax, int interpolation);
	long getChannelOfMinimum(double xmin, double xmax, int interpolation);
	long getChannelOfMaximum(double xmin, double xmax, int interpolation);

	double getMean(double xmin, double xmax, long channel);
	double getStandardDeviation(double xmin, double xmax, long channel);

	void addScalar(double scalar);
	void multiplyByScalar(double scalar);
	void scale(double scale);

private:
#endif
};

#endif
