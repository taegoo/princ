/*************************************************************************
 *  PRDurationTier.h
 *
 **************************************************************************/
#ifndef __PRDURATION_TIER_H__
#define __PRDURATION_TIER_H__

#include "PRRealTier.h"

class PRDurationTier : public PRRealTier
{
public:
	PRDurationTier(double tmin, double tmax);

	/*
	RealPoint **m_points; // must be sorted all the time
	long m_setSize;
	*/
	// inherited:
	/*
	double m_xmin;		
	double m_xmax;		

	long m_nx;		
	double m_dx;
	double m_x1;
	double m_ymin;	
	double m_ymax;	
	long m_ny;	
	double m_dy; 
	double m_y1;	
	double **m_z;	
	*/

private:

};

#endif
