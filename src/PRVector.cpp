/**************************************************************************
 *  PRVector.cpp
 *
 *  Taegoo : Migration from praat/fon/Vector.cpp
 *
 *		- 'PRSound' inherits this
 *
 **************************************************************************/

#include "PRVector.h"
#include "NUM.h"

PRVector::PRVector()
{
}

PRVector::~PRVector()
{
	if (m_z)
		NUMdmatrix_free(m_z, 1, 1);
		//NUMdmatrix_free(m_z, m_ny, m_nx); // error!
	m_z = NULL;
}

void PRVector::initVector(double xmin, double xmax, long nx, double dx, double x1,
						double ymin, double ymax, long ny, double dy, double y1)
{
	/*
	_pincmsg1("PRVector::init");
	_pincmsg6("    xmin=%f, xmax=%f, nx=%d, dx=%f, x1=%f",
						xmin, xmax, nx, dx, x1);
	_pincmsg6("    ymin=%f, ymax=%f, ny=%d, dy=%f, y1=%f", 
						ymin, ymax, ny, dy, y1);
						*/

	initSampled(xmin, xmax, nx, dx, x1);

	m_ymin = ymin;
	m_ymax = ymax;
	m_ny = ny;
	m_dy = dy;
	m_y1 = y1;
	m_z = NULL;

	m_z = NUMdmatrix(1, m_ny, 1, m_nx);
	if (!m_z) {
		printf("[ERROR] PRVector::initVector - No m_z\n");
	}
}

double PRVector::getAbsoluteExtremum(double xmin, double xmax, int interpolation)
{
	double minimum = fabs(getMinimum(xmin, xmax, interpolation));
	double maximum = fabs (getMaximum(xmin, xmax, interpolation));
	return minimum > maximum ? minimum : maximum;
}
double PRVector::getMinimum(double xmin, double xmax, int interpolation)
{
	double minimum;
	getMinimumAndXAndChannel(xmin, xmax, interpolation, &minimum, NULL, NULL);
	return minimum;
}

double PRVector::getMaximum(double xmin, double xmax, int interpolation)
{
	double maximum;
	getMaximumAndXAndChannel(xmin, xmax, interpolation, &maximum, NULL, NULL);
	return maximum;
}

void PRVector::getMinimumAndXAndChannel(double xmin, double xmax, int interpolation,
				double *return_minimum, double *return_xOfMinimum, long *return_channelOfMinimum)
{
	double minimum, xOfMinimum;
	long channelOfMinimum = 1;
	getMinimumAndX(xmin, xmax, 1, interpolation, &minimum, &xOfMinimum);
	for (long channel = 2; channel <= m_ny; channel ++) {
		double minimumOfChannel, xOfMinimumOfChannel;
		getMinimumAndX(xmin, xmax, channel, interpolation, &minimumOfChannel, &xOfMinimumOfChannel);
		if (minimumOfChannel < minimum) {
			minimum = minimumOfChannel;
			xOfMinimum = xOfMinimumOfChannel;
			channelOfMinimum = channel;
		}
	}
	if (return_minimum) 
		*return_minimum = minimum;
	if (return_xOfMinimum) 
		*return_xOfMinimum = xOfMinimum;
	if (return_channelOfMinimum) 
		*return_channelOfMinimum = channelOfMinimum;
}

void PRVector::getMaximumAndXAndChannel(double xmin, double xmax, int interpolation,
			double *return_maximum, double *return_xOfMaximum, long *return_channelOfMaximum)
{
	double maximum, xOfMaximum;
	long channelOfMaximum = 1;
	getMaximumAndX(xmin, xmax, 1, interpolation, &maximum, &xOfMaximum);
	for (long channel = 2; channel <= m_ny; channel ++) {
		double maximumOfChannel, xOfMaximumOfChannel;
		getMaximumAndX(xmin, xmax, channel, interpolation, &maximumOfChannel, &xOfMaximumOfChannel);
		if (maximumOfChannel > maximum) {
			maximum = maximumOfChannel;
			xOfMaximum = xOfMaximumOfChannel;
			channelOfMaximum = channel;
		}
	}
	if (return_maximum) 
		*return_maximum = maximum;
	if (return_xOfMaximum) 
		*return_xOfMaximum = xOfMaximum;
	if (return_channelOfMaximum) 
		*return_channelOfMaximum = channelOfMaximum;
}


void PRVector::getMaximumAndX(double xmin, double xmax, long channel, int interpolation,
							double *return_maximum, double *return_xOfMaximum)
{
	if (!m_z) {
		printf("[ERROR] PRVector::getMaximumAndX - No m_z\n");
		return;
	}

	long imin, imax, i;
	long n = m_nx;
	//Melder_assert (channel >= 1 && channel <= my ny);
	double *y = m_z[channel];
	double maximum, x;
	if (xmax <= xmin) { 
		xmin = m_xmin; xmax = m_xmax; 
	}
	if (!getWindowSamples(xmin, xmax, &imin, &imax)) {
		/*
		 * No samples between xmin and xmax.
		 * Try to return the greater of the values at these two points.
		 */
		double yleft = getValueAtX(xmin, channel,
					interpolation > Vector_VALUE_INTERPOLATION_NEAREST ? Vector_VALUE_INTERPOLATION_LINEAR : Vector_VALUE_INTERPOLATION_NEAREST);
		double yright = getValueAtX(xmax, channel,
			interpolation > Vector_VALUE_INTERPOLATION_NEAREST ? Vector_VALUE_INTERPOLATION_LINEAR : Vector_VALUE_INTERPOLATION_NEAREST);
		maximum = yleft > yright ? yleft : yright;
		x = yleft == yright ? (xmin + xmax) / 2 : yleft > yright ? xmin : xmax;
	} else {
		maximum = y[imin];
		x = imin;
		if (y[imax] > maximum) {
			maximum = y[imax];
			x = imax;
		}
		if (imin == 1) 
			imin++;
		if (imax == m_nx) 
			imax--;
		for (i = imin; i <= imax; i++) {
			if (y[i] > y[i - 1] && y[i] >= y[i + 1]) {
				double i_real, localMaximum = NUMimproveMaximum(y, n, i, interpolation, &i_real);
				if (localMaximum > maximum) {
					maximum = localMaximum;
					x = i_real;
				}
			}
		}
		x = m_x1 + (x - 1) * m_dx;   /* Convert sample to x. */
		if (x < xmin) 
			x = xmin; 
		else if (x > xmax) 
			x = xmax;
	}
	if (return_maximum) 
		*return_maximum = maximum;
	if (return_xOfMaximum) 
		*return_xOfMaximum = x;
}

void PRVector::getMinimumAndX(double xmin, double xmax, long channel, int interpolation,
							double *return_minimum, double *return_xOfMinimum)
{
	if (!m_z) {
		printf("[ERROR] PRVector::getMinimumAndX - No m_z\n");
		return;
	}

	long imin, imax, n = m_nx;
	//Melder_assert (channel >= 1 && channel <= my ny);
	double *y = m_z[channel];
	double minimum, x;
	if (xmax <= xmin) { 
		xmin = m_xmin; 
		xmax = m_xmax; 
	}
	if (!getWindowSamples(xmin, xmax, & imin, & imax)) {
		/*
		 * No samples between xmin and xmax.
		 * Try to return the lesser of the values at these two points.
		 */
		double yleft = getValueAtX(xmin, channel,
			interpolation > Vector_VALUE_INTERPOLATION_NEAREST ? Vector_VALUE_INTERPOLATION_LINEAR : Vector_VALUE_INTERPOLATION_NEAREST);
		double yright = getValueAtX(xmax, channel,
			interpolation > Vector_VALUE_INTERPOLATION_NEAREST ? Vector_VALUE_INTERPOLATION_LINEAR : Vector_VALUE_INTERPOLATION_NEAREST);
		minimum = yleft < yright ? yleft : yright;
		x = yleft == yright ? (xmin + xmax) / 2 : yleft < yright ? xmin : xmax;
	} else {
		minimum = y[imin];
		x = imin;
		if (y[imax] < minimum) {
			minimum = y[imax];
			x = imax;
		}
		if (imin == 1) 
			imin++;
		if (imax == m_nx) 
			imax--;
		for (long i = imin; i <= imax; i++) {
			if (y[i] < y[i - 1] && y[i] <= y[i + 1]) {
				double i_real, localMinimum = NUMimproveMinimum (y, n, i, interpolation, &i_real);
				if (localMinimum < minimum) 
					minimum = localMinimum, x = i_real;
			}
		}
		x = m_x1 + (x - 1) * m_dx;   /* Convert sample to x. */
		if (x < xmin) 
			x = xmin; 
		else if (x > xmax) 
			x = xmax;
	}
	if (return_minimum) 
		*return_minimum = minimum;
	if (return_xOfMinimum) 
		*return_xOfMinimum = x;
}

// TODO:?
/*
double PRVector::getValueAtSample (long isamp, long ilevel, int unit)
{
	return 0.0;
}
 */

double PRVector::getValueAtX(double x, long ilevel, int unit, int interpolation)
{
	return getValueAtX(x, ilevel, interpolation);
}

double PRVector::getValueAtX(double x, long ilevel, int interpolation)
{
	if (!m_z) {
		printf("[ERROR] PRVector::getValueAtX - No m_z\n");
		return 0.0;
	}
	double leftEdge = m_x1 - 0.5 * m_dx, rightEdge = leftEdge + m_nx * m_dx;
	if (x < leftEdge || x > rightEdge) 
		return NUMundefined;
	if (ilevel > Vector_CHANNEL_AVERAGE) {
		//Melder_assert (ilevel <= m_ny);
		return NUM_interpolate_sinc(m_z[ilevel], m_nx, xToIndex(x),
			interpolation == Vector_VALUE_INTERPOLATION_SINC70 ? NUM_VALUE_INTERPOLATE_SINC70 :
			interpolation == Vector_VALUE_INTERPOLATION_SINC700 ? NUM_VALUE_INTERPOLATE_SINC700 :
			interpolation);
	}
	double sum = 0.0;
	for (long channel = 1; channel <= m_ny; channel ++) {
		sum += NUM_interpolate_sinc(m_z[channel], m_nx, xToIndex(x),
			interpolation == Vector_VALUE_INTERPOLATION_SINC70 ? NUM_VALUE_INTERPOLATE_SINC70 :
			interpolation == Vector_VALUE_INTERPOLATION_SINC700 ? NUM_VALUE_INTERPOLATE_SINC700 :
			interpolation);
	}
	return sum / m_ny;
}

void PRVector::subtractMean()
{
	if (!m_z) {
		printf("[ERROR] PRVector::subtractMean - No m_z\n");
		return;
	}
	long i;
	for (long channel = 1; channel <= m_ny; channel++) {
		double sum = 0.0;
		for (i = 1; i <= m_nx; i ++) {
			sum += m_z[channel][i];
		}
		double mean = sum / m_nx;
		for (i = 1; i <= m_nx; i ++) {
			m_z[channel][i] -= mean;
		}
	}
}


#if 0
double PRVector::getXOfMinimum(double xmin, double xmax, int interpolation)
{
	double xOfMinimum;
	Vector_getMinimumAndXAndChannel (me, xmin, xmax, interpolation, NULL, & xOfMinimum, NULL);
	return xOfMinimum;
}

double PRVector::getXOfMaximum(double xmin, double xmax, int interpolation)
{
	double xOfMaximum;
	Vector_getMaximumAndXAndChannel (me, xmin, xmax, interpolation, NULL, & xOfMaximum, NULL);
	return xOfMaximum;
}

long PRVector::getChannelOfMinimum(double xmin, double xmax, int interpolation)
{
	long channelOfMinimum;
	Vector_getMinimumAndXAndChannel (me, xmin, xmax, interpolation, NULL, NULL, & channelOfMinimum);
	return channelOfMinimum;
}

long PRVector::getChannelOfMaximum(double xmin, double xmax, int interpolation)
{
	long channelOfMaximum;
	Vector_getMaximumAndXAndChannel (me, xmin, xmax, interpolation, NULL, NULL, & channelOfMaximum);
	return channelOfMaximum;
}

double PRVector::getMean(double xmin, double xmax, long channel)
{
	return Sampled_getMean (me, xmin, xmax, channel, 0, TRUE);
}

double PRVector::getStandardDeviation(double xmin, double xmax, long channel)
{
	if (xmax <= xmin) { xmin = my xmin; xmax = my xmax; }
	long imin, imax, n = Sampled_getWindowSamples (me, xmin, xmax, & imin, & imax);
	if (n < 2) return NUMundefined;
	if (ilevel == Vector_CHANNEL_AVERAGE) {
		double sum2 = 0.0;
		for (long channel = 1; channel <= my ny; channel ++) {
			double mean = Vector_getMean (me, xmin, xmax, channel);
			for (long i = imin; i <= imax; i ++) {
				double diff = my z [channel] [i] - mean;
				sum2 += diff * diff;
			}
		}
		return sqrt (sum2 / (n * my ny - my ny));   // The number of constraints equals the number of channels,
				// because from every channel its own mean was subtracted.
				// Corollary: a two-channel mono sound will have the same stdev as the corresponding one-channel sound.
	}
	double mean = Vector_getMean (me, xmin, xmax, ilevel);
	double sum2 = 0.0;
	for (long i = imin; i <= imax; i ++) {
		double diff = my z [ilevel] [i] - mean;
		sum2 += diff * diff;
	}
	return sqrt (sum2 / (n - 1));
}

void PRVector::addScalar(double scalar)
{
	for (long channel = 1; channel <= my ny; channel ++) {
		for (long i = 1; i <= my nx; i ++) {
			my z [channel] [i] += scalar;
		}
	}
}

void PRVector::multiplyByScalar(double scalar)
{
	for (long channel = 1; channel <= my ny; channel ++) {
		for (long i = 1; i <= my nx; i ++) {
			my z [channel] [i] *= scalar;
		}
	}
}

void PRVector::scale(double scale)
{
	double extremum = 0.0;
	for (long channel = 1; channel <= my ny; channel ++) {
		for (long i = 1; i <= my nx; i ++) {
			if (fabs (my z [channel] [i]) > extremum) extremum = fabs (my z [channel] [i]);
		}
	}
	if (extremum != 0.0) {
		Vector_multiplyByScalar (me, scale / extremum);
	}
}
#endif


