/*************************************************************************
 *  PRRealTier.cpp
 *
 *  Taegoo : Migration from fon/RealTier.c
 *
 **************************************************************************/

#include "PRRealTier.h"
#include "NUM.h"

PRRealTier::PRRealTier()
{
}

PRRealTier::~PRRealTier()
{
	//printf("~PRRealTier\n");
	long i;
	if (m_points) {
		for (i = 1; i <= m_setSize; i++) {
			if (m_points[i]) {
				free(m_points[i]);
				m_points[i] = NULL;
			}
		}
		// be carefull: move the pointer
		free(m_points+1);
	}
	m_points = NULL;
}

PRRealTier::PRRealTier(double tmin, double tmax)
{
	init(tmin, tmax);
}

void PRRealTier::init(double tmin, double tmax)
{
	m_xmin = tmin;
	m_xmax = tmax;

	// Initialize 'm_points'
	pointSet_create();
}

void PRRealTier::removeTail(double lenSec)
{
	//long numSamples = lenSec / (double)m_dx;
	long index = timeToLowIndex(m_xmax-lenSec);

	m_xmax -= lenSec;
	m_setSize = index;
}

// static
// New for v3.0
void PRRealTier::appendRealTierToRealTier(PRRealTier *src, PRRealTier *dest)
{
	// Do not increase m_setSize here. It will be automatically increased in addPoint()
	//dest->m_setSize += src->m_setSize;
	double old_xmax = dest->m_xmax;
	// update
	dest->m_xmax += src->m_xmax;

	for (long i = 1; i <= src->m_setSize; i++) {
		//printf("  adding: time=%f, value=%f\n", src->m_points[i]->m_time, src->m_points[i]->m_value);
		dest->addPoint(src->m_points[i]->m_time + old_xmax, src->m_points[i]->m_value);
	}
}

PRRealTier* PRRealTier::copyPart(double tmin, double tmax)
{
	//printf("PRRealTier::copyPart - start\n");
	long imin, imax;
	PRRealTier *thee = new PRRealTier(0.0, tmax-tmin);
	thee->m_xmax = tmax-tmin;

    imin = timeToLowIndex(tmin);	// Get 'Low' index
	imax = timeToHighIndex(tmax);	// Get 'High' index
	
    if (imin < 1)
		imin = 1;
	if (imax > m_setSize)
		imax = m_setSize;

	for (long i = imin; i <= imax; i++) {
		if (m_points[i]->m_time < tmin)
			thee->addPoint(0.0, m_points[i]->m_value);
		else if (m_points[i]->m_time > tmax)
			thee->addPoint(tmax-tmin, m_points[i]->m_value);
		else
			thee->addPoint(m_points[i]->m_time - tmin, m_points[i]->m_value);
	}

	//printf("PRRealTier::copyPart - end\n");
	return thee;
}

//
// From RealTier.c:
//

int PRRealTier::addPoint(double t, double value)
{
	RealPoint* pt = (RealPoint*)malloc(sizeof(RealPoint));
	pt->m_time = t;
	pt->m_value = value;

	pointSet_addPoint(pt);
	return 1;
}

double PRRealTier::getValueAtTime(double t)
{
	long n = m_setSize;
	long ileft, iright;
	double tleft, tright, fleft, fright;
	RealPoint* pointLeft;
	RealPoint* pointRight;

	if (n == 0) 
		return NUMundefined;

	pointRight = m_points[1];
	if (t <= pointRight->m_time) 
		return pointRight->m_value;   /* Constant extrapolation. */

	pointLeft = m_points[n];
	if (t >= pointLeft->m_time) 
		return pointLeft->m_value;   /* Constant extrapolation. */

	//Melder_assert (n >= 2);
	if (n < 2)
		return NUMundefined;

	ileft = timeToLowIndex(t);
	iright = ileft + 1;
	//Melder_assert (ileft >= 1 && iright <= n);
	if (ileft < 1 || iright > n)
		return NUMundefined;

	pointLeft = m_points[ileft];
	pointRight = m_points[iright];
	tleft = pointLeft->m_time;
	fleft = pointLeft->m_value;
	tright = pointRight->m_time;
	fright = pointRight->m_value;

	return t == tright ? fright   /* Be very accurate. */
		: tleft == tright ? 0.5 * (fleft + fright)   /* Unusual, but possible; no preference. */
		: fleft + (t - tleft) * (fright - fleft) / (tright - tleft);   /* Linear interpolation. */
}

double PRRealTier::getArea(double tmin, double tmax)
{
	long n = m_setSize;
	long imin, imax;
	RealPoint **points = m_points;

	if (n == 0) 
		return NUMundefined;
	if (n == 1) 
		return (tmax - tmin) * points[1]->m_value;

	imin = timeToLowIndex(tmin);
	if (imin == n) 
		return (tmax - tmin) * points[n]->m_value;
	imax = timeToHighIndex(tmax);
	if (imax == 1) 
		return (tmax - tmin) * points[1]->m_value;
	/*
	Melder_assert (imin < n);
	Melder_assert (imax > 1);
	*/
	/*
	 * Sum the areas between the points.
	 * This works even if imin is 0 (offleft) and/or imax is n + 1 (offright).
	 */
	double area = 0.0;
	for (long i = imin; i < imax; i ++) {
		double tleft, fleft, tright, fright;
		if (i == imin) {
			tleft = tmin;
			fleft = getValueAtTime(tmin);
		} else {
			tleft = points[i]->m_time;
			fleft = points[i]->m_value;
		}
		if (i + 1 == imax) {
			tright = tmax;
			fright = getValueAtTime(tmax);
		} else {
			tright = points[i + 1]->m_time;
			fright = points[i + 1]->m_value;
		}
		area += 0.5 * (fleft + fright) * (tright - tleft);
	}
	return area;
}

long PRRealTier::timeToLowIndex(double time) 
{
	if (m_setSize == 0)
		return 0;   // undefined
	long ileft = 1;
	long iright = m_setSize;

	RealPoint **points = m_points;
	double tleft = points[ileft]->m_time;
	if (time < tleft) 
		return 0;   // offleft
	double tright = points[iright]->m_time;
	if (time >= tright) 
		return iright;
	/*
	Melder_assert (time >= tleft && time < tright);
	Melder_assert (iright > ileft);
	*/
	while (iright > ileft + 1) {
		long imid = (ileft + iright) / 2;
		double tmid = points[imid]->m_time;
		if (time < tmid) {
			iright = imid;
			tright = tmid;
		} else {
			ileft = imid;
			tleft = tmid;
		}
	}
	/*
	Melder_assert (iright == ileft + 1);
	Melder_assert (ileft >= 1);
	Melder_assert (iright <= my points -> size);
	Melder_assert (time >= points [ileft] -> time);
	Melder_assert (time <= points [iright] -> time);
	*/
	return ileft;
}

long PRRealTier::timeToHighIndex(double time) 
{
	if (m_setSize == 0) 
		return 0;   // undefined; is this right?
	long ileft = 1;
	long iright = m_setSize;

	RealPoint **points = m_points;
	double tleft = points[ileft]->m_time;
	if (time <= tleft) 
		return 1;
	double tright = points[iright]->m_time;
	if (time > tright) 
		return iright + 1;   // offright
	/*
	Melder_assert (time > tleft && time <= tright);
	Melder_assert (iright > ileft);
	*/
	while (iright > ileft + 1) {
		long imid = (ileft + iright) / 2;
		double tmid = points[imid]->m_time;
		if (time <= tmid) {
			iright = imid;
			tright = tmid;
		} else {
			ileft = imid;
			tleft = tmid;
		}
	}
	/*
	Melder_assert (iright == ileft + 1);
	Melder_assert (ileft >= 1);
	Melder_assert (iright <= my points -> size);
	Melder_assert (time >= points [ileft] -> time);
	Melder_assert (time <= points [iright] -> time);
	*/
	return iright;
}

double PRRealTier::getMean_points(double tmin, double tmax) 
{
	long n = m_setSize;
	long imin, imax;
	double sum = 0.0;
	RealPoint **points = m_points;

	if (tmax <= tmin) {  // autowindow
		tmin = m_xmin; 
		tmax = m_xmax; 
	} 

	n = getWindowPoints(tmin, tmax, &imin, &imax);
	if (n == 0) 
		return NUMundefined;
	for (long i = imin; i <= imax; i ++)
		sum += points[i]->m_value;

	return sum / n;
}

long PRRealTier::getWindowPoints(double tmin, double tmax, long *imin, long *imax) 
{
	if (m_setSize == 0) 
		return 0;
	*imin = timeToHighIndex(tmin);
	*imax = timeToLowIndex(tmax);
	if (*imax < *imin) 
		return 0;
	return *imax - *imin + 1;
}
#if 0

double PRRealTier::getValueAtIndex(long point)
{
	return 0.0;
}

double PRRealTier::getMinimumValue()
{
	return 0.0;
}

double PRRealTier::getMaximumValue()
{
	return 0.0;
}


double PRRealTier::getMean_curve(double tmin, double tmax)
{
	return 0.0;
}


double PRRealTier::getStandardDeviation_curve(double tmin, double tmax)
{
	return 0.0;
}

double PRRealTier::getStandardDeviation_points(double tmin, double tmax)
{
	return 0.0;
}

int PRRealTier::interpolateQuadratically(long numberOfPointsPerParabola, int logarithmically)
{
	return 0;
}

PRRealTier* PRRealTier::Vector_to_RealTier(long channel)
{
	return NULL;
}

PRRealTier* PRRealTier::Vector_to_RealTier_peaks(long channel)
{
	return NULL;
}

PRRealTier* PRRealTier::Vector_to_RealTier_valleys(long channel)
{
	return NULL;
}

PRRealTier* PRRealTier::PointProcess_upto_RealTier(PRPointProcess *pointProc, double value)
{
	return NULL;
}

void PRRealTier::multiplyPart(double tmin, double tmax, double factor)
{
}

void PRRealTier::removePointsBelow(double level)
{
}
#endif

//
// Adopted from Collction.c:
//
void PRRealTier::pointSet_create() 
{
	m_setCapacity = 10; // initial capacity
	m_setSize = 0;

	m_points = (RealPoint**)calloc(m_setCapacity, sizeof(RealPoint*));
	m_points--; /* base 1 */
}

// Compares m_time of the two points
int PRRealTier::pointSet_compare(RealPoint* p1, RealPoint* p2) 
{
	if (p1->m_time < p2->m_time) 
		return -1;
	if (p1->m_time > p2->m_time) 
		return +1;
	return 0;
}

long PRRealTier::pointSet_position(RealPoint* data) 
{
	int where;
	long left = 1;
	long right = m_setSize;

	if (m_setSize == 0) 
		return 1;   /* Empty set? 'data' is going to be the first item. */

	where = pointSet_compare(data, m_points[m_setSize]);   /* Compare with last item. */

	if (where > 0) 
		return m_setSize + 1;   /* Insert at end. */
	if (where == 0) 
		return 0;
	if (pointSet_compare(data, m_points[1]) < 0) 
		return 1;   /* Compare with first item. */

	while (left < right - 1) {
		long mid = (left + right) / 2;
		if (pointSet_compare(data, m_points[mid]) >= 0)
			left = mid;
		else
			right = mid;
	}
	//Melder_assert (right == left + 1);
	if (!pointSet_compare(data, m_points[left]) || !pointSet_compare(data, m_points[right]))
		return 0;

	return right;
}

int PRRealTier::pointSet_hasPoint(RealPoint* item) 
{
	return pointSet_position(item) == 0;
}

int PRRealTier::pointSet_insertPoint(RealPoint* data, long pos) 
{
	long i;
	if (m_setSize >= m_setCapacity) {
		RealPoint **dum = (RealPoint **)realloc(m_points + 1, 2 * m_setCapacity * sizeof(RealPoint*));
		/*
		if (! dum) 
			return Melder_error1 (L"Collection_insert: out of memory.");
			*/
		m_points = dum - 1;
		m_setCapacity *= 2;
	}

	m_setSize++;
	for (i = m_setSize; i > pos; i --) 
		m_points[i] = m_points[i - 1];
	m_points[pos] = data;

	return 1;
}

int PRRealTier::pointSet_addPoint(RealPoint* data) 
{
	long index;
	if (!data)
		return 0;
	index = pointSet_position(data);

	if (index) {
		return pointSet_insertPoint(data, index);
	} else {
		printf("[WARNING] PRRealTier::pointSet_addPoint() could not insert at time=%f\n", data->m_time);
		if (data)
			free(data);   /* Could not insert; I am owner, so I must dispose of the data!!! */
		return 1;   /* Refusal; all right. */
	}
}

void PRRealTier::pointSet_removePoint(long pos) 
{
	long i;
	if (pos < 1 || pos > m_setSize)
		return;
	if (m_points[pos])
		free(m_points[pos]);
	for (i = pos; i < m_setSize; i ++) 
		m_points[i] = m_points[i + 1];

	m_setSize--;
}

void PRRealTier::pointSet_removeAllPoints() 
{
	long i;
	for (i = 1; i <= m_setSize; i ++) 
		free(m_points[i]);
	m_setSize = 0;
}

