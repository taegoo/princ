/*************************************************************************
 *  PRSampled.h
 *
 *  Taegoo : Migration from praat/fon/Sampled.h
 *
 *		- Parent class of 'PRSound' and 'PRPitch'
 *
 **************************************************************************/
#ifndef __PRSAMPLED_H__
#define __PRSAMPLED_H__

/* A Sampled is a Function that is sampled at nx points [1..nx], */
/* which are spaced apart by a constant distance dx. */
/* The first sample point is at x1, the second at x1 + dx, */
/* and the last at x1 + (nx - 1) * dx. */

class PRSampled
{
public:
	PRSampled();
	virtual ~PRSampled();
	void initSampled(double xmin, double xmax, long nx, double dx, double x1);

	long m_nx;		
	double m_dx;		
	double m_x1;	
	double m_xmin;	
	double m_xmax;		

	double m_ymin;	
	double m_ymax;
	long m_ny;	
	double m_dy; 
	double m_y1;	
	double **m_z;		

	double getNx();
	double getDx(); 
	double getX(long ix);

	double indexToX(long i);
	double xToIndex (double x);
	long xToLowIndex (double x);
	long xToHighIndex(double x);
	long xToNearestIndex(double x);

	double _getValueAtSample (long isamp, long ilevel, int unit);

	int shortTermAnalysis(double windowDuration, double timeStep, long *numberOfFrames, 
								double *firstTime);
	/*
		Function:
			how to put as many analysis windows of length 'windowDuration' as possible into my duration,
			when they are equally spaced by 'timeStep'.
		Input arguments:
			windowDuration:
				the duration of the analysis window, in seconds.
			timeStep:
				the time step, in seconds.
		Output arguments:
			numberOfFrames:
				at least 1 (if no failure); equals floor ((nx * dx - windowDuration) / timeStep) + 1.
			firstTime:
				the centre of the first frame, in seconds.
		Return value:
			1 = OK, 0 = failure.
		Failures:
			Window longer than signal.
		Postconditions:
			the frames are divided symmetrically over my defined domain,
			which is [x1 - dx/2, x[nx] + dx/2], where x[nx] == x1 + (nx - 1) * dx.
			All analysis windows will fit into this domain.
		Usage:
			the resulting Sampled (analysis sequence, e.g., Pitch, Formant, Spectrogram, etc.)
			will have the following attributes:
				result -> xmin == my xmin;   // Copy logical domain.
				result -> xmax == my xmax;
				result -> nx == numberOfFrames;
				result -> dx == timeStep;
				result -> x1 == firstTime;
	*/

	virtual double getValueAtX(double x, long ilevel, int unit, int interpolate);
	virtual double getValueAtSample (long isamp, long ilevel, int unit);

	long getWindowSamples(double xmin, double xmax, long *ixmin, long *ixmax);
#if 0
	double getValueAtSample(long isamp, long ilevel, int unit);
	long countDefinedSamples(long ilevel, int unit);
	double* getSortedValues (long ilevel, int unit, long *numberOfValues);

	double getQuantile(double xmin, double xmax, double quantile, long ilevel, int unit);
	double getMean(double xmin, double xmax, long ilevel, int unit, int interpolate);
	double getMean_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate);
	double getIntegral(double xmin, double xmax, long ilevel, int unit, int interpolate);
	double getIntegral_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate);
	double getStandardDeviation(double xmin, double xmax, long ilevel, int unit, int interpolate);
	double getStandardDeviation_standardUnit(double xmin, double xmax, long ilevel, int averagingUnit, int interpolate);

	void getMinimumAndX(double xmin, double xmax, long ilevel, int unit, int interpolate,
								double* return_minimum, double *return_xOfMinimum);
	double getMinimum(double xmin, double xmax, long ilevel, int unit, int interpolate);
	double getXOfMinimum(double xmin, double xmax, long ilevel, int unit, int interpolate);
	void getMaximumAndX(double xmin, double xmax, long ilevel, int unit, int interpolate,
							double *return_maximum, double *return_xOfMaximum);
	double getMaximum(double xmin, double xmax, long ilevel, int unit, int interpolate);
	double getXOfMaximum(double xmin, double xmax, long ilevel, int unit, int interpolate);
#endif

//private:
};

#endif
