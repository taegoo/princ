/*************************************************************************
 *  PRPointProcess.h
 *
 *  Taegoo : Migration from fon/PointProcess.h 
 *
 **************************************************************************/
#ifndef __PRPOINT_PROCESS_H_
#define __PRPOINT_PROCESS_H_

class PRPointProcess
{
public:
	PRPointProcess(double startingTime, double finishingTime, long initialMaxnt);
	virtual ~PRPointProcess();

	double m_xmin;	
	double m_xmax;	
	long m_maxnt;
	long m_nt;
	double *m_t;

	int init(double startingTime, double finishingTime, long initialMaxnt);
	int addPoint(double t);
	void removeTail(double lenSec);
	long getLowIndex(double t);
	long getHighIndex(double t);
	long getNearestIndex(double t);

	PRPointProcess* copyPart(double tmin, double tmax);
	static void appendPointProcessToPointProcess(PRPointProcess *src, PRPointProcess *dest);
#if 0
	PRPointProcess* createPoissonProcess(double startingTime, double finishingTime, double density);
	long getWindowPoints(double tmin, double tmax, long *imin, long *imax);
	long findPoint(double t);
	void removePointNear(double t);
	void removePoints(long first, long last);
	void removePointsBetween(double fromTime, double toTime);
	double getInterval(double t);
	PRPointProcess* pointUnion(PRPointProcess* thee);
	PRPointProcess* intersection(PRPointProcess* thee);
	PRPointProcess* difference(PRPointProcess* thee);
	int fill(double tmin, double tmax, double period);
	int voice(double period, double maxT);

	long getNumberOfPeriods(double tmin, double tmax, double minimumPeriod, 
									double maximumPeriod, double maximumPeriodFactor);
	double getMeanPeriod(double tmin, double tmax, double minimumPeriod, 
								double maximumPeriod, double maximumPeriodFactor);
	double getStdevPeriod(double tmin, double tmax, double minimumPeriod, 
								double maximumPeriod, double maximumPeriodFactor);
#endif
private:
};

#endif
