/*************************************************************************
 *  PRPointProcess.cpp
 *
 *  Taegoo : Migration from fon/PointProcess.c 
 *
 **************************************************************************/

#include "PRPointProcess.h"
#include "NUM2.h"

PRPointProcess::PRPointProcess(double startingTime, double finishingTime, long initialMaxnt)
{
	init(startingTime, finishingTime, initialMaxnt);
}

PRPointProcess::~PRPointProcess()
{
	NUMdvector_free(m_t, 1);
}

int PRPointProcess::init(double tmin, double tmax, long initialMaxnt)
{
	m_xmin = tmin;
	m_xmax = tmax;
	if (initialMaxnt < 1) 
		initialMaxnt = 1;
	m_maxnt = initialMaxnt;
	m_nt = 0;

	/*
	m_t = (double**)calloc(m_maxnt, sizeof(double*));
	for (long i = 0; i < m_maxnt; i++) {
		m_t[i] = (double*)calloc(1, sizeof(double));
	}
	*/
	m_t = NUMdvector(1, m_maxnt);

	return 1;
}

void PRPointProcess::removeTail(double lenSec)
{
	//long numSamples = lenSec / (double)m_dx;
	long index = getLowIndex(m_xmax-lenSec);

	m_xmax -= lenSec;
	m_nt = index;
}

void PRPointProcess::appendPointProcessToPointProcess(PRPointProcess *src, PRPointProcess *dest)
{
	long i;
	double old_xmax = dest->m_xmax;
	// update
	dest->m_xmax += src->m_xmax;

	// Do not assing new m_maxnt, m_nt here!
	// They will be reassigned with proper memory allocation in addPoint()
	//dest->m_maxnt += src->m_maxnt;
	//dest->m_nt += src->m_nt;

	for (i = 1; i <= src->m_nt; i++) {
		dest->addPoint(src->m_t[i] + old_xmax);
	}
}

PRPointProcess* PRPointProcess::copyPart(double tmin, double tmax)
{
	PRPointProcess* thee = new PRPointProcess(0, tmax-tmin, 10);

	long imin = getHighIndex(tmin);
	long imax = getLowIndex(tmax);

	if (imin < 1)
		imin = 1;
	if (imax > m_nt)
		imax = m_nt;

	for (long i = imin; i <= imax; i++) {
		if (m_t[i] - tmin >= 0.0)		// double-check
			thee->addPoint(m_t[i] - tmin);
	}
	return thee;
}
int PRPointProcess::addPoint(double t)
{
	if (t == NUMundefined) {
		//_pincmsg1("Cannot add a point at an undefined time.");
		return 0;
	}
	if (m_nt >= m_maxnt) {
		double *dum = NUMdvector(1, 2 * m_maxnt);
		if (!dum) 
			return 0;
		NUMdvector_copyElements(m_t, dum, 1, m_nt);
		NUMdvector_free(m_t, 1);   // dangle
		m_t = dum;   // undangle
		m_maxnt *= 2;
	}
	if (m_nt == 0 || t >= m_t[m_nt]) {   /* Special case that often occurs in practice. */
		m_t [++m_nt] = t;
	} else {
		long left = getLowIndex(t);
		long i;
		for (i = m_nt; i > left; i --) 
			m_t[i + 1] = m_t[i];
		m_nt++;
		m_t[left + 1] = t;
	}
	return 1;
}

long PRPointProcess::getLowIndex(double t)
{
	long left, right;
	if (m_nt == 0 || t < m_t[1])
		return 0;
	if (t >= m_t[m_nt])   /* Special case that often occurs in practice. */
		return m_nt;
	//Melder_assert (my nt != 1);   /* May fail if t or my t [1] is NaN. */
	/* Start binary search. */
	left = 1;
	right = m_nt;
	while (left < right - 1) {
		long mid = (left + right) / 2;
		if (t >= m_t[mid]) 
			left = mid; 
		else 
			right = mid;
	}
	//Melder_assert (right == left + 1);
	return left;
}

long PRPointProcess::getHighIndex(double t)
{
	long left, right;
	if (m_nt == 0)
		return 0;
	if (t <= m_t[1])
		return 1;
	if (t > m_t[m_nt])
		return m_nt + 1;

	/* Start binary search. */
	left = 1;
	right = m_nt;
	while (left < right - 1) {
		long mid = (left + right) / 2;
		if (t > m_t[mid]) 
			left = mid; 
		else 
			right = mid;
	}
	//Melder_assert (right == left + 1);
	return right;
}

long PRPointProcess::getNearestIndex(double t)
{
	long left, right;
	if (m_nt == 0)
		return 0;
	if (t <= m_t[1])
		return 1;
	if (t >= m_t[m_nt])
		return m_nt;

	/* Start binary search. */
	left = 1;
	right = m_nt;
	while (left < right - 1) {
		long mid = (left + right) / 2;
		if (t >= m_t[mid]) 
			left = mid; 
		else 
			right = mid;
	}
	//Melder_assert (right == left + 1);
	return t - m_t[left] < m_t[right] - t ? left : right;
}


#if 0
PRPointProcess* PRPointProcess::createPoissonProcess(double startingTime, double finishingTime, double density)
{
	return NULL;
}

long PRPointProcess::getWindowPoints(double tmin, double tmax, long *imin, long *imax)
{
	return 0;
}


long PRPointProcess::findPoint(double t)
{
	return 0;
}

void PRPointProcess::removePoint(long index)
{
}

void PRPointProcess::removePointNear(double t)
{
}

void PRPointProcess::removePoints(long first, long last)
{
}

void PRPointProcess::removePointsBetween(double fromTime, double toTime)
{
}

double PRPointProcess::getInterval(double t)
{
	return 0.0;
}

PRPointProcess* PRPointProcess::pointUnion(PRPointProcess* thee)
{
	return NULL;
}

PRPointProcess* PRPointProcess::intersection(PRPointProcess* thee)
{
	return NULL;
}

PRPointProcess* PRPointProcess::difference(PRPointProcess* thee)
{
	return NULL;
}

int PRPointProcess::fill(double tmin, double tmax, double period)
{
	return 0;
}

int PRPointProcess::voice(double period, double maxT)
{
	return 0;
}

long PRPointProcess::getNumberOfPeriods(double tmin, double tmax, double minimumPeriod, 
								double maximumPeriod, double maximumPeriodFactor)
{
	return 0;
}

double PRPointProcess::getMeanPeriod(double tmin, double tmax, double minimumPeriod, 
							double maximumPeriod, double maximumPeriodFactor)
{
	return 0.0;
}

double PRPointProcess::getStdevPeriod(double tmin, double tmax, double minimumPeriod, 
							double maximumPeriod, double maximumPeriodFactor)
{
	return 0.0;
}
#endif

