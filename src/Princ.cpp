/*************************************************************************
 *  Princ.cpp
 *
 *  Taegoo : The main gateway for Princ
 *
 *  Summary of the public methods
 *
 * 	init() :
 *			Creates m_manipulation - analyze pitch
 *			Saves m_origPitchTier
 *
 *		updatePlan() :
 *			Gets chord/scale info and updates m_manipulation->m_pitchTier
 *
 *		renderOutput() :
 *			Processes for the whole sound to a 'WaveTrack'
 *
 *		processBuffer() :
 *			Processes for the requested region and returns the processed buffer.
 *			(audio-callback may call this)
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Princ.h"
#include "PRPitchTier.h"
#include "PRManipulation.h"
#include "NUMmachar.h"
#include "NUM.h"

//#define MIN_NOTE_LEN_SEC	0.15 // sensitive parameter

//extern PincProject* gActiveProject;
extern double gPitchTimeStep;	// Declared in PRManipulation

//
//
Princ::Princ(double rate, princ_quality_t quality)
{
	m_quality = quality;
	m_manipulation = NULL;
	//m_origPitchTier = NULL; // Moved to PRManipulation
	//m_pitchObj = NULL;
	//m_chordList = NULL;

	m_playbackSpeed = 1.0;
	m_rate = rate;
	//m_cellLen = cellLen;
	m_addHeadSec = 0.0;
	m_addTailSec = 0.0;
	//m_key = C_maj; // default to C major

	// TODO: Init with scale - DEPRECATED:
	//setupScaleNoteList();

	//
	// init globals on the library
	//
	NUMmachar();
	//NUMinit();
}

Princ::~Princ()
{
	deinit();
}

void Princ::deinit()
{
	printf("Princ::deinit\n");
	if (m_manipulation)
		delete m_manipulation;
	m_manipulation = NULL;
	/*
	if (m_pitchObj)
		delete m_pitchObj;
		*/
	/*
	if (m_origPitchTier)
		delete m_origPitchTier;
	m_origPitchTier = NULL;
	*/
}

// init input buffer and analyze
// For backward compatibility where speed was not supported
void Princ::init(float* inputBuf, 
					long numSamples, 
					long tailLen, 
					char *sndRootDir,
					bool releaseSndMemory,
					__progress_callback_t callback, float progOffset, float progChunk)
{
	init(inputBuf, 
		numSamples, 
		tailLen, 
		sndRootDir,
		releaseSndMemory,
		1.0, // speed
		callback, progOffset, progChunk);
}

void Princ::init(float* inputBuf, 
					long numSamples, 
					long tailLen, 
					char *sndRootDir,
					bool releaseSndMemory,
					double playbackSpeed, // new parameter
					__progress_callback_t callback, float progOffset, float progChunk)
{
	long i;
	int numChannels = 1; 	// mono
	double xmin = 0.0; 		// start sec 
	double xmax = (double)(numSamples+tailLen) / m_rate;	// end time sec
	double dx = 1.0 / m_rate;		// period
	char* progMsg = (char*)"Preparing...";

	printf("Princ::init - start\n");

	m_addHeadSec = 0.05;//(double)numAddHeadSamples / m_rate;
	m_addTailSec = 0.05;//(double)numAddTailSamples / m_rate;

	m_playbackSpeed = playbackSpeed;
	/*
	printf("Princ::init - numSamples=%ld, m_addHeadSec=%f, m_addTailSec=%f\n", numSamples, m_addHeadSec, m_addTailSec);
	*/

	//
	// init globals on the library
	//
	/*
	NUMmachar();
	NUMinit();
	*/
	m_sndEndSec = xmax;
	//m_pitchObj = NULL;
	
	// invalidate:
	if (m_manipulation)
		delete m_manipulation;
	m_manipulation = NULL;

	//
	// Create PRSound instance and to send to PRManipulation
	//
	if (callback)
		callback(progOffset + 0.05*progChunk, progMsg);

	// STAT:
	/*
	float sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("      +++ sec: %f - init() - start\n", sysTime);
	*/

	//printf("Princ::init - init PRSound\n");
	PRSound *snd = new PRSound();
	snd->init(numChannels, xmin, xmax, numSamples+tailLen, dx, xmin);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("      +++ sec: %f - after init PRSound\n", sysTime);
	*/

	// m_z : base 1 ,  inputBuf : base 0
	for (i = 1; i <= numSamples+tailLen; i++) {
		//_pincmsg3("    i=%d, val=%f", i, inputBuf[i-1]);
		snd->m_z[1][i] = (double)inputBuf[i-1]; 
	}

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("      +++ sec: %f - before creating PRManipulation\n", sysTime);
	*/

	//m_manipulation = new PRManipulation(snd->m_xmin, snd->m_xmax);
	//printf("Creating new PRManipulation\n");
	//printf("Princ::init - init PRManipulation\n");

	// snd will assigned to m_manipulation->m_sound
	m_manipulation = new PRManipulation(snd, m_quality);
	m_manipulation->updateDurationTierWithSpeed(m_playbackSpeed);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("      +++ sec: %f - after creating PRManipulation\n", sysTime);
	*/
	if (callback)
		callback(progOffset+0.1*progChunk, progMsg);

	// Creates 'm_manipulation->m_pulses' and returns the pitch-tier
	//printf("Calling analyzePitch\n");
	m_manipulation->m_origPitchTier = m_manipulation->analyzePitch(callback, progOffset+0.1*progChunk, 0.9*progChunk);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("      +++ sec: %f - after analyzing pitchPRManipulation\n", sysTime);
	*/

	if (!m_manipulation->m_origPitchTier)
		printf("[ERROR] couldn't create m_manipulation->m_origPitchTier!!\n");

	//
	// inputBuf is always mono (not interleaved)
	//
	//m_manipulation->m_sound = snd->convertToMono(); // if already mono, duplicate it

	//
	// Cut the tail
	// TODO: Free the memory on tail?
	//
	if (tailLen > 0) {
		double tailLenSec = (double)tailLen / m_rate;
		m_sndEndSec -= tailLenSec;
		m_manipulation->removeTail(tailLenSec);
		m_manipulation->m_origPitchTier->removeTail(tailLenSec);
	}

	//
	// Release memory of m_manipulation->snd after analyzing
	//
	if (releaseSndMemory && sndRootDir) {
		m_manipulation->m_sound->releaseMemoryToFile(sndRootDir);
	}
	/*
	_pincmsg1("++++++++++++++++++++++++ origPitchTier ++++++++++++++++++++++++++++");
	for (long x = 1; x <= m_origPitchTier->m_setSize; x++) {
		_pincmsg4("+  i=%d, time=%f, freq=%f", x,
				m_origPitchTier->m_points[x]->m_time, m_origPitchTier->m_points[x]->m_value);
	}
	_pincmsg1("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	*/

	//printf("Princ::init - done\n");
	if (callback)
		callback(progOffset+1.0*progChunk, progMsg);
}

void Princ::changePlaybackSpeed(double speed)
{
	m_playbackSpeed = speed;
	if (m_manipulation)
		m_manipulation->updateDurationTierWithSpeed(m_playbackSpeed);
}

// New for v3.0
// Add new buffer after initialized
void Princ::addBuffer(float* inputBuf, long numSamples, long tailLen)
{
	long i;
	int numChannels = 1; 	// mono
	double xmin = 0.0; 		// start sec 
	double xmax = (double)(numSamples+tailLen) / m_rate;	// end time sec
	double dx = 1.0 / m_rate;		// period

	// Update m_sndEndSec
	m_sndEndSec += xmax;
	
	/*
	if (callback)
		callback(progOffset + 0.05*progChunk, progMsg);
		*/

	//printf("Princ::addBuffer - init PRSound\n");
	PRSound *snd = new PRSound();
	snd->init(numChannels, xmin, xmax, numSamples+tailLen, dx, xmin);

	// m_z : base 1 ,  inputBuf : base 0
	for (i = 1; i <= numSamples+tailLen; i++) {
		snd->m_z[1][i] = (double)inputBuf[i-1]; 
	}

	//printf("Princ::addBuffer - init PRManipulation\n");
	PRManipulation *manipulation = new PRManipulation(snd, m_quality);

	/*
	if (callback)
		callback(progOffset+0.1*progChunk, progMsg);
		*/

	// Creates 'manipulation->m_pulses' and returns the pitch-tier
	PRPitchTier *origPitchTier = manipulation->analyzePitch(NULL, 0, 0); // callback=NULL

	if (!origPitchTier)
		printf("[ERROR] couldn't create origPitchTier!!\n");

	//
	// inputBuf is always mono (not interleaved)
	//
	//m_manipulation->m_sound = snd->convertToMono(); // if already mono, duplicate it
	/*
	m_manipulation->m_sound = snd;
	m_manipulation->m_sound->subtractMean();
	*/

	/*
	_pincmsg1("++++++++++++++++++++++++ origPitchTier ++++++++++++++++++++++++++++");
	for (long x = 1; x <= m_origPitchTier->m_setSize; x++) {
		_pincmsg4("+  i=%d, time=%f, freq=%f", x,
				m_origPitchTier->m_points[x]->m_time, m_origPitchTier->m_points[x]->m_value);
	}
	_pincmsg1("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	*/
	//
	// Cut the tail - BEFORE appending
	//
	if (tailLen > 0) {
		double tailLenSec = (double)tailLen / m_rate;
		m_sndEndSec -= tailLenSec;
		manipulation->removeTail(tailLenSec);
		origPitchTier->removeTail(tailLenSec);
	}

	// Append manipulation to m_manipulation
	PRManipulation::appendManipulationToManipulation(manipulation, m_manipulation);

	// Append origPitchTier to m_origPitchTier
	PRRealTier::appendRealTierToRealTier(origPitchTier, m_manipulation->m_origPitchTier);

	//printf("  freeing manipulation\n");
	delete manipulation; // DO NOT use free()
	//printf("  freeing origPitchTier\n");
	delete origPitchTier;	// DO NOT use free()

	//
	// Cut the tail - NOT HERE
	//
	/*
	if (tailLen > 0) {
		double tailLenSec = (double)tailLen / m_rate;
		m_sndEndSec -= tailLenSec;
		m_manipulation->removeTail(tailLenSec);
		m_origPitchTier->removeTail(tailLenSec);
	}
	*/

	//printf("Princ::addBuffer - done\n");
	/*
	if (callback)
		callback(progOffset+1.0*progChunk, progMsg);
		*/
}

//
// Trim to shorten to match the given length
// Cut tail, if it needs to shorten.
//
void Princ::shortenLengthTo(long numSamples)
{
	double lenSec = (double)numSamples / m_rate;
	double cutLen = m_sndEndSec - lenSec;

	if (cutLen > 0.0) {
		m_sndEndSec -= cutLen;
		m_manipulation->removeTail(cutLen);
		m_manipulation->m_origPitchTier->removeTail(cutLen);
	}
}

double Princ::getTrackLenSec()
{
	return m_sndEndSec;
}

//
// Invoke repeatedly to fill m_manipulation->m_sound,
// followed by initWithAnalyticFiles()
// Analyzing won't happen here
// 
void Princ::addSound(float* inputBuf, long numSamples, char *sndRootDir)
{
	long i;
	int numChannels = 1; 	// mono
	double xmin = 0.0; 		// start sec 
	double xmax = (double)numSamples / m_rate;	// end time sec
	double dx = 1.0 / m_rate;		// period

	PRSound *snd = new PRSound();
	snd->init(numChannels, xmin, xmax, numSamples, dx, xmin);

	// m_z : base 1 ,  inputBuf : base 0
	for (i = 1; i <= numSamples; i++) {
		snd->m_z[1][i] = (double)inputBuf[i-1]; 
	}

	if (!m_manipulation) {
		// Create new manipulation
		m_manipulation = new PRManipulation(snd, m_quality);
		m_manipulation->updateDurationTierWithSpeed(m_playbackSpeed);
		// Release memory and use block files
		m_manipulation->m_sound->releaseMemoryToFile(sndRootDir);

		m_sndEndSec = xmax;
	} else {
		// Append sound into the existing manipulation
		PRSound::appendSoundToSound(snd, m_manipulation->m_sound);

		m_sndEndSec += xmax;
		// Free snd
		delete snd;
	}
}

//
// Init using input files
//
void Princ::initWithAnalyticFiles(
							//float* inputBuf, 
							//long numSamples, 
							char *pathToPulses, 
							char *pathToOrigTier,
							__progress_callback_t callback, float progOffset, float progChunk)
{
	//long i;
	char* progMsg = (char*)"Preparing...";
#if 0
	int numChannels = 1; 	// mono
	double xmin = 0.0; 		// start sec 
	double xmax = (double)numSamples / m_rate;	// end time sec
	double dx = 1.0 / m_rate;		// period

	m_sndEndSec = xmax;
	
	// invalidate:
	/*
	if (m_manipulation)
		delete m_manipulation;
	m_manipulation = NULL;
	*/

	//
	// Create PRSound instance and to send to PRManipulation
	//
	if (callback)
		callback(progOffset + 0.05*progChunk, progMsg);

	//printf("Princ::initWithAnalyticFiles - init PRSound\n");
	PRSound *snd = new PRSound();
	snd->init(numChannels, xmin, xmax, numSamples, dx, xmin);

	// m_z : base 1 ,  inputBuf : base 0
	for (i = 1; i <= numSamples; i++) {
		//_pincmsg3("    i=%d, val=%f", i, inputBuf[i-1]);
		snd->m_z[1][i] = (double)inputBuf[i-1]; 
	}

	//m_manipulation = new PRManipulation(snd->m_xmin, snd->m_xmax);
	//printf("Princ::initWithAnalyticFiles - init PRManipulation\n");
	m_manipulation = new PRManipulation(snd, m_quality);
#endif

	if (callback)
		callback(progOffset+0.1*progChunk, progMsg);

	//
	// Creates 'm_manipulation->m_pulses' and returns the pitch-tier
	/*
	m_origPitchTier = m_manipulation->analyzePitch(callback, progOffset+0.1*progChunk, 0.9*progChunk);

	*/
	m_manipulation->m_origPitchTier = m_manipulation->analyzePitchByFiles(pathToPulses, pathToOrigTier, callback, progOffset+0.1*progChunk, 0.9*progChunk);

	if (!m_manipulation->m_origPitchTier)
		printf("[ERROR] couldn't create m_manipulation->m_origPitchTier!!\n");

	//putPitchTier(m_activePitchTier);
	printf("Princ::initWithAnalyticFiles - done\n");
	if (callback)
		callback(progOffset+1.0*progChunk, progMsg);


	//
	// Print out the result
	//
#if 0
	printf("==== RESULT after initWithAnalyticFiles ====\n");

	printf("-- m_manipulation:\n");
	//printf("---- m_sound:\n");
	printf("---- m_pulses:\n");
	printf("------ m_xmin=%f\n", m_manipulation->m_pulses->m_xmin);
	printf("------ m_xmax=%f\n", m_manipulation->m_pulses->m_xmax);
	printf("------ m_maxnt=%ld\n", m_manipulation->m_pulses->m_maxnt);
	printf("------ m_nt=%ld\n", m_manipulation->m_pulses->m_nt);
	/*
	printf("-------- m_t:\n");
	for (i = 1; i <= m_manipulation->m_pulses->m_nt; i++) {
		printf("          %f\n", m_manipulation->m_pulses->m_t[i]);
	}
	*/
	if (m_manipulation->m_pitchTier) {
		printf("---- m_pitchTier:\n");
		printf("------ m_xmin=%f\n", m_manipulation->m_pitchTier->m_xmin);
		printf("------ m_xmax=%f\n", m_manipulation->m_pitchTier->m_xmax);
		printf("------ m_setSize=%ld\n", m_manipulation->m_pitchTier->m_setSize);
		/*
		printf("-------- m_points:\n");
		for (i = 1; i <= m_manipulation->m_pitchTier->m_setSize; i++) {
			printf("          m_time=%f, m_value=%f\n", 
									m_manipulation->m_pitchTier->m_points[i]->m_time,
									m_manipulation->m_pitchTier->m_points[i]->m_value);
		}
		*/
	}

	printf("-- m_origPitchTier:\n");
	printf("---- m_xmin=%f\n", m_origPitchTier->m_xmin);
	printf("---- m_xmax=%f\n", m_origPitchTier->m_xmax);
	printf("---- m_setSize=%ld\n", m_origPitchTier->m_setSize);
	/*
	printf("------ m_points:\n");
	for (i = 1; i <= m_origPitchTier->m_setSize; i++) {
		printf("        m_time=%f, m_value=%f\n", 
								m_origPitchTier->m_points[i]->m_time,
								m_origPitchTier->m_points[i]->m_value);
	}
	*/

	//printf("---- m_duration:\n");
	printf("---- m_xmin=%f\n", m_manipulation->m_xmin);
	printf("---- m_xmax=%f\n", m_manipulation->m_xmax);
	printf("-- m_sndEndSec=%f\n", m_sndEndSec);
#endif

}

//
// Save m_manipulation->m_pulses into buffer
// The caller must free the buffer
//
char* Princ::getPulsesAnalytic()
{
	printf("getPulsesAnalytic - start\n");
	long i;
	char *pBuffer = NULL;
	char *buffer;

	/* 
		PRPointProcess:
			double m_xmin;	
			double m_xmax;	
			long m_maxnt;
			long m_nt;
			double *m_t;
	*/

	PRPointProcess *pulses = m_manipulation->m_pulses;
	buffer = (char*)calloc(pulses->m_nt + 32, sizeof(double));

	pBuffer = buffer;
	*(double*)pBuffer = pulses->m_xmin;
	pBuffer += sizeof(double);
	*(double*)pBuffer = pulses->m_xmax;
	pBuffer += sizeof(double);
	*(long*)pBuffer = pulses->m_maxnt;
	pBuffer += sizeof(long);
	*(long*)pBuffer = pulses->m_nt;
	pBuffer += sizeof(long);

	for (i = 1; i <= pulses->m_nt; i++) { // Starts from '1'
		*(double*)pBuffer = pulses->m_t[i];
		pBuffer += sizeof(double);
	}

#if 0
	printf("getPulsesAnalytic - end\n");

	printf("==== RESULT after getPulsesAnalytic ====\n");

	printf("-- m_manipulation:\n");
	//printf("---- m_sound:\n");
	pBuffer = buffer;
	printf("---- m_pulses:\n");
	printf("------ m_xmin=%f\n", (double)(*(double*)pBuffer));
	pBuffer += sizeof(double);
	printf("------ m_xmax=%f\n", (double)(*(double*)pBuffer));
	pBuffer += sizeof(double);
	printf("------ m_maxnt=%ld\n", (long)(*(long*)pBuffer));
	pBuffer += sizeof(long);
	printf("------ m_nt=%ld\n", (long)(*(long*)pBuffer));
	pBuffer += sizeof(long);
	/*
	printf("-------- m_t:\n");
	for (i = 1; i <= m_manipulation->m_pulses->m_nt; i++) {
		printf("          %f\n", (double)(*(double*)pBuffer));
		pBuffer += sizeof(double);
	}
	*/
#endif
	return buffer;
}

//
// Save m_origPitchTier, or m_pitchTier0 into buffer
// The caller must free the buffer
//
char* Princ::getPitchTierBinary(bool origOrActive)
{
	printf("getPitchTierBinary - start\n");
	long i;
	char *pBuffer = NULL;
	char *buffer;

	PRPitchTier *pitchTier;
	if (origOrActive)
		pitchTier = m_manipulation->m_origPitchTier;
	else
		pitchTier = m_manipulation->m_pitchTier0;

	/* 
		PRPitchTier (PRRealTier):
			double m_xmin;		
			double m_xmax;		
			long m_setSize;
			RealPoint **m_points; // must be sorted all the time

			typedef struct {
				double m_time;
				double m_value;
			} RealPoint;
	*/
	buffer = (char*)calloc(pitchTier->m_setSize*2 + 32, sizeof(double));

	pBuffer = buffer;
	*(double*)pBuffer = pitchTier->m_xmin;
	pBuffer += sizeof(double);
	*(double*)pBuffer = pitchTier->m_xmax;
	pBuffer += sizeof(double);
	*(long*)pBuffer = pitchTier->m_setSize;
	pBuffer += sizeof(long);

	for (i = 1; i <= pitchTier->m_setSize; i++) {	// Starts from '1'
		//printf("    i=%ld\n", i);
		*(double*)pBuffer = pitchTier->m_points[i]->m_time;
		pBuffer += sizeof(double);
		*(double*)pBuffer = pitchTier->m_points[i]->m_value;
		pBuffer += sizeof(double);
		//printf("    time=%f, value=%f\n", m_manipulation->m_origPitchTier->m_points[i]->m_time, m_manipulation->m_origPitchTier->m_points[i]->m_value);
	}


	return buffer;
}

// Legacy code:
// Save m_origPitchTier into buffer
// The caller must free the buffer
//
char* Princ::getOrigTierAnalytic()
{
	printf("getOrigTierAnalytic - start\n");
	long i;
	char *pBuffer = NULL;
	char *buffer;
	/* 
		PRPitchTier (PRRealTier):
			double m_xmin;		
			double m_xmax;		
			long m_setSize;
			RealPoint **m_points; // must be sorted all the time

			typedef struct {
				double m_time;
				double m_value;
			} RealPoint;
	*/
	buffer = (char*)calloc(m_manipulation->m_origPitchTier->m_setSize*2 + 32, sizeof(double));

	pBuffer = buffer;
	*(double*)pBuffer = m_manipulation->m_origPitchTier->m_xmin;
	pBuffer += sizeof(double);
	*(double*)pBuffer = m_manipulation->m_origPitchTier->m_xmax;
	pBuffer += sizeof(double);
	*(long*)pBuffer = m_manipulation->m_origPitchTier->m_setSize;
	pBuffer += sizeof(long);

	for (i = 1; i <= m_manipulation->m_origPitchTier->m_setSize; i++) {	// Starts from '1'
		//printf("    i=%ld\n", i);
		*(double*)pBuffer = m_manipulation->m_origPitchTier->m_points[i]->m_time;
		pBuffer += sizeof(double);
		*(double*)pBuffer = m_manipulation->m_origPitchTier->m_points[i]->m_value;
		pBuffer += sizeof(double);
		//printf("    time=%f, value=%f\n", m_manipulation->m_origPitchTier->m_points[i]->m_time, m_manipulation->m_origPitchTier->m_points[i]->m_value);
	}

#if 0
	printf("getOrigTierAnalytic - end\n");

	printf("==== RESULT after getOrigTierAnalytic ====\n");

	pBuffer = buffer;
	printf("-- m_origPitchTier:\n");
	printf("---- m_xmin=%f\n", (double)(*(double*)pBuffer));
	pBuffer += sizeof(double);
	printf("---- m_xmax=%f\n", (double)(*(double*)pBuffer));
	pBuffer += sizeof(double);
	printf("---- m_setSize=%ld\n", (long)(*(long*)pBuffer));
	pBuffer += sizeof(long);
	/*
	printf("------ m_points:\n");
	for (i = 1; i <= m_origPitchTier->m_setSize; i++) {
		printf("        m_time=%f,", (double)(*(double*)pBuffer));
		pBuffer += sizeof(double);
		printf(" m_value=%f\n", (double)(*(double*)pBuffer));
		pBuffer += sizeof(double);
	}
	*/
#endif

	return buffer;
}

//
// v3.1
// Convert TierNode to PRPitchTier and apply
// Update active pitch tiers - primary & secondary
//
bool Princ::updateActivePitchTier(TierNode *nodeHead0, TierNode *nodeHead1)
{
	double time, freq;
	TierNode *curNode;

	if (!m_manipulation) {
		printf("[ERROR] updateActivePitchTier - not ready\n");
		return false;
	}

	if (!m_manipulation->m_origPitchTier) {
		printf("[ERROR] updateActivePitchTier - No m_manipulation->m_origPitchTier!\n");
		return false;
	}

	PRPitchTier *newTier0 = new PRPitchTier(m_manipulation->m_origPitchTier->m_xmin, m_manipulation->m_origPitchTier->m_xmax);
	PRPitchTier *newTier1 = NULL;
	if (nodeHead1)
		newTier1 = new PRPitchTier(m_manipulation->m_origPitchTier->m_xmin, m_manipulation->m_origPitchTier->m_xmax);

	//
	// newTier0
	//
	curNode = nodeHead0;
	while (curNode) {
		time = curNode->time;
		freq = curNode->value;
		newTier0->addPoint(time, freq);

		//printf("   addPoint: t=%f, f=%f\n", time, freq);

		curNode = curNode->next;
	}

	//
	// newTier1
	//
	if (newTier1) {
		curNode = nodeHead1;
		while (curNode) {
			time = curNode->time;
			freq = curNode->value;
			newTier1->addPoint(time, freq);

			curNode = curNode->next;
		}
	}

	//
	// duplicates:
	//
	m_manipulation->updatePitchTier(newTier0, newTier1);

	delete newTier0;
	if (newTier1)
		delete newTier1;

	return true;
}

// TODO: Do more elaboratedly
double Princ::getMatchingTimeFromOrigPitchTier(double origTime)
{
	const double maxDiffFromOrigTime = 0.1;	// sensitive parameter
	const double minDiff = 0.04;				// sensitive parameter
	double time, nextTime, prevTime;

	if (!m_manipulation) {
		printf("[ERROR] updateActivePitchTier - No m_manipulation!\n");
		return origTime;
	}

	time = 0.0;
	prevTime = 0.0;
    // base 1
	for (long i = 1; i <= m_manipulation->m_pulses->m_nt; i++) {
		nextTime = m_manipulation->m_pulses->m_t[i];

		if (nextTime > origTime)
			break;

		// Mark if it's the beginning of a new series of pulses
		if (nextTime - prevTime > minDiff)
			time = nextTime;

		prevTime = nextTime;
	}

	if (origTime > time && origTime - time < maxDiffFromOrigTime)
		return time;		// the nearest 'head' time
	else
		return prevTime;	// the nearest time
		//return origTime;

}

#if 0 // DEPRECATED
// precondition: m_manipulation not NULL
// input string: [sec]:[freq]|[sec]:[freq]... e.g. 0.0:440.0|2.4435:234.3
bool Princ::updateActivePitchTier(char *_tierStr, int lenStr)
{
	double time, freq;
	double prevFreq = 0.0;
	double prevTime = 0.0;
	double minWidth;
	// sensitive parameter
	//const double diffSec = 0.02;

	// sensitive parameter
	// Decides how steep between notes
	const double minNoteTime = 0.1;
	//printf("Princ::updateActivePitchTier - start\n");

	if (!_tierStr || !m_manipulation) {
		printf("[ERROR] updateActivePitchTier - not ready\n");
		return false;
	}

	// Must duplicate before 'strtok'
	char *tierStr = (char*)malloc(lenStr);
	strcpy(tierStr, _tierStr);

	printf("updateActivePitchTier: tierStr=%s\n", (char*)tierStr);
	//printf("    creating newTier\n");
	if (!m_origPitchTier) {
		printf("[ERROR] updateActivePitchTier - No m_origPitchTier!\n");
		return false;
	}

	PRPitchTier *newTier = new PRPitchTier(m_origPitchTier->m_xmin, m_origPitchTier->m_xmax);

	//printf("    start parsing\n");
	bool firstNote = true;
	char *tok = strtok((char*)tierStr, ":");
	while (tok) {
		// Parse string
		time = (double)atof(tok);
		tok = strtok(NULL, "|");
		if (tok)
			freq = (double)atof(tok);
		else
			break;

		//printf("    time=%f, freq=%f\n", time, freq);
		// Add points
		if (time + minNoteTime > m_origPitchTier->m_xmax)
			minWidth = m_origPitchTier->m_xmax - time;
		else
			minWidth = minNoteTime;

		//
		// add another point to maintain prev note upto now
		// by making steep slopes
		//
		if (!firstNote && prevFreq > 0 && prevFreq != freq) {
			if (time - prevTime > minWidth) {
				newTier->addPoint(time - minWidth*0.5, prevFreq);
			}
		}

		if (firstNote) {
			newTier->addPoint(0.0, freq);
			firstNote = false;
		} else {
			// Apply m_addHeadSec
			//newTier->addPoint(time + m_addHeadSec, freq);
			newTier->addPoint(time, freq);
		}

		prevFreq = freq;
		prevTime = time;
		
		tok = strtok(NULL, ":");
	}

	if (prevFreq > 0.0)
		newTier->addPoint(m_origPitchTier->m_xmax, prevFreq); 

	// duplicates:
	//printf("    calling m_manipulation->updatePitchTier\n");
	m_manipulation->updatePitchTier(newTier);

	//_pincmsg1("deleting newTier");
	free(tierStr);
	delete newTier;
	//printf("Princ::updateActivePitchTier - end\n");
	return true;
}
#endif
//
// output: 0-based
// tierIndex: -1: original, 0-primary, 1-secondary(i.e.chorus)
//
// origDiffSemitones : Effective only when tierIndex == -1
float* Princ::processBuffer(double startSec, 
									double lenSec, 
									long *retBufLen, 
									int tierIndex,
									int origDiffSemitones)
									//bool getOrig) 
{
	//_pincmsg1("Princ::processBuffer");
	if (!m_manipulation) {
		printf("[WARNING] Princ::processBuffer - NO m_manipulation!\n");
		*retBufLen = 0;
		return NULL;
	}

	if (lenSec < gPitchTimeStep*2) {
		printf("[WARNING] Princ::processBuffer() lenSec is too short\n");
		*retBufLen = 0;
		return NULL;
	}

	// 
	// Neglect samples upto m_addHeadSec
	//
	double addHeadSec = m_addHeadSec;
	if (startSec - addHeadSec < 0)
		addHeadSec = startSec;

	long procBufLen = 0;
	float *procBuf = m_manipulation->playPart(startSec-addHeadSec,
													startSec+ m_addHeadSec + m_addTailSec+ lenSec,
													Manipulation_OVERLAPADD,
													&procBufLen,
													tierIndex,
													origDiffSemitones);
													//getOrig);

	long lenTail = (long)(m_addTailSec * m_rate);
	long lenHead = (long)(m_addHeadSec * m_rate);
	long resBufLen = procBufLen - lenHead - lenTail;
	if (resBufLen <= 0) {
		printf("[ERROR] processBuffer: empty result!: procBufLen=%ld, resBufLen=%ld\n", procBufLen, resBufLen);
		return NULL;
	}
	float *resBuf = (float*)malloc(resBufLen*sizeof(float));
	for (long i = 0; i < resBufLen; i++) {
		resBuf[i] = procBuf[i + lenHead];
	}

	*retBufLen = resBufLen;

	free(procBuf);
	return resBuf;
}

// Frequecy on updated pitch-tier
double Princ::getFreqAt(double sec)
{
	return m_manipulation->getFreqAtSec(sec);
}

double Princ::getOrigFreqAt(double sec)
{
	if (m_manipulation->m_origPitchTier)
		return m_manipulation->m_origPitchTier->getValueAtTime(sec);
	else
		return 0.0;
}

// DEPRECATED?
double Princ::getPitch()
{
	double freq;
	if (!m_manipulation || m_sndEndSec <= 0.0)
		return 0.0;

	double timeStep = m_sndEndSec;

	PRPitch *pitchObj = PRManipulation::Sound_to_Pitch(
																m_manipulation->m_sound, 
																timeStep, 	// sec
																MIN_PITCH,  // hz
																MAX_PITCH,	// hz
																NULL, 0, 0
																); 

	freq = pitchObj->m_frame[1]->m_candidate[1]->m_frequency;
	delete pitchObj;

	return freq;
}

// DEPRECATED:
// public
float* Princ::initAndProcessBuffer(float *inputBuf, 
													long numSamples, 
													long *retLen,
													float *freq,
													double paddingT
													)
{
#if 0
	float sysTime;
	float* outBuf = NULL;
	int scaleNote;
	double newFreq;
	deinit();

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++ sec: %f - initAndProcessBuffer - start\n", sysTime);
	*/

	init(inputBuf, numSamples, NULL, 0, 0);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++ sec: %f - after init()\n", sysTime);
	*/

	// Get original pitch
	double width = m_origPitchTier->m_xmax - m_origPitchTier->m_xmin;
	double origFreq = m_origPitchTier->getValueAtTime(m_origPitchTier->m_xmin+width/2); // average 
	//  setupScaleNoteList() must be set
	if (*freq > 0.0) {
		// if input frequency is given, apply it
		newFreq = *freq;
	} else {
		scaleNote = getNearestScaleNoteByFreq(origFreq, 0);
		newFreq = Princ::midiToFreq(scaleNote);
	}


	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++ sec: %f - before crerating newTier()\n", sysTime);
	*/

	PRPitchTier *newTier = new PRPitchTier(m_origPitchTier->m_xmin, m_origPitchTier->m_xmax);
	newTier->addPoint(0, newFreq);

	// duplicates:
	m_manipulation->updatePitchTier(newTier);
	delete newTier;

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++ sec: %f - after crerating newTier()\n", sysTime);
	*/

	//outBuf = processBuffer(0, m_sndEndSec, retLen);
	outBuf = processBuffer(paddingT, m_sndEndSec - 2.0*paddingT, retLen);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++ sec: %f - after processBuffer()\n", sysTime);
	*/

	*freq = newFreq;
	return outBuf;
#endif
	return NULL;
}


// public
#if 0
void Princ::renderOutput(int dSemitones, 
										int option, 
										char* newChordList, 
										WaveTrack *outWave, 
										__progress_callback_t callback, float progOffset, float progChunk) 
{
	/*
	_pincmsg1("======== renderOutput ========");
	_pincmsg4("  dSemitones=%d, option=%d, chordList=%s", dSemitones, option, newChordList);
	*/
	char *progMsg = "Rendering...";
	long retBufLen;

	updatePlan(newChordList, dSemitones, option);

	long bufLen = (long)(m_sndEndSec * m_rate);

	double curSec = 0.0;
	double lenSec = 0.5;
	float *outBuf;
	while(curSec < m_sndEndSec) {
		//outBuf = processBuffer(0.0, m_sndEndSec, &retBufLen);
		if (curSec + lenSec > m_sndEndSec)
			lenSec = m_sndEndSec - curSec;
		outBuf = processBuffer(curSec, lenSec, &retBufLen);

		/*
		_pincmsg3("    ++++++++ renderOutput: requestedLen=%d, returedLen=%d",
												(long)(lenSec*m_rate), retBufLen);
												*/
		if (retBufLen < 1) {
			if (outBuf)
				free(outBuf);
			break;
		}
		outWave->Append((samplePtr)outBuf, floatSample, retBufLen);

		if (outBuf)
			free(outBuf);

		//_pincmsg2("    retBufLen = %d", retBufLen);
		curSec += lenSec;
		//curSec += (double)retBufLen / m_rate;

		// update callback
		if (callback)
			callback(progOffset + progChunk * (curSec / m_sndEndSec), progMsg);
	}
	/*
	outBuf = processBuffer(0.0, m_sndEndSec, &retBufLen);
	outWave->Append((samplePtr)outBuf, floatSample, retBufLen);
	*/

	outWave->Flush();


#if 0
	//int bufLen = 16384; // sensitive parameter?
	int bufLen = 1024*4; // sensitive parameter?
	long remainLen = mInputWaveLen;
	if (bufLen > remainLen)
		bufLen = remainLen;

	int *buf32 = new int[bufLen];
	short *buf16 = new short[bufLen/2];

	int timecode = 0;
	if (mProgressCallback)
		mProgressCallback(progOffset, progText);
	while(remainLen > 0) {
		ProcessBuffer(buf32, bufLen, timecode);

		for (int i = 0; i < bufLen/2; i++) {
			buf16[i] = (short) MIN(32767, MAX(-32768, buf32[i]));
		}

		// Append to output
		mOutputWave->Append((samplePtr)buf16, int16Sample, bufLen/2);

		timecode += bufLen * 8 / 16;

		remainLen -= bufLen/2;
		if (bufLen > remainLen) {
			// Don't reassign 'bufLen'!! - simply do nothing!
			/*
			memset(buf32, 0, bufLen*sizeof(int));
			bufLen = remainLen;
			*/
		}

		// update progress
		if (mProgressCallback) {
			double totalLen = (double)mInputWaveLen;
			mProgressCallback(progOffset + progChunk*(totalLen-(float)remainLen)/totalLen, progText);
		}
	}

	mOutputWave->Flush();

	delete [] buf32;
	delete [] buf16;

	long newLen = mOutputWave->GetEndSamplePos();
	if (newLen > mRate*0.1 || (float)abs(newLen-mInputWaveLen) > mRate*0.1) {
		if (mProgressCallback)
			mProgressCallback(progOffset + progChunk, "Finalizing...");
		//
		//	If the two lenhths are differ by more than 0.1 sec,
		// finalize by resampling to make the lengths same
		// Pitch will be also tuned
		//
		ResampleToFixLen();
	}
#endif
}
#endif

TierNode* Princ::getOrigPitchTier()
{
	double time = 0.0;
	double freq = 0.0;
	TierNode *node;
	TierNode *nodeHead = NULL;
	TierNode *prevNode = NULL;

	if (!m_manipulation || !m_manipulation->m_origPitchTier) {
		printf("[WARNING] Princ::getOrigPitchTier() returns NULL!\n");
   	return NULL;
	}
    
    // base 1
	for (long i = 1; i <= m_manipulation->m_pulses->m_nt; i++) {
		time = m_manipulation->m_pulses->m_t[i];
		freq = m_manipulation->m_origPitchTier->getValueAtTime(time);

		node = new TierNode();
		node->time = time;
		node->value = freq;
		node->next = NULL;

		if (prevNode)
			prevNode->next = node;

		if (!nodeHead)
			nodeHead = node;

		prevNode = node;

	}

	//
	// Workaround - Do not return null
	//
	if (nodeHead == NULL) {
		// Create a dummy
		nodeHead = new TierNode();
		nodeHead->time = 0.0;
		nodeHead->value = 1;
		nodeHead->next = NULL;
	}

	return nodeHead;
}

double Princ::getTimeStepSec()
{
	return gPitchTimeStep;
}

// Get orig pitch tier in string format: "0.0:440.3|0.45:345.33|1.3:564.23" 
// DEPRECATED: 3.1 -> Use getOrigPitchTier()
void Princ::origPitchTierToStr(char *outStr, int *numNotes, double minNoteLenSec)
{
	double width;
	double curFreq, prevFreq;
	double t0 = 0.0;
	int cntNotes = 0;

	char tempStr[1000];
	memset(tempStr, 0, 1000*sizeof(char));

	prevFreq = 0.0;
	if (m_manipulation->m_origPitchTier) {
		while (t0 < m_manipulation->m_origPitchTier->m_xmax) 
		{
			if (t0 + minNoteLenSec > m_manipulation->m_origPitchTier->m_xmax)
				width = m_manipulation->m_origPitchTier->m_xmax - t0;
			else
				width = minNoteLenSec;

			//curFreq = m_origPitchTier->getMean_points(t0, t0+width);
			curFreq = m_manipulation->m_origPitchTier->getValueAtTime(t0+width/2); // average 

			if (curFreq < 0.0)
				curFreq = 0.0;
			if (curFreq > 1200.0)
				curFreq = prevFreq;

			if (t0 == 0.0 || curFreq != prevFreq) {
				// Add string
				if (t0 == 0.0) { // The first note
					sprintf(outStr, "%.2f:%.2f", t0, curFreq);

				} else {
					sprintf(tempStr, "|%.2f:%.2f", t0, curFreq);
					strcat(outStr, tempStr);
				}
				cntNotes++;
			}

			prevFreq = curFreq;
			t0 += width;
		}
	}

	*numNotes = cntNotes;
}


