#ifndef __PR_SND_BLOCK__
#define __PR_SND_BLOCK__

class PRSndBlock
{
public:
	long m_len;
	long m_pos;		// the position of this block in the whole samples

	PRSndBlock(double *buf, long len, char *path, long pos);
	~PRSndBlock();

	double getSample(long at);
	void releaseMemory();

private:
	double *m_buffer;
	char *m_file;

	void createFile(double *buf, long len, char *path);

};
#endif
