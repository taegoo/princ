/* SVD.c
 *
 * Copyright (C) 1994-2008 David Weenink
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 djmw 20010719
 djmw 20020408 GPL + cosmetic changes.
 djmw 20020415 +SVD_synthesize.
 djmw 20030624 Removed NRC svd calls.
 djmw 20030825 Removed praat_USE_LAPACK external variable.
 djmw 20031018 Removed  bug in SVD_solve that caused incorrect output when nrow > ncol
 djmw 20031101 Changed documentation in SVD_compute + bug correction in SVD_synthesize.
 djmw 20031111 Added GSVD_create_d.
 djmw 20051201 Adapt for numberOfRows < numberOfColumns
 djmw 20060810 Removed #include praat.h
 djmw 20061212 Changed info to Melder_writeLine<x> format.
 djmw 20070102 Removed the #include "TableOfReal.h"
 djmw 20071012 Added: o_CAN_WRITE_AS_ENCODING.h
*/

#include <stdlib.h>
#include "SVD.h"
//#include "NUMlapack.h"
#include "NUMmachar.h"
#include "NUMclapack.h"
#include "NUMcblas.h"

#define MAX(m,n) ((m) > (n) ? (m) : (n))
#define MIN(m,n) ((m) < (n) ? (m) : (n))

extern machar_Table NUMfpp;
static void NUMtranspose_d (double **m, long n);

/* if A=UDV' then A' = (UDV')'=VDU' */
static void SVD_transpose (SVD *svd)
{
	long tmpl = svd->numberOfRows;
	double **tmpd = svd->u;

	svd->u = svd->v;
	svd->v = tmpd;
	svd->numberOfRows = svd->numberOfColumns;
	svd->numberOfColumns = tmpl;
}


/*
	m >=n, mxn matrix A has svd UDV', where u is mxn, D is n and V is nxn.
	m < n, mxn matrix A. Consider A' with svd (UDV')'= VDU', where v is mxm, D is m and U' is mxn
*/
int SVD_init (SVD* svd, long numberOfRows, long numberOfColumns)
{
	long mn_min = MIN(numberOfRows, numberOfColumns);
	svd->numberOfRows = numberOfRows;
	svd->numberOfColumns = numberOfColumns;
	if (!NUMfpp) 
		NUMmachar ();

	svd->tolerance = NUMfpp->eps * MAX (numberOfRows, numberOfColumns);
	if (((svd->u = NUMdmatrix (1, numberOfRows, 1, mn_min)) == NULL) ||
		((svd->v = NUMdmatrix (1, numberOfColumns, 1, mn_min)) == NULL) ||
		((svd->d = NUMdvector (1, mn_min)) == NULL)) return 0;
	return 1;
}

SVD* SVD_create (long numberOfRows, long numberOfColumns)
{
	SVD *svd = (SVD*)malloc(sizeof(SVD));
	
	SVD_init (svd, numberOfRows, numberOfColumns);

	return svd;
}

SVD* SVD_create_d (double **m, long numberOfRows, long numberOfColumns)
{
	SVD* svd = SVD_create (numberOfRows, numberOfColumns);
	
	SVD_svd_d (svd, m);
	return svd;
}

SVD* SVD_create_f (float **m, long numberOfRows, long numberOfColumns)
{
	SVD* svd = SVD_create (numberOfRows, numberOfColumns);
	 
	SVD_svd_f (svd, m);
	return svd;
}

int SVD_svd_d (SVD* svd, double **m)
{
	long i, j;
	if (svd->numberOfRows >= svd->numberOfColumns)
	{
		/* Store m in u */
		for (i = 1; i <= svd->numberOfRows; i++)
		{
			for (j = 1; j <= svd->numberOfColumns; j++) 
				svd->u[i][j] = m[i][j];
		}
	}
	else
	{
		/* Store m transposed in v */
		for (i = 1; i <= svd->numberOfRows; i++ )
		{
			for (j = 1; j <= svd->numberOfColumns; j++) 
				svd->v[j][i] = m[i][j];
		}
	}
	return SVD_compute (svd);
}

int SVD_svd_f (SVD* svd, float **m)
{
	long i, j;
	if (svd->numberOfRows >= svd->numberOfColumns)
	{
		/* Store in u */
		for (i = 1; i <= svd->numberOfRows; i++)
		{
			for (j = 1; j <= svd->numberOfColumns; j++) 
				svd->u[j][i] = m[i][j];
		}
	}
	else
	{
		/* Store transposed in v */
		for (i = 1; i <= svd->numberOfRows; i++ )
		{
			for (j = 1; j <= svd->numberOfColumns; j++) 
				svd->v[i][j] = m[j][i];
		}
	}
	
	return SVD_compute (svd);
}

void SVD_setTolerance (SVD* svd, double tolerance)
{
	svd->tolerance = tolerance;
}

double SVD_getTolerance (SVD* svd)
{
	return svd->tolerance;
}

static void NUMtranspose_d (double **m, long n)
{
	long i, j;
	for (i = 1; i <= n - 1; i++)
	{
		for (j = i + 1; j <= n; j++)
		{
			double t = m[i][j];
			m[i][j] = m[j][i];
			m[j][i] = t;
		}
	}
}


/*
	Compute svd(A) = U D Vt.
	The svd routine from CLAPACK uses (fortran) column major storage, while	C uses row major storage.
	To solve the problem above we have to transpose the matrix A, calculate the
	solution and transpose the U and Vt matrices of the solution.
	However, if we solve the transposed problem svd(A') = V D U', we have less work to do:
	We may call the algorithm with reverted row/column dimensions, and we switch the U and V'
	output arguments.
	The only thing that we have to do afterwards is transposing the (small) V matrix
	because the SVD-object has row vectors in v.
	The sv's are already sorted.
	int NUMlapack_dgesvd (char *jobu, char *jobvt, long *m, long *n, double *a, long *lda,
		double *s, double *u, long *ldu, double *vt, long *ldvt, double *work,
		long *lwork, long *info);
*/
int SVD_compute (SVD* svd)
{
	char jobu = 'S', jobvt = 'O';
	long m, n, lda, ldu, ldvt, info, lwork = -1;
	double *work = NULL, wt[2];
	int transpose = svd->numberOfRows < svd->numberOfColumns;

	/* transpose: if rows < cols then data in v */
	if (transpose) 
		SVD_transpose (svd);
	
	lda = ldu = ldvt = m = svd->numberOfColumns;
	n = svd->numberOfRows;
	
	(void) NUMlapack_dgesvd (&jobu, &jobvt, &m, &n, &svd->u[1][1], &lda, &svd->d[1], &svd->v[1][1], &ldu, NULL, &ldvt, wt, &lwork, &info);

	if (info != 0) goto end;

	lwork = wt[0];
	work = NUMdvector (1, lwork);
	if (work == NULL) goto end;
	(void) NUMlapack_dgesvd(&jobu, &jobvt, &m, &n, &svd->u[1][1], &lda, &svd->d[1], &svd->v[1][1], &ldu, NULL, &ldvt, &work[1], &lwork, &info);

	NUMtranspose_d (svd->v, MIN(m, n));

	NUMdvector_free (work, 1);
end:
	if (transpose) SVD_transpose (svd);
	return info == 0;
}


int SVD_solve (SVD* svd, double b[], double x[])
{
	double *t, tmp;
	long i, j, mn_min = MIN (svd->numberOfRows, svd->numberOfColumns);

	t = NUMdvector (1, mn_min);
	if (t == NULL) return 0;
	
	/*  Solve UDV' x = b.
		Solution: x = V D^-1 U' b */

	for (j = 1; j <= mn_min; j++)
	{
		tmp = 0;
		if (svd->d[j] > 0)
		{
			for (i = 1; i <= svd->numberOfRows; i++)
			{
				tmp += svd->u[i][j] * b[i];
			}
			tmp /= svd->d[j];
		}
		t[j] = tmp;
	}

	for (j = 1; j <= svd->numberOfColumns; j++)
	{
		tmp = 0;
		for (i = 1; i <= mn_min; i++)
		{
			tmp += svd->v[j][i] * t[i];
		}
		x[j] = tmp;
	}
	
	NUMdvector_free (t, 1);
	return 1;
}

#if 0 // TODO:
int SVD_sort (SVD* svd)
{
	SVD* thee = NULL; 
	long i, j, mn_min = MIN (svd->numberOfRows, svd->numberOfColumns);
	long *index = NULL;
	
	if (((thee = Data_copy (svd)) == NULL) ||
		((index = NUMlvector (1, mn_min)) == NULL)) 
		goto end;
	NUMindexx (svd->d, mn_min, index);
			
	for (j = 1; j <= mn_min; j++)
	{
		long from = index[mn_min - j + 1];
		svd->d[j] = thee->d[from];
		for (i = 1; i <= svd->numberOfRows; i++) 
			svd->u[i][j] = thee->u[i][from];
		for (i = 1; i <= svd->numberOfColumns; i++) 
			svd->v[i][j] = thee->v[i][from];
	}
end:
	if (thee)
		free (thee);
	NUMlvector_free (index, 1);
	//return ! Melder_hasError ();
	return 1;
}
#endif

long SVD_zeroSmallSingularValues (SVD* svd, double tolerance)
{
	long i, numberOfZeroed = 0, mn_min = MIN (svd->numberOfRows, svd->numberOfColumns);
	double dmax = svd->d[1];
	
	if (tolerance == 0) 
		tolerance = svd->tolerance;
	for (i = 2; i <= mn_min; i++)
	{
		if (svd->d[i] > dmax) 
			dmax = svd->d[i];
	}
	for (i = 1; i <= mn_min; i++)
	{
		if (svd->d[i] < dmax * tolerance)
		{
			svd->d[i] = 0; numberOfZeroed++;
		}
	}
	return numberOfZeroed;
}


long SVD_getRank (SVD* svd)
{
	long i, rank = 0, mn_min = MIN (svd->numberOfRows, svd->numberOfColumns);
	for (i = 1; i <= mn_min; i++)
	{
		if (svd->d[i] > 0) 
			rank++;
	}
	return rank;
}

/*
	SVD of A = U D V'.
	If u[i] is the i-th column vector of U and v[i] the i-th column vector of V and s[i] the i-th singular value,
	we can write the svd expansion  A = sum_{i=1}^n {d[i] u[i] v[i]'}.
	Golub & van Loan, 3rd ed, p 71.
*/
int SVD_synthesize (SVD* svd, long sv_from, long sv_to, double **m)
{
	long i, j, k, mn_min = MIN (svd->numberOfRows, svd->numberOfColumns);

	if (sv_to == 0) 
		sv_to = mn_min;

	if (sv_from > sv_to || sv_from < 1 || sv_to > mn_min) 
		return 0;

	for (i = 1; i <= svd->numberOfRows; i++)
	{
		for (j = 1; j <= svd->numberOfColumns; j++) 
			m[i][j] = 0;
	}

	for (k = sv_from; k <= sv_to; k++)
	{
		for (i = 1; i <= svd->numberOfRows; i++)
		{
			for (j = 1; j <= svd->numberOfColumns; j++)
			{
				m[i][j] += svd->d[k] * svd->u[i][k] * svd->v[j][k];
			}
		}
	}
	return 1;
}


GSVD* GSVD_create (long numberOfColumns)
{
	GSVD* gsvd = (GSVD*)malloc(sizeof(GSVD));

	gsvd->numberOfColumns = numberOfColumns;

	if (((gsvd->q = NUMdmatrix(1, numberOfColumns, 1, numberOfColumns)) == NULL) ||
		((gsvd->r = NUMdmatrix(1, numberOfColumns, 1, numberOfColumns)) == NULL) ||
		((gsvd->d1 = NUMdvector (1, numberOfColumns)) == NULL) ||
		((gsvd->d2 = NUMdvector (1, numberOfColumns)) == NULL)) {
		free (gsvd);
	}
	return gsvd;
}

GSVD* GSVD_create_d (double **m1, long numberOfRows1, long numberOfColumns,
							double **m2, long numberOfRows2)
{
	long m = numberOfRows1, n = numberOfColumns, p = numberOfRows2, kl;
	long i, j, k, l, lwork = MAX (MAX (3*n, m), p) + n, *iwork = NULL, info;
	double **a = NULL, **b = NULL, *work = NULL, *pr;
	double *alpha = NULL, *beta = NULL, **q = NULL;
	GSVD* gsvd = NULL;

	/*
		Store the matrices a and b as column major!
	*/
	if (((a = NUMdmatrix_transpose (m1, m, n)) == NULL) ||
		((b = NUMdmatrix_transpose (m2, p, n)) == NULL) ||
		((alpha = NUMdvector (1, n)) == NULL) ||
		((beta = NUMdvector (1, n)) == NULL) ||
		((q = NUMdmatrix (1, n, 1, n)) == NULL) ||
		((work = NUMdvector (1, lwork)) == NULL) ||
		((iwork = NUMlvector (1, n)) == NULL)) goto end;

	{
		char jobu1 = 'N', jobu2 = 'N', jobq = 'Q';
		(void) NUMlapack_dggsvd (&jobu1, &jobu2, &jobq, &m, &n, &p, &k, &l,
			&a[1][1], &m, &b[1][1], &p, &alpha[1], &beta[1], NULL, &m,
			NULL, &p, &q[1][1], &n, &work[1], &iwork[1], &info);
		if (info != 0)
		{
			goto end;
		}
	}

	kl = k + l;
	gsvd = GSVD_create (kl);
	if (gsvd == NULL) 
		return NULL;
	
	for (i = 1; i <= kl; i++)
	{
		gsvd->d1[i] = alpha[i];
		gsvd->d2[i] = beta[i];
	}

	/*
		Transpose q
	*/
	for (i = 1; i <= n; i++)
	{
		for (j = i + 1; j <= n; j++)
		{
			gsvd->q[i][j] = q[j][i];
			gsvd->q[j][i] = q[i][j];
		}
		gsvd->q[i][i] = q[i][i];
	}
	/*
		Get R from a(1:k+l,n-k-l+1:n)
	*/
	pr = &a[1][1];
	for (i = 1; i <= kl; i++)
	{
		for (j = i; j <= kl; j++)
		{
			gsvd->r[i][j] = pr[i - 1 + (n - kl + j - 1) * m]; /* from col-major */
		}
	}
end:
	NUMdmatrix_free (q, 1, 1); NUMdvector_free (alpha,1);
	NUMdvector_free (beta,1); NUMlvector_free (iwork, 1);
	NUMdvector_free (work, 1); NUMdmatrix_free (b, 1, 1);
	NUMdmatrix_free (a, 1, 1);

	return gsvd;
}

void GSVD_setTolerance (GSVD* gsvd, double tolerance)
{
	gsvd->tolerance = tolerance;
}

double GSVD_getTolerance (GSVD* gsvd)
{
	return gsvd->tolerance;
}

#undef MAX
#undef MIN

/* End of file SVD.c */
