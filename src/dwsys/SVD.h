#ifndef _SVD_h_
#define _SVD_h_
/* 
	SVD.h
*/

#include "NUM2.h"

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef WIN32
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#else
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif
#endif

__BEGIN_DECLS
typedef struct {
	double tolerance;
	long numberOfRows;
	long numberOfColumns;
	double **u; /* column vectors */
	double **v; /* row vectors */ 
	double *d; /* decreasing singular values */
} SVD;

typedef struct {
	double tolerance;
	long numberOfColumns;
	double **q;
	double **r; 
	double *d1;
	double *d2;
} GSVD;


int SVD_init (SVD* svd, long numberOfRows, long numberOfColumns);

SVD* SVD_create (long numberOfRows, long numberOfColumns);
/*
	my tolerance = eps * MAX (numberOfRows, numberOfColumns) 
	where eps is the floating point precision, approximately 2.2e-16
*/

SVD* SVD_create_d (double **m, long numberOfRows, long numberOfColumns);
SVD* SVD_create_f (float **m, long numberOfRows, long numberOfColumns);
/*
	Copy matrix into svd->u and calculate U D V'
*/

int SVD_svd_d (SVD *svd, double **m);
int SVD_svd_f (SVD *svd, float **m);
/*
	Perform SVD analysis on matrix M, i.e., decompose M as M = UDV'.
	Watch out: dataType contains V, not V' !!
*/

int SVD_compute (SVD *svd);

int SVD_solve (SVD *svd, double b[], double x[]);
/* Solve Ax = b */

//int SVD_sort (SVD *svd);
/*
	Sort singular values (and corresponding column vectors of U and V) in decreasing order.
*/

void SVD_setTolerance (SVD *svd, double tolerance);
double SVD_getTolerance (SVD *svd);

long SVD_zeroSmallSingularValues (SVD *svd, double tolerance);
/*
	Zero singular values smaller than maximum_singular_value * tolerance
	If tolerance == 0 then then my tolerance will be used.
	Return the number of s.v.'s zeroed.
*/

int SVD_synthesize (SVD *svd, long sv_from, long sv_to, double **m);
/*
	Synthesize matrix as U D(sv_from:sv_to) V'.
	(The synthesized matrix is an approximation of the svd'ed matrix with 
	only a selected number of sv's). 
	Matrix m is [numberOfRows x numberOfColumns] and must be allocated
	by caller!
*/

long SVD_getRank (SVD *svd);

GSVD* GSVD_create (long numberOfColumns);

GSVD* GSVD_create_d (double **m1, long numberOfRows1, long numberOfColumns,
							double **m2, long numberOfRows2);

void GSVD_setTolerance (GSVD* gsvd, double tolerance);

double GSVD_getTolerance (GSVD* gsvd);

__END_DECLS
#endif /* _SVD_h_ */
