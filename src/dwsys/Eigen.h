#ifndef _Eigen_h_
#define _Eigen_h_
/* 
	Eigen.h

	- Ported from dwsys/Eigen.h , dwsys/Eigen_def.h
*/
#undef __BEGIN_DECLS
#undef __END_DECLS

#ifdef WIN32
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#else
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif
#endif


__BEGIN_DECLS
typedef struct {
	long numberOfEigenvalues;	
	long dimension;	
	double *eigenvalues;	
	double **eigenvectors; /* eigenvectors stored in row */
} Eigen;

Eigen* Eigen_create (long numberOfEigenvalues, long dimension);

int Eigen_init (Eigen* eig, long numberOfEigenvalues, long dimension);

int Eigen_initFromSymmetricMatrix_f (Eigen* eig, float **a, long n);
int Eigen_initFromSymmetricMatrix (Eigen* eig, double **a, long n);

int Eigen_initFromSquareRoot (Eigen* eig, double **a, long numberOfRows, long numberOfColumns);
/*
	Calculate eigenstructure for symmetric matrix A'A (e.g. covariance matrix),
	when only A is given.
	Precondition: numberOfRows > 1
	Method: SVD.
*/

int Eigen_initFromSquareRootPair (Eigen* eig, double **a, long numberOfRows,
								long numberOfColumns, double **b, long numberOfRows_b);
/*
	Calculate eigenstructure for A'Ax - lambda B'Bx = 0
	Preconditions: numberOfRows >= numberOfColumns &&
		numberOfRows_b >= numberOfColumns
	Method: Generalized SVD.
*/

long Eigen_getNumberOfEigenvectors (Eigen* eig);

long Eigen_getDimensionOfComponents (Eigen* eig);

double Eigen_getCumulativeContributionOfComponents (Eigen* eig, long from, long to);

long Eigen_getDimensionOfFraction (Eigen* eig, double fraction);

double Eigen_getEigenvectorElement (Eigen* eig, long ivec, long element);

double Eigen_getSumOfEigenvalues (Eigen* eig, long from, long to);


void Eigen_sort (Eigen* eig);
/*
	Sort eigenvalues and corresponding eigenvectors in decreasing order.
*/

void Eigen_invertEigenvector (Eigen* eig, long ivec);

#if 0
void Eigen_drawEigenvalues (I, Graphics g, long first, long last, double ymin, double ymax, 
	int fractionOfTotal, int cumulative, double size_mm, const wchar_t *mark, int garnish);
	
void Eigen_drawEigenvector (I, Graphics g, long ivec, long first, long last,
	double minimum, double maximum, int weigh, double size_mm, const wchar_t *mark,
	int connect, wchar_t **rowLabels, int garnish);
#endif
/*
	Draw eigenvector. When rowLabels != NULL, draw row text labels on bottom axis.
*/
	
//int Eigens_alignEigenvectors (Ordered me);
/*
	Correlate all eigenvectors with the eigenvectors of the first Eigen.
	If r < 0 then mirror the eigenvectors of 
*/

double Eigens_getAngleBetweenEigenplanes_degrees (Eigen* eig, Eigen* thou);
/*
	Get angle between the eigenplanes, spanned by the first two eigenvectors, .
*/
__END_DECLS

#endif /* _Eigen_h_ */

/* End of file Eigen.h */
