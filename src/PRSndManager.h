#ifndef __PR_SND_MANAGER__
#define __PR_SND_MANAGER__

#include "PRSndBlock.h"

// sensitive parameter
// It decides how many blocks will use memory, instead of files
//#define NUM_RECENT_SND_BLOCKS		12	//10//5	// old
#define NUM_RECENT_SND_BLOCKS		10

// sensitive parameter
// sample count per block
//#define MAX_LEN_SND_BLOCK			88200	 // old
#define MAX_LEN_SND_BLOCK			44100	

//
// Manages PRSndBlock
//
class PRSndManager
{
public:
	PRSndManager(char *dir);
	~PRSndManager();

	void addBuffer(double *buf, long len);
	double getSample(long at);

private:
	PRSndBlock** m_blocks;
	int m_numBlocks;

	// Saves the 'index' on m_blocks that are recently used
	int m_recentBlocks[NUM_RECENT_SND_BLOCKS];
	int m_curIdxOnRecentBlocks;

	char *m_rootDir;
};

#endif
