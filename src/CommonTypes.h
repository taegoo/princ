#ifndef __COMMON_TYPES__
#define __COMMON_TYPES__
typedef bool (*__progress_callback_t)(float percent, char *text);
typedef bool (*__progress_callback_end_t)(char *res);


typedef enum {
	PRINC_QUALITY_HIGHEST,
	PRINC_QUALITY_HIGH,
	PRINC_QUALITY_MEDIUM,
	PRINC_QUALITY_LOW,
	PRINC_QUALITY_LOWEST
} princ_quality_t;

struct _TierNode {
	double time;
	double value;
	struct _TierNode *next;
};

typedef struct _TierNode TierNode;

#undef MAX
#undef MIN
#define	MAX(a,b)	(a > b ? a : b)
#define	MIN(a,b)	(a < b ? a : b)

#endif

