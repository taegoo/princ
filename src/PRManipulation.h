/*************************************************************************
 *  PRManipulation.h
 *
 *  Taegoo : Migration from fon/Manipulation.h 
 *
 **************************************************************************/
#ifndef __PRMANIPULATION_H__
#define __PRMANIPULATION_H__

#include "CommonTypes.h"
#include "PRSound.h"
#include "PRPointProcess.h"
#include "PRPitch.h"
#include "PRPitchTier.h"
#include "PRDurationTier.h"

// default: min=75, max=700
#define MAX_PITCH		700	//default 700		// F5 = 698.45hz
#define MIN_PITCH		75		//default 75			// D2 = 73.42hz

#define Manipulation_OVERLAPADD  1
#define Manipulation_PULSES  2
#define Manipulation_PULSES_HUM  3
#define Manipulation_PITCH  4
#define Manipulation_PITCH_HUM  5
#define Manipulation_PULSES_PITCH  6
#define Manipulation_PULSES_PITCH_HUM  7
#define Manipulation_OVERLAPADD_NODUR  8
#define Manipulation_PULSES_FORMANT  9
#define Manipulation_PULSES_FORMANT_INTENSITY  10
#define Manipulation_PULSES_LPC  11
#define Manipulation_PULSES_LPC_INTENSITY  12
#define Manipulation_PITCH_LPC  13
#define Manipulation_PITCH_LPC_INTENSITY  14
#define Manipulation_PITCH_LPC_INT_DUR  15

class PRManipulation
{
public:
	//PRManipulation(double tmin, double tmax);
	PRManipulation(PRSound* snd, princ_quality_t quality);
	~PRManipulation();

	PRSound *m_sound;
	PRPointProcess *m_pulses;
	//PRPitchTier *m_pitchTier;
	PRPitchTier *m_origPitchTier;
	PRPitchTier *m_pitchTier0;	// primary
	PRPitchTier *m_pitchTier1;	// secondary
	PRDurationTier *m_duration;

	double m_xmin;		
	double m_xmax;		
	/*
	// inherited:
	long m_nx;		
	double m_dx;
	double m_x1;
	double m_ymin;	
	double m_ymax;	
	long m_ny;	
	double m_dy; 
	double m_y1;	
	double **m_z;	
	*/

	PRPitchTier* analyzePitch(__progress_callback_t callback, float progOffset, float progChunk);
	PRPitchTier* analyzePitchByFiles(char *pathToPulses, char *pathToOrigTier, __progress_callback_t callback, float progOffset, float progChunk);
	void updatePitchTier(PRPitchTier* newTier0, PRPitchTier* newTier1);

	void updateDurationTierWithSpeed(double speed);
	void removeTail(double lenSec);

	static PRManipulation* Sound_to_Manipulation(PRSound *me, double timeStep, double minimumPitch, double maximumPitch);
	static int Pitch_getVoicedIntervalAfter (PRPitch* me, double after, double *tleft, double *tright);
	static double Sound_findExtremum (PRSound *snd, double tmin, double tmax, int includeMaxima, int includeMinima);
	static double findExtremum_3(double *channel1_base, double *channel2_base, long d, long n, int includeMaxima, int includeMinima);
	static double Sound_findMaximumCorrelation (PRSound* snd, double t1, double windowLength, 
																double tmin2, double tmax2, double *tout, double *peak);

	static void appendManipulationToManipulation(PRManipulation *src, PRManipulation *dest);
	// For playing:
	//
	//float* playPart(double tmin, double tmax, int method, long *retBufLen, int tierIndex, bool getOrig);
	float* playPart(double tmin, 
							double tmax, 
							int method, 
							long *retBufLen, 	// in/out
							int tierIndex,
							int origDiffSemitones);
	//float* getFinalBuffer(PRSound* snd, double tmin, double tmax, long *retBufLen);
	float* getFinalBuffer(PRSound* snd, long imin, long imax, long *retBufLen);

	//PRSound* Manipulation_to_Sound(int method, double tmin, double tmax, int tierIndex);
	PRSound* Manipulation_to_Sound(int method, PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier) ;
	//PRSound* synthesize_overlapAdd(double tmin, double tmax, int tierIndex);
	PRSound* synthesize_overlapAdd(PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier) ;
	//PRSound* synthesize_overlapAdd_nodur(double tmin, double tmax, int tierIndex);
	PRSound* synthesize_overlapAdd_nodur(PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier);
	PRSound* Sound_Point_Pitch_Duration_to_Sound(PRSound* me, PRPointProcess* pulses,
												PRPitchTier* pitch, PRDurationTier* duration);

	static PRPointProcess* PitchTier_Point_to_PointProcess(PRPitchTier *pitch, PRPointProcess *vuv);

	static int PointProcess_isVoiced_t(PRPointProcess* me, double t);
	static PRPointProcess* PitchTier_to_PointProcess(PRPitchTier *pitch);
	static PRSound* Sound_Point_Point_to_Sound(PRSound* snd, PRPointProcess* source, PRPointProcess* target);
	static void copyRise(PRSound *snd, double tmin, double tmax, PRSound* thee, double tmaxTarget);
	static void copyFall(PRSound *snd, double tmin, double tmax, PRSound *thee, double tminTarget);
	static void copyBell(PRSound *snd, double tmid, double leftWidth, double rightWidth, PRSound *thee, double tmidTarget);
	static void copyBell2(PRSound *snd, PRPointProcess *source, long isource, double leftWidth, double rightWidth, PRSound *thee, double tmidTarget);
	static void copyFlat(PRSound *snd, double tmin, double tmax, PRSound* thee, double tminTarget) ;

	//////////////////////////////////////////////////////////////////////////////////
	//
	//	From fon/Sound_to_Pitch.c
	//
	/////////////////////////////////////////////////////////////////////////////////
	static PRPitch* Sound_to_Pitch_any(PRSound *snd,
													double dt, 
													double minimumPitch, 
													double periodsPerWindow, 
													int maxnCandidates,
													int method, 		// 'acurate'
													double silenceThreshold, 
													double voicingThreshold,
													double octaveCost, 
													double octaveJumpCost, 
													double voicedUnvoicedCost, 
													double ceiling,
													__progress_callback_t callback,
													float progOffset,
													float progChunk);

	static PRPitch* Sound_to_Pitch(PRSound *snd, 
											double timeStep, 
											double minimumPitch, 
											double maximumPitch,
											__progress_callback_t callback, 
											float progOffset, 
											float progChunk);

	static PRPitch* Sound_to_Pitch_ac(PRSound *snd,
												double dt, 
												double minimumPitch, 
												double periodsPerWindow, 
												int maxnCandidates, 
												int accurate, 
												double silenceThreshold, 
												double voicingThreshold, 
												double octaveCost, 
												double octaveJumpCost, 
												double voicedUnvoicedCost, 
												double ceiling,
												__progress_callback_t callback,
												float progOffset,
												float progChunk);

	static PRPitch* Sound_to_Pitch_cc(PRSound *snd,
												double dt, 
												double minimumPitch, 
												double periodsPerWindow, 
												int maxnCandidates, 
												int accurate,
												double silenceThreshold, 
												double voicingThreshold,
												double octaveCost, 
												double octaveJumpCost, 
												double voicedUnvoicedCost, 
												double ceiling);

	//////////////////////////////////////////////////////////////////////////////////
	//
	//	From fon/Pitch_to_PointProcess.c
	//
	/////////////////////////////////////////////////////////////////////////////////
	static PRPointProcess* Sound_Pitch_to_PointProcess_cc(PRSound* snd, PRPitch* pitch,
							__progress_callback_t callback, float progOffset, float progChunk);

	//////////////////////////////////////////////////////////////////////////////////
	//
	//	From fon/Pitch_to_PitchTier.c
	//
	// all 'static'
	//
	/////////////////////////////////////////////////////////////////////////////////
	static PRPitchTier* Pitch_to_PitchTier(PRPitch* pitch) ;

	double getFreqAtSec(double sec);

private:
	double m_pitchTimeStep;
	double m_durationFactor;		
};

#endif
