#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PRSndManager.h"

PRSndManager::PRSndManager(char *dir)
{
	m_numBlocks = 0;
	m_blocks = NULL;

	m_curIdxOnRecentBlocks = -1;
	// Clear 
	for (int i = 0; i < NUM_RECENT_SND_BLOCKS; i++)
		m_recentBlocks[i] = -1;			// Will save recently used indices on m_blocks

	// Root directory of blokc files
	m_rootDir = (char*)malloc(200);
	strcpy(m_rootDir, dir);
}

PRSndManager::~PRSndManager()
{
	printf("~PRSndManager\n");
	for (int i = 0; i < m_numBlocks; i++) {
		if (m_blocks[i])
			delete m_blocks[i];
	}
	free(m_blocks);

	if (remove(m_rootDir) != 0) {
		printf("[WARNING] Couldn't delete m_rootDir\n");
	}
	free(m_rootDir);
}

// Create sound blocks
void PRSndManager::addBuffer(double *buf, long len)
{
	if (!buf || len == 0)
		return;

	PRSndBlock *newBlock, *prevBlock;
	double *bufPtr;
	long startPos;
	long remainLen;
	long blockLen;
	int numNewBlocks = len / MAX_LEN_SND_BLOCK + 1;

	PRSndBlock **new_blocks = (PRSndBlock**)realloc(m_blocks, (m_numBlocks+numNewBlocks)*sizeof(PRSndBlock*));
	if (new_blocks == NULL) {
		printf("[ERROR] Failed to (re)alloc memory\n");
		return;
	} else {
		m_blocks = new_blocks;
	}

	remainLen = len;
	bufPtr = buf;
	for (int i = 0; i < numNewBlocks; i++) {
		if (remainLen > MAX_LEN_SND_BLOCK)
			blockLen = MAX_LEN_SND_BLOCK;
		else
			blockLen = remainLen;

		// file path for the block
		char *blockPath = (char*)malloc(200);
		sprintf(blockPath, "%s/%d", m_rootDir, m_numBlocks);

		if (m_numBlocks > 0) {
			prevBlock = m_blocks[m_numBlocks-1];
			startPos = prevBlock->m_pos + prevBlock->m_len;
		} else {
			startPos = 0;
		}

		newBlock = new PRSndBlock(bufPtr, blockLen, blockPath, startPos);
		// Increment the count, also
		m_blocks[m_numBlocks++] = newBlock;

		free(blockPath);	// caller-free

		bufPtr += blockLen;
		remainLen -= blockLen;

		if (remainLen <= 0)
			break;
	}
}

double PRSndManager::getSample(long at)
{
	PRSndBlock *block;
	int i;
	double res = 0.0;
	//long curStart = 0;
	int newIdx = -1;	// MUST -1
	int curIdx = -1;

	if (m_curIdxOnRecentBlocks > -1)
		curIdx = m_recentBlocks[m_curIdxOnRecentBlocks];

	//printf("curIdx=%d\n", curIdx);

	if (curIdx > -1) {
		block = m_blocks[curIdx];

		//printf("    curIdx=%d, at=%ld, block->m_pos=%ld, block->m_len=%ld\n", curIdx, at, block->m_pos, block->m_len);

		if (at >= block->m_pos) {
			// Search to the right
			for (i = curIdx; i < m_numBlocks; i++) {
				block = m_blocks[i];
				if (block != NULL) {
					if (at >= block->m_pos && at < block->m_pos + block->m_len) {
						// Gotcha
						res = block->getSample(at - block->m_pos);
						newIdx = i;
						break;
					}
				}
			}
		} else {
			// Search to the left
			for (i = curIdx; i >= 0; i--) {
				block = m_blocks[i];
				if (block != NULL) {
					if (at >= block->m_pos && at < block->m_pos + block->m_len) {
						// Gotcha
						res = block->getSample(at - block->m_pos);
						newIdx = i;
						break;
					}
				}
			}
		}
	} else {
		// Search from '0'
		for (i = 0; i < m_numBlocks; i++) {
			block = m_blocks[i];
			if (block != NULL) {
				if (at >= block->m_pos && at < block->m_pos + block->m_len) {
					// Gotcha
					res = block->getSample(at - block->m_pos);

					newIdx = i;
					break;
				}
			}
		}
	}

	//
	// Manage the array of recent blocks which are allocating memory
	//
	if (newIdx >= 0) {
		// Look for the same index
		for (i = 0; i < NUM_RECENT_SND_BLOCKS; i++) {
			if (newIdx == m_recentBlocks[i]) {
				m_curIdxOnRecentBlocks = i;
				break;
			}
		}

		if (m_curIdxOnRecentBlocks == -1
				|| newIdx != m_recentBlocks[m_curIdxOnRecentBlocks]) {
			// Increment
			m_curIdxOnRecentBlocks++;
			if (m_curIdxOnRecentBlocks >= NUM_RECENT_SND_BLOCKS) {
				// Ring buffer
				m_curIdxOnRecentBlocks = 0;
			}

			int oldBlockIdx = m_recentBlocks[m_curIdxOnRecentBlocks];
			if (newIdx != oldBlockIdx) {
				// Release memory of the block
				if (oldBlockIdx >= 0) {
					block = m_blocks[oldBlockIdx];
					block->releaseMemory();
				}
			}
			// Update
			m_recentBlocks[m_curIdxOnRecentBlocks] = newIdx;
		}
	} else {
		printf("[ERROR] PRSndManager::getSample - No sample at %ld\n", at);
		res = 0.0;
	}

	return res;
}

