/*************************************************************************
 *  PRPitch.h
 *
 *  Taegoo : Migration from fon/Pitch.h & fon/Pitch_def.h
 *
 **************************************************************************/
#ifndef __PRPITCH_H__
#define __PRPITCH_H__

#include "PRSampled.h"

#define Pitch_LEVEL_FREQUENCY  	1
#define Pitch_LEVEL_STRENGTH  	2

#define Pitch_STRENGTH_UNIT_min  				0
#define Pitch_STRENGTH_UNIT_AUTOCORRELATION  0
#define Pitch_STRENGTH_UNIT_NOISE_HARMONICS_RATIO  1
#define Pitch_STRENGTH_UNIT_HARMONICS_NOISE_DB  	2
#define Pitch_STRENGTH_UNIT_max  						2

#define kPitch_unit_HERTZ  						0
#define kPitch_unit_HERTZ_LOGARITHMIC  		1
#define kPitch_unit_MEL  							2
#define kPitch_unit_LOG_HERTZ  					3
#define kPitch_unit_SEMITONES_1  				4
#define kPitch_unit_SEMITONES_100  				5
#define kPitch_unit_SEMITONES_200  				6
#define kPitch_unit_SEMITONES_440  				7
#define kPitch_unit_ERB  							8

#define Pitch_NEAREST  	0
#define Pitch_LINEAR  	1

// define 'Pitch_Frame' struct
typedef struct structPitch_Candidate {
	double m_frequency;
	double m_strength;
} Pitch_Candidate;

typedef struct {
	double m_intensity;
	long m_nCandidates;
	// TODO:
	Pitch_Candidate **m_candidate; // oo_STRUCT_VECTOR(Pitch_Candidate, candidate, my nCandidates)
} Pitch_Frame;


/* Attributes:
	xmin				// Start time (seconds).
	xmax > xmin		// End time (seconds).
	nx >= 1			// Number of time slices.
	dx > 0.0			// Time step (seconds).
	x1					// Centre of first time slice (seconds).
	ceiling			// Candidates with a higher frequency are unvoiced.
	maxnCandidates >= 1	// Maximum number of candidates per time slice.
	frame[1..nx].nCandidates	// Number of candidates in each time slice, including the unvoiced candidate.
	frame[1..nx].candidate[1..nCandidates].frequency
		// The frequency of each candidate (Hertz), 0 means aperiodic or silent.
		// candidate[1].frequency is the frequency of the currently best candidate.
	frame[1..nx].candidate[1..nCandidates].strength
		// The strength of each candidate, a real number between 0 and 1:
		// 0 means not periodic at all, 1 means perfectly periodic;
		// if the frequency of the candidate is 0, its strength is a real number
		// that represents the maximum periodicity that
		// can still be considered to be due to noise (e.g., 0.4).
		// candidate[1].strength is the strength of the currently best candidate.
	frame[1..nx].intensity
		// The relative intensity of each frame, a real number between 0 and 1.
*/
	/*
		Function:
			create space for a number of candidates; space already there is disposed of.
		Preconditions:
			nCandidates >= 1;
		Postconditions:
			my nCandidates == nCandidates;
			my candidate [1..nCandidates]. frequency == 0.0; // unvoiced
			my candidate [1..nCandidates]. strength == 0.0; // aperiodic
			my intensity == 0.0; // silent
	*/


class PRPitch : public PRSampled
{
public:
	PRPitch();
	void init(double tmin, double tmax, long nt, double dt, double t1, double ceiling, int maxnCandidates);
	/*
		Function:
			create an empty pitch contour (voiceless), or NULL if out of memory.
		Preconditions:
			tmax > tmin;
			nt >= 1;
			dt > 0.0;
			maxnCandidates >= 2;
		Postconditions:
			my xmin == tmin;
			my xmax == tmax;
			my nx == nt;
			my dx == dt;
			my x1 == t1;
			my ceiling == ceiling;
			my maxnCandidates == maxnCandidates;
			my frame [1..nt]. nCandidates == 1;
			my frame [1..nt]. candidate [1]. frequency == 0.0; // unvoiced
			my frame [1..nt]. candidate [1]. strength == 0.0; // aperiodic
			my frame [1..nt]. intensity == 0.0; // silent
	*/
	virtual ~PRPitch();

	double m_ceiling; // Candidates with a higher frequency are unvoiced.
	int m_maxnCandidates; // >= 1	// Maximum number of candidates per time slice.
	Pitch_Frame **m_frame; // oo_STRUCT_VECTOR(Pitch_Frame, frame, my nx)
	/*
	double m_xmin;		// Start time (seconds).
	double m_xmax;		// xmax > xmin		// End time (seconds).
	long m_nx;			// nx >= 1		// Number of time slices.
	double m_dx;		// dx > 0.0		// Time step (seconds).
	double m_x1;		// Centre of first time slice (seconds).
	*/

	/*
		frame[1..nx].nCandidates	// Number of candidates in each time slice, including the unvoiced candidate.

		frame[1..nx].candidate[1..nCandidates].frequency
			// The frequency of each candidate (Hertz), 0 means aperiodic or silent.
			// candidate[1].frequency is the frequency of the currently best candidate.

		frame[1..nx].candidate[1..nCandidates].strength
			// The strength of each candidate, a real number between 0 and 1:
			// 0 means not periodic at all, 1 means perfectly periodic;
			// if the frequency of the candidate is 0, its strength is a real number
			// that represents the maximum periodicity that
			// can still be considered to be due to noise (e.g., 0.4).
			// candidate[1].strength is the strength of the currently best candidate.

		frame[1..nx].intensity
			// The relative intensity of each frame, a real number between 0 and 1.
	*/

	// not using?:
	double m_ymin;	
	double m_ymax;	
	long m_ny;	
	double m_dy; 
	double m_y1;	
	double **m_z;	

	/*
	virtual double indexToX(long i); // PRSampled
	virtual double xToIndex (double x);
	virtual long xToNearestIndex(double x);
	virtual long xToHighIndex(double x);
	*/

	static int Frame_init(Pitch_Frame* me, int nCandidates);
	double convertStandardToSpecialUnit (double value, long ilevel, int unit);
	double getValueAtSample(long iframe, long ilevel, int unit);

	int isVoiced_i(long index);
	/*
		Is the frame 'index' voiced?
		A frame is considered voiced if the frequency of its first candidate
		is greater than 0.0 but less than my ceiling.
		Precondition:
			index >= 1 && index <= my nx;
	*/

	double getValueAtTime(double time, int unit, int interpolate);
	double getValueAtX(double x, long ilevel, int unit, int interpolate);

	int getMaxnCandidates();
	/*
		Returns the largest number of candidates actually attested in a frame.
	*/

	void pathFinder(double silenceThreshold, double voicingThreshold,
						double octaveCost, double octaveJumpCost, double voicedUnvoicedCost,
						double ceiling, int pullFormants);



#if 0
	int isVoiced_t(double t);
	/*
		Are you voiced at time 't'?
		The answer is TRUE iff 't' lies within a voiced frame.
	*/

	double getStrengthAtTime(double time, int unit, int interpolate);

	double getMean(double tmin, double tmax, int unit);
	double getMeanStrength(double tmin, double tmax, int unit);
	double getQuantile(double tmin, double tmax, double quantile, int unit);
	double getStandardDeviation(double tmin, double tmax, int unit);
	void getMaximumAndTime(double tmin, double tmax, int unit, int interpolate,
									double *return_maximum, double *return_timeOfMaximum);
	double getMaximum(double tmin, double tmax, int unit, int interpolate);
	double getTimeOfMaximum(double tmin, double tmax, int unit, int interpolate);
	void getMinimumAndTime(double tmin, double tmax, int unit, int interpolate,
									double *return_minimum, double *return_timeOfMinimum);
	double getMinimum(double tmin, double tmax, int unit, int interpolate);
	double getTimeOfMinimum(double tmin, double tmax, int unit, int interpolate);

	void setCeiling(double ceiling);
	/*
		Postcondition:
			my ceiling = ceiling;
	*/


	void difference(PRPitch* thee);
	/* give information about frames that are different in me and thee. */

	long getMeanAbsSlope_hertz(double *slope);
	long getMeanAbsSlope_mel(double *slope);
	long getMeanAbsSlope_semitones(double *slope);
	long getMeanAbsSlope_erb(double *slope);
	long getMeanAbsSlope_noOctave(double *slope);
	/*
		The value returned is the number of voiced frames (nVoiced);
		this signals if the values are valid:
		'value', 'minimum', 'maximum', and 'mean' are valid if nVoiced >= 1;
		'variance' and 'slope' are valid if nVoiced >= 2.
		Invalid variables are always set to 0.0.
		'minimum', 'maximum', 'mean', and 'variance' may be NULL.
	*/

	PRPitch* killOctaveJumps();
	/* Add octave jumps so that every pitch step,
		including those across unvoiced frames,
		does not exceed 1/2 octave.
		Postcondition:
			result -> ceiling = my ceiling * 2;
	*/

	PRPitch* interpolate();
	/* Interpolate the pitch values of unvoiced frames. */
	/* No extrapolation beyond first and last voiced frames. */

	PRPitch* subtractLinearFit(int unit);

	PRPitch* smooth(double bandWidth);
	/* Smoothing by convolution with Gaussian curve.
		Time domain: exp (- (pi t bandWidth) ^ 2)
			down to 8.5 % for t = +- 0.5/bandWidth
		Frequency domain: exp (- (f / bandWidth) ^ 2)
			down to 1/e for f = +- bandWidth
		Example:
			if bandWidth = 10 Hz,
			then the Gaussian curve has a 8.5% duration of 0.1 s,
			and a 1/e low-pass point of 10 Hz.
		Algorithm:
			Interpolation of pitch at internal unvoiced parts.
			Zeroth-degree extrapolation at edges (duration triples).
			FFT; multiply by Gaussian; inverse FFT.
			Cut back to normal duration.
			Undo interpolation.
	*/

	void step(double step, double precision, double tmin, double tmax);
	/*
		Instead of the currently chosen candidate,
		choose the candidate with another ("target") frequency, determined by 'step'.
		E.g., for an upward octave jump, 'step' is 2.
		Only consider frequencies between (1 - precision) * targetFrequency
		and (1 - precision) * targetFrequency.
		Take the candidate nearest to targetFrequency,
		as long as that candidate is in between 0 and my ceiling.
	*/

	//int formula(const unsigned short *formula, Interpreter interpreter);

private:

	long countVoicedFrames();
#endif

};

#endif
