/*************************************************************************
 *  PRPitchTier.cpp
 *
 *  Taegoo : Migration from fon/PitchTier.c & fon/RealTier.c
 *
 **************************************************************************/
#include "PRPitchTier.h"

PRPitchTier::PRPitchTier()
{
}
PRPitchTier::PRPitchTier(double tmin, double tmax)
{
	init(tmin, tmax);
}

#if 0
int PRPitchTier::shiftFrequencies(double tmin, double tmax, double shift, int units)
{
	return 0;
}

void PRPitchTier::multiplyFrequencies(double tmin, double tmax, double factor)
{
}

PRPitchTier* PRPitchTier::PointProcess_upto_PitchTier(PRPointProcess *pointProc, double frequency)
{
	return NULL;
}

void PRPitchTier::stylize(double frequencyResolution, int useSemitones)
{
}
#endif

