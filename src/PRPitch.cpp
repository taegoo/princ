/*************************************************************************
 *  PRPitch.cpp
 *
 *  Taegoo : Migration from fon/Pitch.c
 *
 **************************************************************************/
#import "PRPitch.h"
#import "NUM.h"

PRPitch::PRPitch()
{
}

void PRPitch::init(double tmin, double tmax, long nt, double dt, double t1, double ceiling, int maxnCandidates)
{
	long it;

	m_xmin = tmin;
	m_xmax = tmax;
	m_nx = nt;
	m_dx = dt;
	m_x1 = t1;
	m_ceiling = ceiling;
	m_maxnCandidates = maxnCandidates;

	// alloc frames
	m_frame = (Pitch_Frame**)calloc(nt+1, sizeof(Pitch_Frame*));

	/* Put one candidate in every frame (unvoiced, silent). */
	for (it = 1; it <= nt; it++) {
		m_frame[it] = (Pitch_Frame*)calloc(1, sizeof(Pitch_Frame));

		if (!PRPitch::Frame_init(m_frame[it], 1)) {
			break;
		}
	}
}

PRPitch::~PRPitch()
{
	//printf("~PRPitch()\n");
	long x, y;
	Pitch_Frame *frame = NULL;
	for (x = 1; x <= m_nx; x++) {
		frame = m_frame[x];
		if (frame) {
			for (y = 1; y <= frame->m_nCandidates; y++) {
				free(frame->m_candidate[y]);
			}
			free(frame);
		}
	}
	free(m_frame);
	//printf("~PRPitch() - end\n");
}

// static
int PRPitch::Frame_init(Pitch_Frame* frame, int nCandidates)
{
	frame->m_candidate = (Pitch_Candidate **)calloc(nCandidates+1, sizeof(Pitch_Candidate*));
	for (long i = 1; i <= nCandidates; i++) {
		frame->m_candidate[i] = (Pitch_Candidate*)calloc(1, sizeof(Pitch_Candidate));
	}
	frame->m_nCandidates = nCandidates;
	return 1;
}

double PRPitch::convertStandardToSpecialUnit(double value, long ilevel, int unit) 
{
	if (ilevel == Pitch_LEVEL_FREQUENCY) {
		return
			unit == kPitch_unit_HERTZ ? value :
			unit == kPitch_unit_HERTZ_LOGARITHMIC ? value <= 0.0 ? NUMundefined : log10 (value) :
			unit == kPitch_unit_MEL ? NUMhertzToMel (value) :
			unit == kPitch_unit_LOG_HERTZ ? value <= 0.0 ? NUMundefined : log10 (value) :
			unit == kPitch_unit_SEMITONES_1 ? value <= 0.0 ? NUMundefined : 12.0 * log (value / 1.0) / NUMln2 :
			unit == kPitch_unit_SEMITONES_100 ? value <= 0.0 ? NUMundefined : 12.0 * log (value / 100.0) / NUMln2 :
			unit == kPitch_unit_SEMITONES_200 ? value <= 0.0 ? NUMundefined : 12.0 * log (value / 200.0) / NUMln2 :
			unit == kPitch_unit_SEMITONES_440 ? value <= 0.0 ? NUMundefined : 12.0 * log (value / 440.0) / NUMln2 :
			unit == kPitch_unit_ERB ? NUMhertzToErb (value) :
			NUMundefined;
	} else {
		return
			unit == Pitch_STRENGTH_UNIT_AUTOCORRELATION ? value :
			unit == Pitch_STRENGTH_UNIT_NOISE_HARMONICS_RATIO ?
				value <= 1e-15 ? 1e15 : value > 1.0 - 1e-15 ? 1e-15 : (1.0 - value) / value :   /* Before losing precision. */
			unit == Pitch_STRENGTH_UNIT_HARMONICS_NOISE_DB ?
				value <= 1e-15 ? -150.0 : value > 1.0 - 1e-15 ? 150.0 : 10 * log10 (value / (1.0 - value)) :   /* Before losing precision. */
			NUMundefined;
	}
}

double PRPitch::getValueAtSample(long iframe, long ilevel, int unit) 
{
	if (iframe < 1 || iframe > m_nx) 
		return NUMundefined;

	double f = m_frame[iframe]->m_candidate[1]->m_frequency;
	if (f <= 0.0 || f >= m_ceiling)
		return NUMundefined;   /* Frequency out of range (or NUMundefined)? Voiceless. */

	return convertStandardToSpecialUnit(ilevel == Pitch_LEVEL_FREQUENCY ? f : m_frame[iframe]->m_candidate[1]->m_strength, ilevel, unit);
}

int PRPitch::isVoiced_i(long iframe)
{
	return NUMdefined(getValueAtSample(iframe, Pitch_LEVEL_FREQUENCY, kPitch_unit_HERTZ));
}

double PRPitch::getValueAtTime(double time, int unit, int interpolate)
{
	return getValueAtX(time, Pitch_LEVEL_FREQUENCY, unit, interpolate);
}

// taegoo: carefully?
double PRPitch::getValueAtX(double x, long ilevel, int unit, int interpolate) 
{
	if (x < m_xmin || x > m_xmax) 
		return NUMundefined;
	if (interpolate) {
		double ireal = xToIndex(x);
		double phase, fnear, ffar;
		long ileft = floor(ireal), inear, ifar;
		phase = ireal - ileft;
		if (phase < 0.5) {
			inear = ileft, ifar = ileft + 1;
		} else {
			ifar = ileft, inear = ileft + 1;
			phase = 1.0 - phase;
		}
		if (inear < 1 || inear > m_nx) 
			return NUMundefined;   /* X out of range? */

		fnear = getValueAtSample(inear, ilevel, unit);
		if (fnear == NUMundefined) 
			return NUMundefined;   /* Function value not defined? */
		if (ifar < 1 || ifar > m_nx) 
			return fnear;   /* At edge? Extrapolate. */

		ffar = getValueAtSample(ifar, ilevel, unit);
		if (ffar == NUMundefined) 
			return fnear;   /* Neighbour undefined? Extrapolate. */

		return fnear + phase * (ffar - fnear);   /* Interpolate. */
	}
	return getValueAtSample(xToNearestIndex(x), ilevel, unit);
}

int PRPitch::getMaxnCandidates() 
{
	int result = 0;
	long i;
	for (i = 1; i <= m_nx; i++) {
		int nCandidates = m_frame[i]->m_nCandidates;
		if (nCandidates > result) 
			result = nCandidates;
	}
	return result;
}

void PRPitch::pathFinder(double silenceThreshold, double voicingThreshold,
					double octaveCost, double octaveJumpCost, double voicedUnvoicedCost,
					double ceiling, int pullFormants)
{
	long maxnCandidates = getMaxnCandidates();
	long place;
	long iframe;
	volatile double maximum, value;
	double **delta = NULL;
	long **psi = NULL;
	double ceiling2 = pullFormants ? 2 * ceiling : ceiling;
	/* Next three lines 20011015 */
	double timeStepCorrection = 0.01 / m_dx;
	octaveJumpCost *= timeStepCorrection;
	voicedUnvoicedCost *= timeStepCorrection;

	m_ceiling = ceiling;
	delta = NUMdmatrix(1, m_nx, 1, maxnCandidates); 
	psi = NUMlmatrix(1, m_nx, 1, maxnCandidates);

	for (iframe = 1; iframe <= m_nx; iframe++) {
		Pitch_Frame* frame = m_frame[iframe];
		double unvoicedStrength = silenceThreshold <= 0 ? 0 :
							2 - frame->m_intensity / (silenceThreshold / (1 + voicingThreshold));
		unvoicedStrength = voicingThreshold + (unvoicedStrength > 0 ? unvoicedStrength : 0);
		for (long icand = 1; icand <= frame->m_nCandidates; icand ++) {
			Pitch_Candidate* candidate = frame->m_candidate[icand];
			int voiceless = candidate->m_frequency == 0 || candidate->m_frequency > ceiling2;
			delta[iframe][icand] = voiceless ? unvoicedStrength :
							candidate->m_strength - octaveCost * NUMlog2(ceiling / candidate->m_frequency);
		}
	}

	/* Look for the most probable path through the maxima. */
	/* There is a cost for the voiced/unvoiced transition, */
	/* and a cost for a frequency jump. */

	for (iframe = 2; iframe <= m_nx; iframe ++) {
		Pitch_Frame *prevFrame = m_frame[iframe - 1]; 
		Pitch_Frame *curFrame = m_frame[iframe];

		double *prevDelta = delta[iframe - 1];
		double *curDelta = delta[iframe];
		long *curPsi = psi[iframe];

		for (long icand2 = 1; icand2 <= curFrame->m_nCandidates; icand2 ++) {
			double f2 = curFrame->m_candidate[icand2]->m_frequency;
			maximum = -1e30;
			place = 0;
			for (long icand1 = 1; icand1 <= prevFrame->m_nCandidates; icand1 ++) {
				double f1 = prevFrame->m_candidate[icand1]->m_frequency;
				double transitionCost;
				bool previousVoiceless = f1 <= 0 || f1 >= ceiling2;
				bool currentVoiceless = f2 <= 0 || f2 >= ceiling2;
				if (currentVoiceless) {
					if (previousVoiceless) {
						transitionCost = 0;   // both voiceless
					} else {
						transitionCost = voicedUnvoicedCost;   // voiced-to-unvoiced transition
					}
				} else {
					if (previousVoiceless) {
						transitionCost = voicedUnvoicedCost;   // unvoiced-to-voiced transition
#if 0
						if (Melder_debug == 30) {
							/*
							 * Try to take into account a frequency jump across a voiceless stretch.
							 */
							long place1 = icand1;
							for (long jframe = iframe - 2; jframe >= 1; jframe --) {
								place1 = psi [jframe + 1] [place1];
								f1 = m_frame [jframe]. candidate [place1]. frequency;
								if (f1 > 0 && f1 < ceiling) {
									transitionCost += octaveJumpCost * fabs (NUMlog2 (f1 / f2)) / (iframe - jframe);
									break;
								}
							}
						}
#endif
					} else {
						transitionCost = octaveJumpCost * fabs (NUMlog2 (f1 / f2));   // both voiced
					}
				}
				value = prevDelta[icand1] - transitionCost + curDelta[icand2];
				//if (Melder_debug == 33) Melder_casual ("Frame %ld, current candidate %ld (delta %g), previous candidate %ld (delta %g), "
				//	"transition cost %g, value %g, maximum %g", iframe, icand2, curDelta [icand2], icand1, prevDelta [icand1], transitionCost, value, maximum);
				if (value > maximum) {
					maximum = value;
					place = icand1;
				} else if (value == maximum) {
					/*
					if (Melder_debug == 33) Melder_casual ("A tie in frame %ld, current candidate %ld, previous candidate %ld", iframe, icand2, icand1);
					*/
				}
			}
			curDelta [icand2] = maximum;
			curPsi [icand2] = place;
		}
	}

	/* Find the end of the most probable path. */

	place = 1;
	maximum = delta[m_nx][place];
	for (long icand = 2; icand <= m_frame[m_nx]->m_nCandidates; icand ++) {
		if (delta[m_nx][icand] > maximum) {
			place = icand;
			maximum = delta[m_nx][place];
		}
	}

	//
	// Taegoo: carefull below
	//

	/* Backtracking: follow the path backwards. */
	for (iframe = m_nx; iframe >= 1; iframe--) {
		//if (Melder_debug == 33) Melder_casual ("Frame %ld: swapping candidates 1 and %ld", iframe, place);
		Pitch_Frame *frame = m_frame[iframe];
		//struct structPitch_Candidate help = *(frame->m_candidate[1]);
		Pitch_Candidate *help = frame->m_candidate[1];
		frame->m_candidate[1] = frame->m_candidate[place];
		frame->m_candidate[place] = help;
		place = psi [iframe] [place];   // This assignment is challenging to CodeWarrior 11.
	}

	/* Pull formants: devoice frames with frequencies between ceiling and ceiling2. */

	if (ceiling2 > ceiling) {
		//if (Melder_debug == 33) Melder_casual ("Pulling formants...");
		for (iframe = m_nx; iframe >= 1; iframe--) {
			Pitch_Frame *frame = m_frame[iframe];
			Pitch_Candidate *winner = frame->m_candidate [1];
			double f = winner->m_frequency;
			if (f > ceiling && f <= ceiling2) {
				for (long icand = 2; icand <= frame->m_nCandidates; icand++) {
					Pitch_Candidate *loser = frame->m_candidate[icand];
					if (loser->m_frequency == 0.0) {
						struct structPitch_Candidate help = *winner;
						//Pitch_Candidate *help = winner;
						*winner = *loser;
						*loser = help;
						break;
					}
				}
			}
		}
	}

end:
	NUMdmatrix_free(delta, 1, 1);
	NUMlmatrix_free(psi, 1, 1);
}

#if 0
int PRPitch::isVoiced_t(double t)
{
	return 0;
}

double PRPitch::getStrengthAtTime(double time, int unit, int interpolate)
{
	return 0.0;
}

long PRPitch::countVoicedFrames()
{
	return 0;
}

double PRPitch::getMean(double tmin, double tmax, int unit)
{
	return 0.0;
}

double PRPitch::getMeanStrength(double tmin, double tmax, int unit)
{
	return 0.0;
}

double PRPitch::getQuantile(double tmin, double tmax, double quantile, int unit)
{
	return 0.0;
}

double PRPitch::getStandardDeviation(double tmin, double tmax, int unit)
{
	return 0.0;
}

void PRPitch::getMaximumAndTime(double tmin, double tmax, int unit, int interpolate,
								double *return_maximum, double *return_timeOfMaximum)
{
}

double PRPitch::getMaximum(double tmin, double tmax, int unit, int interpolate)
{
	return 0.0;
}

double PRPitch::getTimeOfMaximum(double tmin, double tmax, int unit, int interpolate)
{
	return 0.0;
}

void PRPitch::getMinimumAndTime(double tmin, double tmax, int unit, int interpolate,
								double *return_minimum, double *return_timeOfMinimum)
{
}

double PRPitch::getMinimum(double tmin, double tmax, int unit, int interpolate)
{
	return 0.0;
}

double PRPitch::getTimeOfMinimum(double tmin, double tmax, int unit, int interpolate)
{
	return 0.0;
}

void PRPitch::setCeiling(double ceiling)
{
}


void PRPitch::difference(PRPitch* thee)
{
}

long PRPitch::getMeanAbsSlope_hertz(double *slope)
{
	return 0;
}

long PRPitch::getMeanAbsSlope_mel(double *slope)
{
	return 0;
}

long PRPitch::getMeanAbsSlope_semitones(double *slope)
{
	return 0;
}

long PRPitch::getMeanAbsSlope_erb(double *slope)
{
	return 0;
}

long PRPitch::getMeanAbsSlope_noOctave(double *slope)
{
	return 0;
}

PRPitch* PRPitch::killOctaveJumps()
{
	return NULL;
}

PRPitch* PRPitch::interpolate()
{
	return NULL;
}

PRPitch* PRPitch::subtractLinearFit(int unit)
{
	return NULL;
}

PRPitch* PRPitch::smooth(double bandWidth)
{
	return NULL;
}

void PRPitch::step(double step, double precision, double tmin, double tmax)
{
}

/*
int PRPitch::formula(const wchar_t *formula, Interpreter interpreter)
{
	return 0;
}
*/
#endif

