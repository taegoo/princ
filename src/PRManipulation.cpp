/*************************************************************************
 *  PRManipulation.cpp
 *
 *  Taegoo : Migration from fon/Manipulation.c 
 *
 **************************************************************************/
#include "PRManipulation.h"
#include "NUM2.h"

//
// The most sensitive parameters:
// Will be reassigned properly
//
double gPitchTimeStep = 0.01;
long gCorStrideFactorH = 2;	
long gCorStrideFactorL = 1;

//////////////////////////////////////////

#define	SILENCE_THRESHOLD		0.05 	// default = 0.03

#define  PERIODS_PER_WINDOW	3.0 	// default = 3.0
#define 	MAX_NCANDID				15		// default = 15

// sensitive parameter
// Higher the faster
#define	VOICING_THRESHOLD		0.45 	// default = 0.45

// sensitive parameter - default: 0.02000000001 => NO OTHER VALUE!
#define MAX_T  0.02000000001   /* Maximum interval between two voice pulses (otherwise voiceless). */

#define AC_HANNING  		0
#define AC_GAUSS  		1
#define FCC_NORMAL  		2
#define FCC_ACCURATE  	3
#define SCC_NORMAL  		4
#define SCC_ACCURATE  	5

#define MAXIMUM_NUMBER_OF_CHANNELS  2


static double midiToFreq(double midi) 
{ 
	double freq;
	freq = exp (log (440.0) + (double)(midi - 69.0) * log (2.0) / 12.0);
	//printf("midiToFreq: midi=%d, freq=%f\n", midi, freq);
	if (freq < 0.0)
		freq = 0.0;
	return freq;
}

static double freqToMidi(double f)
{
	double midi;
	midi = 69.0 + 12.0/log(2.0) * log(f/440.0) + 0.5;
	if (midi < 0)
		midi = 0;
	return midi;
}

//PRManipulation::PRManipulation(double tmin, double tmax)
PRManipulation::PRManipulation(PRSound* snd, princ_quality_t quality)
{
	//
	// sensitive parameters
	//
	switch (quality) {
		case PRINC_QUALITY_HIGHEST:
			//gPitchTimeStep	= 0.005;
			gPitchTimeStep	= 0.01;
			gCorStrideFactorH	= 1;
			gCorStrideFactorL = 1;
			break;

		case PRINC_QUALITY_HIGH:		// for iPhone4S
			//gPitchTimeStep	= 0.01;
			gPitchTimeStep	= 0.01;
			gCorStrideFactorH	= 2;
			gCorStrideFactorL = 1;
			break;

		case PRINC_QUALITY_MEDIUM:	// standard
		default:
			gPitchTimeStep	= 0.03;
			gCorStrideFactorH	= 3;
			gCorStrideFactorL = 2;
			break;

		case PRINC_QUALITY_LOW:			// for iPhone4 or below
			gPitchTimeStep	= 0.05;
			gCorStrideFactorH	= 4;
			gCorStrideFactorL = 3;
			break;

		case PRINC_QUALITY_LOWEST:
			gPitchTimeStep	= 0.1;
			gCorStrideFactorH	= 10;
			gCorStrideFactorL = 5;
			break;
	}

	m_xmin = snd->m_xmin;
	m_xmax = snd->m_xmax;

	m_durationFactor = 1.0;

	// sensitive parameter
	m_pitchTimeStep = gPitchTimeStep;

	//
	// Assumes snd is always mono (don't need to converToMono)
	//
	//m_sound snd->convertToMono();
	m_sound = snd;
	m_sound->subtractMean();

	m_pulses = NULL; // PRPointProcess
	m_origPitchTier = NULL;
	m_pitchTier0 = NULL; // PRPitchTier
	m_pitchTier1 = NULL; // PRPitchTier
	m_duration = NULL;
}

PRManipulation::~PRManipulation()
{
	printf("PRManipulation::~PRManipulation()\n");
	if (m_sound)
		delete m_sound;
	m_sound = NULL;

	if (m_pulses)
		delete m_pulses;
	m_pulses = NULL;

	if (m_origPitchTier)
		delete m_origPitchTier;
	m_origPitchTier = NULL;

	if (m_pitchTier0)
		delete m_pitchTier0;
	m_pitchTier0 = NULL;

	if (m_pitchTier1)
		delete m_pitchTier1;
	m_pitchTier1 = NULL;

	if (m_duration)
		delete m_duration;
	m_duration = NULL;
}

// Update m_duration
// speed: > 0.0
void PRManipulation::updateDurationTierWithSpeed(double speed)
{
	printf("PRManipulation::updateDurationTierWithSpeed: speed=%f\n", speed);
	if (speed <= 0.0)
		speed = 0.2; // Minimum speed

	m_durationFactor = 1.0 / speed;


	if (m_durationFactor == 1.0) {
		// Remove m_duration so that it won't process with m_duration
		if (m_duration)
			delete m_duration;
		m_duration = NULL;
	} else {
		if (!m_duration)
			m_duration = new PRDurationTier(0, m_xmax); // PRDurationTier
		else {
			m_duration->m_xmin = 0.0;
			m_duration->m_xmax = m_xmax; // make sure to have corrent length
			m_duration->pointSet_removeAllPoints();
		}
		// Add new point at the beginning
		m_duration->addPoint(0.0, m_durationFactor);
	}
}

void PRManipulation::removeTail(double lenSec)
{
	if (lenSec >= m_xmax) {
		printf("[WARNING] PRManipulation::removeTail() does nothing\n");
		return;
	}

	// Do for me
	m_xmax -= lenSec;
	if (m_duration)
		m_duration->m_xmax = m_xmax; // make sure to have corrent length

	// Do for m_sound
	if (m_sound)
		m_sound->removeTail(lenSec);

	// Do for m_pulses
	m_pulses->removeTail(lenSec);
}

// static
// New for v3.0
void PRManipulation::appendManipulationToManipulation(PRManipulation *src, PRManipulation *dest)
{
	//
	// TODO: Append to m_duration
	// As of v3.0, m_duration is not supported yet, though
	//

	// Update m_xmax
	dest->m_xmax += src->m_xmax;

	//
	// Append to m_sound
	//
	//src->m_sound->subtractMean();
	PRSound::appendSoundToSound(src->m_sound, dest->m_sound);

	//
	// Append to m_pulses
	//
	//printf("appendManipulationToManipulation: appending to m_pulses\n");
	PRPointProcess::appendPointProcessToPointProcess(src->m_pulses, dest->m_pulses);

	if (dest->m_duration)
		dest->m_duration->m_xmax = dest->m_xmax; // make sure to have corrent length
}

//
// Creates 'm_pulses' and returns the pitch-tier
//
PRPitchTier* PRManipulation::analyzePitch(__progress_callback_t callback, float progOffset, float progChunk)
{
	//printf("PRManipulation::analyzePitch - start\n");
	//double t, v;
	//long i;
	char *progMsg = (char*)"Preparing...";
	if (!m_sound)
		return NULL;

	// STAT:
	/*
	float sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++++++ sec: %f - analyzePitch - before Sound_to_Pitch\n", sysTime);
	*/

	//printf("    calling Sound_to_Pitch\n");
	//
	// This is the MOST time-consuming part:
	//
	PRPitch* pitch = PRManipulation::Sound_to_Pitch(
																m_sound, 
																m_pitchTimeStep, 	// sec
																MIN_PITCH,  // hz
																MAX_PITCH,	// hz
																callback, 
																progOffset, 
																progChunk*0.5);
	////////////////////////////////////////////////////////////////////////
	// TEST:
	// create temp PRSound with half of m_sound
	//
#if 0
	PRSound *snd_temp = new PRSound();
	snd_temp->init(m_sound->m_ny, m_sound->m_xmin, m_sound->m_xmax, m_sound->m_nx / 2, m_sound->m_dx, m_sound->m_xmin);
	for (i = 1; i <= snd_temp->m_nx; i++) {
		//_pincmsg3("    i=%d, val=%f", i, inputBuf[i-1]);
		snd_temp->m_z[1][i] = m_sound->m_z[1][i];
	}

	PRPitch* tempPitch = PRManipulation::Sound_to_Pitch(
																	snd_temp, 
																	m_pitchTimeStep, 	// sec
																	MIN_PITCH,  // hz
																	MAX_PITCH	// hz
																	); 
	// Double the length
	PRPitch* pitch = new PRPitch();
	pitch->init(tempPitch->m_xmin, tempPitch->m_xmax*2, tempPitch->m_nx*2, tempPitch->m_dx, tempPitch->m_x1, tempPitch->m_ceiling, tempPitch->m_maxnCandidates);

	long idx = 1;
	for (i=1; i <= pitch->m_nx; i++) {
		pitch->m_frame[i]->m_nCandidates = 1;
		pitch->m_frame[i]->m_candidate[1]->m_frequency
			//= tempPitch->m_frame[1]->m_candidate[1]->m_frequency; 
			= tempPitch->m_frame[idx]->m_candidate[1]->m_frequency; 

		pitch->m_frame[i]->m_candidate[1]->m_strength
			//= tempPitch->m_frame[1]->m_candidate[1]->m_strength; 
			= tempPitch->m_frame[idx]->m_candidate[1]->m_strength; 
		idx++;
		if (idx > tempPitch->m_nx)
			idx = 1;
	}

	delete tempPitch;
	delete snd_temp;
#endif
	///////////////////////// end of TEST //////////////////////////////////


	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++++++ sec: %f - analyzePitch - after Sound_to_Pitch\n", sysTime);
	*/

	if (!pitch) {
		printf("[ERROR] analyzePitch: couldn't create pitch!!\n");
		return NULL;
	}

	//printf("    pitch=%f\n", pitch->m_frame[1]->m_candidate[1]->m_frequency);

	if (callback)
		callback(progOffset + progChunk*0.5, progMsg);
	// update m_pulses 
	if (m_pulses)
		delete m_pulses;

	// create m_pulses
	//printf("    calling Sound_Pitch_to_PointProcess_cc\n");

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++++++ sec: %f - analyzePitch - before Sound_to_Pitch_to_PointProcess_cc\n", sysTime);
	*/

	//
	// Second-most time-consuming
	//
	m_pulses = PRManipulation::Sound_Pitch_to_PointProcess_cc(m_sound, pitch,
											callback, progOffset+progChunk*0.5, progChunk*0.48);

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++++++ sec: %f - analyzePitch - after Sound_to_Pitch_to_PointProcess_cc\n", sysTime);
	*/

#if 0
	_pincmsg1("++++++++++++++++++++ result: Pitch +++++++++++++++++++++++++");
	_pincmsg6("+ xmin=%f, xmax=%f, nx=%d, dx=%f, x1=%f", 
			pitch->m_xmin,pitch->m_xmax,pitch->m_nx,
			pitch->m_dx,pitch->m_x1);

	for (long idx=1; idx <= pitch->m_nx; idx++) {
		_pincmsg4("+ #%d: freq=%f, str=%f", idx,
					pitch->m_frame[idx]->m_candidate[1]->m_frequency, 
					pitch->m_frame[idx]->m_candidate[1]->m_strength);
	}
	_pincmsg1("++++++++++++++++ end of result: Pitch ++++++++++++++++++++++");
#endif

	if (!m_pulses) {
		printf("[ERROR] analyzePitch: couldn't create m_pulses!!\n");
		delete pitch;
		return NULL;
	}

	//printf("    calling Pitch_to_PitchTier\n");
	PRPitchTier* origTier =  PRManipulation::Pitch_to_PitchTier(pitch);
	if (!origTier) {
		printf("[ERROR] analyzePitch: couldn't create origTier!!\n");
		delete pitch;
		return NULL;
	}

	// STAT:
	/*
	sysTime = (float)clock() / (float)CLOCKS_PER_SEC;
	printf("    +++++++ sec: %f - analyzePitch - after Pitch_to_PitchTier\n", sysTime);
	*/

	if (callback)
		callback(progOffset + progChunk*1.0, progMsg);

	// TODO: really need it?
	// Initially, m_pitchTier duplicates the original pitch tier
	//	with high slopes added
	//
	/*
	m_pitchTier = new PRPitchTier(origTier->m_xmin, origTier->m_xmax);

	for (i = 1; i <= origTier->m_setSize; i++) {
		t = origTier->m_points[i]->m_time;
		v = origTier->m_points[i]->m_value;
		m_pitchTier->addPoint(t, v);

		if (i < origTier->m_setSize) {
			t = origTier->m_points[i+1]->m_time - 0.01;
			v = origTier->m_points[i]->m_value;
			m_pitchTier->addPoint(t, v);
		}
	}
	*/

	//printf("PRManipulation::analyzePitch - end\n");

	// free
	delete pitch;
	// return the original tier, which will be kept by the caller
	return origTier;
}

//
// Alternative to analyzePitch() 
// - Creates 'm_pulses' and returns the pitch-tier using files
//
PRPitchTier* PRManipulation::analyzePitchByFiles(char *pathToPulses, char *pathToOrigTier, __progress_callback_t callback, float progOffset, float progChunk)
{
	printf("PRManipulation::analyzePitchByFiles - start\n");
	PRPitchTier* origTier = NULL;
	FILE *pFile;
	long i;
	char *progMsg = (char*)"Preparing...";
	double xmin;	
	double xmax;	
	long maxnt;	
	long nt;	
	double t;
	long setSize;	
	double time;
	double frequency;
	size_t ret;

	if (!m_sound)
		return NULL;

	printf("    calling Sound_to_Pitch\n");


	if (callback)
		callback(progOffset + progChunk*0.5, progMsg);
	// update m_pulses 
	if (m_pulses)
		delete m_pulses;

	// create m_pulses
	printf("    calling Sound_Pitch_to_PointProcess_cc\n");

	//
	// Build m_pulses
	//
	/* 
		PRPointProcess:
			double m_xmin;	
			double m_xmax;	
			long m_maxnt;		
			long m_nt;		
			double *m_t;
	*/
	m_pulses = NULL;
	pFile = fopen(pathToPulses, "rb");

	if (!pFile) {
		printf("Couldn't open pathToPulses file:%s\n", pathToPulses);
		return NULL;
	}

	if (fread(&xmin, 1, sizeof(double), pFile) != sizeof(double)) {
		fclose(pFile);
		printf("[ERROR] Failed to read xmin!!\n");
		return NULL;
	}
	printf("    xmin=%f\n", xmin);

	if (fread(&xmax, 1, sizeof(double), pFile) != sizeof(double)) {
		fclose(pFile);
		printf("[ERROR] Failed to read xmax!!\n");
		return NULL;;
	}
	printf("    xmax=%f\n", xmax);

	// Double check
	if (xmin != m_sound->m_xmin || xmax != m_sound->m_xmax) {
		printf("[WARNING] xmin and/or xmax inconsistent!!\n");
		printf("  xmin=%f, m_sound->m_xmin=%f, xmax=%f, m_sound->m_xmax=%f\n", xmin, m_sound->m_xmin, xmax, m_sound->m_xmax);
	}

	// Create new m_pulses
	m_pulses = new PRPointProcess(xmin, xmax, 10);

	if (fread(&maxnt, 1, sizeof(long), pFile) != sizeof(long)) {
		fclose(pFile);
		printf("[ERROR] Failed to read maxnt!!\n");
		return NULL;
	}
	printf("    maxnt=%ld\n", maxnt);

	if (fread(&nt, 1, sizeof(long), pFile) != sizeof(long)) {
		fclose(pFile);
		printf("[ERROR] Failed to read nt!!\n");
		return NULL;
	}
	printf("    nt=%ld\n", nt);

	for (i = 0; i < nt; i++) {
		ret = fread(&t, 1, sizeof(double), pFile);
		if (ret != sizeof(double))
			break;
		else {
			//printf("    adding a point into m_pulses at t=%f\n", t);
			m_pulses->addPoint(t);
		}
	}

	fclose(pFile);

	if (callback)
		callback(progOffset + progChunk*0.8, progMsg);
	
	if (!m_pulses) {
		printf("[ERROR] analyzePitchByFiles: couldn't create m_pulses!!\n");
		return NULL;
	}

	//
	// Build origTier
	//
	/* 
		PRPitchTier (PRRealTier):
			double m_xmin;		
			double m_xmax;		
			long m_setSize;
			RealPoint **m_points; // must be sorted all the time

			typedef struct {
				double m_time;
				double m_value;
			} RealPoint;
	*/
	pFile = fopen(pathToOrigTier, "rb");
	if (!pFile) {
		printf("Couldn't open pathToOrigTier file:%s\n", pathToOrigTier);
		return NULL;
	}
	if (fread(&xmin, 1, sizeof(double), pFile) != sizeof(double)) {
		fclose(pFile);
		printf("[ERROR] Failed to read xmin!!\n");
		return NULL;
	}
	if (fread(&xmax, 1, sizeof(double), pFile) != sizeof(double)) {
		fclose(pFile);
		printf("[ERROR] Failed to read xmax!!\n");
		return NULL;;
	}

	// Create new PRPitchTier
	origTier = new PRPitchTier(xmin, xmax);   /* Same domain, for synchronization. */

	if (fread(&setSize, 1, sizeof(long), pFile) != sizeof(long)) {
		fclose(pFile);
		printf("[ERROR] Failed to read setSize!!\n");
		return NULL;
	}

	for (i = 0; i < setSize; i++) {
		if (fread(&time, 1, sizeof(double), pFile) != sizeof(double))
			break;
		if (fread(&frequency, 1, sizeof(double), pFile) != sizeof(double))
			break;
		// Add point
		origTier->addPoint(time, frequency);
	}

	fclose(pFile);

	if (callback)
		callback(progOffset + progChunk*1.0, progMsg);


	printf("PRManipulation::analyzePitchByFiles - end\n");

	// return the original tier, which will be kept by the caller
	return origTier;
}

//
// NOTE: The caller thread should take care of syncing updatePitchTier() and playPart()
// update m_pitchTier
//
void PRManipulation::updatePitchTier(PRPitchTier* newTier0, PRPitchTier* newTier1)
{
	//printf("PRManipulation::updatePitchTier - deleting old m_pitchTier\n");

	//_pincmsg1("updatePitchTier - start");
	long i;
	//long maxSize;
	PRPitchTier *tierPtr;

	//
	// m_pitchTier0
	//
    if (newTier0) {
        tierPtr = m_pitchTier0;	// save to delete
        m_pitchTier0 = new PRPitchTier(newTier0->m_xmin, newTier0->m_xmax);

        for (i = 1; i <= newTier0->m_setSize; i++) {
            //printf("PRManipulation::updatePitchTier addPoint to m_pitchTier0: time=%f, value=%f\n", newTier0->m_points[i]->m_time, newTier0->m_points[i]->m_value);
            m_pitchTier0->addPoint(newTier0->m_points[i]->m_time, newTier0->m_points[i]->m_value);
        }

		delete tierPtr;
    } else {
        printf("Error                                     k - PRManipulation::updatePitchTier - No pitchTier0\n");
        if (m_pitchTier0)
            delete m_pitchTier0;
        m_pitchTier0 = NULL;
    }

	//
	// m_pitchTier1
	//
	if (newTier1) {
        tierPtr = m_pitchTier1;	// save to delete

		m_pitchTier1 = new PRPitchTier(newTier1->m_xmin, newTier1->m_xmax);

		for (i = 1; i <= newTier1->m_setSize; i++) {
			//printf("PRManipulation::updatePitchTier addPoint to m_pitchTier1: time=%f, value=%f\n", newTier1->m_points[i]->m_time, newTier1->m_points[i]->m_value);
			m_pitchTier1->addPoint(newTier1->m_points[i]->m_time, newTier1->m_points[i]->m_value);
		}
        delete tierPtr;

    } else {
        if (m_pitchTier1)
            delete m_pitchTier1;
        m_pitchTier1 = NULL;
    }


}

double PRManipulation::getFreqAtSec(double sec)
{
	// Use primary tier
	return m_pitchTier0->getValueAtTime(sec);
}

// Creates a new instance:
// static:
/*
PRManipulation* PRManipulation::Sound_to_Manipulation(PRSound *sound, double timeStep, double minimumPitch, double maximumPitch)
{
	PRPitch *pitch = NULL;

	PRManipulation *thee = new PRManipulation(sound->m_xmin, sound->m_xmax);

	thee->m_sound = sound->convertToMono(); // if already mono, duplicate it
	thee->m_sound->subtractMean();

	pitch = PRManipulation::Sound_to_Pitch(thee->m_sound, timeStep, minimumPitch, maximumPitch);

	thee->m_pulses = PRManipulation::Sound_Pitch_to_PointProcess_cc(thee->m_sound, pitch);

	thee->m_pitchTier = PRManipulation::Pitch_to_PitchTier(pitch);

	if (pitch)
		delete pitch;

	return thee;
}
*/

// 
// NOTE: The caller thread should take care of syncing updatePitchTier() and playPart()
//
// Process and returns processed buffer (16-int)
//
// Important!
// Process more buffer than required and return the required part, in order to
// prevent clicks
//
//
// tierIndex:
//		-1: original
//		0: primary pitch tier
//		1: secondary pitch tier
//
// origiDiffSemitones : Effective only when tierIndex == -1
//
float* PRManipulation::playPart(double _tmin, 
											double _tmax, 
											int method, 
											long *retBufLen, 	// in/out
											int tierIndex,
											int origDiffSemitones)
											//bool getOrig)
{
	//_pincmsg1("========================= playPart ========================");

	// Currently only support for 'overlap-and-add'

	//long i;
	PRSound *part_sound = NULL;
	PRPointProcess *part_pulses = NULL;
	PRPitchTier *part_pitchTier = NULL;
	PRSound *played = NULL;
	long imin, imax;
	long imin_proc, imax_proc;
	double tmin_proc, tmax_proc;
	//PRSound *saved = m_sound;
	//long i;
	//long imin, imax; 		// original index 
	//double *amp;

	// Apply m_durationFactor
	double tmin = _tmin/m_durationFactor;
	double tmax = _tmax/m_durationFactor;
	/*
	if (m_durationFactor > 1.0) // workaround: add more samples
		tmax += 1.0;
		*/

	if (!m_sound) {
		printf("[ERROR] playPart - No m_sound.\n");
		*retBufLen = 0;
		return NULL;
	} else {
		// See PRSound::copyPart() and conform indexes
		imin = m_sound->xToHighIndex(tmin);
		imax = m_sound->xToHighIndex(tmax)-1;
		if (imin < 1)
			imin = 1;
		if (imax > m_sound->m_nx) 
			imax = m_sound->m_nx;

		tmin_proc = tmin - MAX_T;
		tmax_proc = tmax + MAX_T;
		if (tmin_proc < 0.0)
			tmin_proc = 0.0;

		// See PRSound::copyPart() and conform indexes
		imin_proc = m_sound->xToHighIndex(tmin_proc);
		imax_proc = m_sound->xToHighIndex(tmax_proc)-1;
		if (imin_proc > imin)
			imin_proc = imin;
		if (imax_proc > m_sound->m_nx) 
			imax_proc = m_sound->m_nx;
		else if (imax_proc < imax)
			imax_proc = imax;

		// Copy from m_sound
		part_sound = m_sound->copyPart(tmin_proc, tmax_proc);
	}

	/*
		-------|-------|---------------------|--------|---------
		  tmin_proc  tmin                  tmax   tmax_proc
	*/
    if (part_sound) {
        //if (getOrig) {
		 if (tierIndex < 0) {
			 if (origDiffSemitones == 0) {
				// Return the original samples
				played = part_sound;
			 } else {
				// Copy from m_pulses
				part_pulses = m_pulses->copyPart(tmin_proc, tmax_proc);
				 // 
				 // Get original pitch tier and apply diff semitones
				 //
				 part_pitchTier = (PRPitchTier*)(m_origPitchTier->copyPart(tmin_proc, tmax_proc));
				 if (part_pitchTier) {
					for (int i = 1; i <= part_pitchTier->m_setSize; i++) {
						if (part_pitchTier->m_points[i]) {
							float orig_val = part_pitchTier->m_points[i]->m_value;
							// Apply diff semitones
							float new_val = midiToFreq(freqToMidi(orig_val)+origDiffSemitones);

							part_pitchTier->m_points[i]->m_value = new_val;
						}
					}
				 }

				played = Manipulation_to_Sound(Manipulation_OVERLAPADD, part_sound, part_pulses, part_pitchTier);

				// Remove duplicated ones
				delete part_pulses;
				delete part_pitchTier;
				delete part_sound;
			 }
        } else {
            // Copy from m_pulses
            part_pulses = m_pulses->copyPart(tmin_proc, tmax_proc);

            // Copy from m_pitchTier0 or m_pitchTier1
            if (tierIndex == 0)
                part_pitchTier = (PRPitchTier*)(m_pitchTier0->copyPart(tmin_proc, tmax_proc));
            else
                part_pitchTier = (PRPitchTier*)(m_pitchTier1->copyPart(tmin_proc, tmax_proc));

            played = Manipulation_to_Sound(Manipulation_OVERLAPADD, part_sound, part_pulses, part_pitchTier);

				// Remove duplicated ones
            delete part_pulses;
            delete part_pitchTier;
            delete part_sound;
        }
    } else {
		printf("[ERROR] playPart - Couldn't copy sound: tmin=%lf, tmax=%lf\n", tmin, tmax);
	}


	if (!played) {
		*retBufLen = 0;
		printf("[ERROR] playPart - Manipulation_to_sound fails\n");
		return NULL;
	}

	float* res = getFinalBuffer(played, 
							imin-imin_proc+1, 	// 1-based
							imax-imin_proc+1,
							retBufLen);
	delete played;

	return res;
}

// output: 0-based
//float* PRManipulation::getFinalBuffer(PRSound* snd, double tmin, double tmax, long *retBufLen)
float* PRManipulation::getFinalBuffer(PRSound* snd, long imin, long imax, long *retBufLen)
{
	float *res = NULL;
	float *bufPtr = NULL;

	// Sampling rate:
	//long ifsamp = floor(1.0 / snd->m_dx + 0.5);
	/*
	double *fromLeft = snd->m_z[1];
	double *fromRight = snd->m_ny > 1 ? snd->m_z[2] : NULL;
	*/
	long i1, i2;
	long numberOfSamples;
	int numberOfChannels = snd->m_ny > 1 ? 2 : 1;

	/*
	i1 = (long)((double)ifsamp * tmin);
	i2 = (long)((double)ifsamp * tmax);
	*/
	//snd->getWindowSamples(tmin, tmax, &i1, &i2);
	i1 = imin;
	i2 = imax;
	numberOfSamples = i2 - i1 +1;

	if (i1 > snd->m_nx)
		i1 = snd->m_nx;
	if (i2 > snd->m_nx)
		i2 = snd->m_nx;

	long bufLen = numberOfSamples * numberOfChannels;
	if (bufLen <= 0) {
		*retBufLen = 0;
		return NULL;
	}

	//thy buffer = NUMsvector (1, (i2 - i1 + 1) * numberOfChannels);
	//_pincmsg2("    allocating final buffer in getFinalBuffer: len=%d", bufLen);
	res = (float*)calloc(bufLen, sizeof(float));
	//res = (float*)malloc(bufLen*sizeof(float));

	*retBufLen = bufLen;
	bufPtr = res;

	/*
	if (numberOfChannels == 2) {
		// interleaving
		for (long i = i1; i <= i2; i++) {
			long valueLeft = (long)floor(fromLeft[i] * 32768.0 + 0.5);
			*(++bufPtr) = (short)(valueLeft < -32768 ? -32768 : valueLeft > 32767 ? 32767 : valueLeft);
			long valueRight = (long)floor(fromRight[i] * 32768.0 + 0.5);
			*(++bufPtr) = (short)(valueRight < -32768 ? -32768 : valueRight > 32767 ? 32767 : valueRight);
		}
	} else {
		for (long i = i1; i <= i2; i ++) {
			long value = (long)floor(fromLeft[i] * 32768.0 + 0.5);
			*(++bufPtr) = (short)(value < -32768 ? -32768 : value > 32767 ? 32767 : value);
		}
	}
	*/

	//
	// NOTE: output buffer is 0-based, not 1-based!!
	//
	if (numberOfChannels == 2) {
		// interleaving
		for (long i = i1; i <= i2; i++) {
			//float valueLeft = (float)fromLeft[i];
			float valueLeft = snd->getZ(1, i);
			*(bufPtr++) = valueLeft < -1.0 ? -1.0 : (valueLeft > 1.0 ? 1.0 : valueLeft);
			//float valueRight = (float)fromRight[i];
			float valueRight = snd->getZ(2, i);
			*(bufPtr++) = valueRight < -1.0 ? -1.0 : (valueRight > 1.0 ? 1.0 : valueRight);
		}
	} else {
		for (long i = i1; i <= i2; i ++) {
			//printf("    i=%ld\n", i);
			//float value = (float)fromLeft[i];
			float value = snd->getZ(1, i);
			//*(++bufPtr) = value < -1.0 ? -1.0 : (value > 1.0 ? 1.0 : value);
			*(bufPtr++) = value < -1.0 ? -1.0 : (value > 1.0 ? 1.0 : value);
		}
	}

	return res;
}

// Currently only support for 'overlap and add'
//PRSound* PRManipulation::Manipulation_to_Sound(int method, double tmin, double tmax, int tierIndex) 
PRSound* PRManipulation::Manipulation_to_Sound(int method, PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier) 
{
	return synthesize_overlapAdd(snd, pulses, tier);
	/*
	switch (method) {
		case Manipulation_OVERLAPADD: return synthesize_overlapAdd (me);
		case Manipulation_PULSES: return synthesize_pulses (me);
		case Manipulation_PULSES_HUM: return synthesize_pulses_hum (me);
		case Manipulation_PITCH: return synthesize_pitch (me);
		case Manipulation_PITCH_HUM: return synthesize_pitch_hum (me);
		case Manipulation_PULSES_PITCH: return synthesize_pulses_pitch (me);
		case Manipulation_PULSES_PITCH_HUM: return synthesize_pulses_pitch_hum (me);
		case Manipulation_OVERLAPADD_NODUR: return synthesize_overlapAdd_nodur (me);
		case Manipulation_PULSES_FORMANT: return 0;
		case Manipulation_PULSES_FORMANT_INTENSITY: return 0;
		case Manipulation_PULSES_LPC: return synthesize_pulses_lpc (me);
		case Manipulation_PULSES_LPC_INTENSITY: return 0;
		case Manipulation_PITCH_LPC: return synthesize_pitch_lpc (me);
		case Manipulation_PITCH_LPC_INTENSITY: return 0;
		default: return synthesize_overlapAdd (me);
	}
	*/
}

//PRSound* PRManipulation::synthesize_overlapAdd(double tmin, double tmax, int tierIndex) 
PRSound* PRManipulation::synthesize_overlapAdd(PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier) 
{
	PRSound *thee = NULL;
	//if (!m_duration || m_duration->m_points->m_size == 0) 
	if (!m_duration || m_duration->m_setSize == 0) {
		// Normally,  reaches here
		return synthesize_overlapAdd_nodur(snd, pulses, tier); // with no duration-tier
	}
	if (!m_pulses) {
		printf("[ERROR] synthesize_overlapAdd - Cannot synthesize overlap-add without pulses.\n");
		return NULL;
	}
	/*
	if ((tierIndex == 0 && !m_pitchTier0) || (tierIndex == 1 && !m_pitchTier1)) {
		printf("[ERROR] synthesize_overlapAdd - No pitch-tier!!\n");
		return NULL;
	}
	*/

	//
	if (!(thee = Sound_Point_Pitch_Duration_to_Sound(snd, pulses, tier, m_duration)))  {
		printf("[ERROR] synthesize_overlapAdd - Failed in Sound_Point_Pitch_Duration_to_Sound\n");
		return NULL;
	}
	
	return thee;
}

//PRSound* PRManipulation::synthesize_overlapAdd_nodur(double tmin, double tmax, int tierIndex) 
PRSound* PRManipulation::synthesize_overlapAdd_nodur(PRSound *snd, PRPointProcess *pulses, PRPitchTier *tier) 
{
	//_pincmsg1("=============== synthesize_overlapAdd_nodur ===============");
	PRPointProcess *targetPulses = NULL;
	PRSound *thee = NULL;
	if (!m_pulses) {
		printf("[ERROR] Cannot synthesize overlap-add without pulses analysis.\n");
		return NULL;
	}
	/*
	if ((tierIndex == 0 && !m_pitchTier0) || (tierIndex == 1 && !m_pitchTier1)) {
		printf("[ERROR] Cannot synthesize overlap-add without pitch manipulation.\n");
		return NULL;
	}

	if (tierIndex == 0) {
		if ((targetPulses = PRManipulation::PitchTier_Point_to_PointProcess(m_pitchTier0, m_pulses, tmin, tmax)) != NULL)
			thee = PRManipulation::Sound_Point_Point_to_Sound(m_sound, m_pulses, targetPulses, tmin, tmax);
	} else {
		if ((targetPulses = PRManipulation::PitchTier_Point_to_PointProcess(m_pitchTier1, m_pulses, tmin, tmax)) != NULL)
			thee = PRManipulation::Sound_Point_Point_to_Sound(m_sound, m_pulses, targetPulses, tmin, tmax);
	}
	*/
	if ((targetPulses = PRManipulation::PitchTier_Point_to_PointProcess(tier, pulses)) != NULL)
		thee = PRManipulation::Sound_Point_Point_to_Sound(snd, pulses, targetPulses);
	else
		printf("[ERROR] synthesize_overlapAdd_nodur: Can't get targetPulses!\n");

	delete targetPulses;

	return thee;
}

// static
PRPointProcess* PRManipulation::PitchTier_Point_to_PointProcess(PRPitchTier *pitch, 
																					PRPointProcess *vuv)
{
	//_pincmsg1("---- PitchTier_Point_to_PointProcess ----");
	PRPointProcess* fullPoint = NULL;
	PRPointProcess* thee = NULL;
	long i;
	if ((fullPoint = PRManipulation::PitchTier_to_PointProcess(pitch)) == NULL) {
		printf("[WARNING] PRManipulation::PitchTier_Point_to_PointProcess - No fullPoint\n");
		return NULL;
	}
	//if ((thee = PointProcess_create(my xmin, my xmax, fullPoint -> maxnt)) == NULL) 

	//_pincmsg4("  pitch->m_xmin=%f, pitch->m_xmax=%f, fullPoint->m_maxnt=%d",
					//pitch->m_xmin, pitch->m_xmax, fullPoint->m_maxnt);

	thee = new PRPointProcess(pitch->m_xmin, pitch->m_xmax, fullPoint->m_maxnt);

	/*
	 * Copy only voiced parts to result.
	 */
	for (i = 1; i <= fullPoint->m_nt; i++) {
		double t = fullPoint->m_t[i];
		//_pincmsg3("        i=%d: t=%f", i, t);
		if (PRManipulation::PointProcess_isVoiced_t(vuv, t) && !thee->addPoint(t)) 
			goto cleanUp;
	}

cleanUp:
	if (fullPoint)
		delete fullPoint;
	return thee;
}

// static
int PRManipulation::PointProcess_isVoiced_t(PRPointProcess *point, double t) 
{
	long imid = point->getNearestIndex(t);
	double tmid;
	int leftVoiced, rightVoiced;
	if (imid == 0) 
		return 0;
	tmid = point->m_t[imid];

	leftVoiced = imid > 1 && tmid - point->m_t[imid - 1] <= MAX_T;
	rightVoiced = imid < point->m_nt && point->m_t[imid + 1] - tmid <= MAX_T;

	if ((leftVoiced && t <= tmid) || (rightVoiced && t >= tmid)) 
		return 1;
	if (leftVoiced && t < 1.5 * tmid - 0.5 * point->m_t[imid - 1])
		return 1;
	if (rightVoiced && t > 1.5 * tmid - 0.5 * point->m_t[imid + 1])
		return 1;
	return 0;
}

// static:
PRPointProcess* PRManipulation::PitchTier_to_PointProcess(PRPitchTier *pitch) 
{
	//_pincmsg1("---- PitchTier_to_PointProcess ----");
	//PRPointProcess *thee = PointProcess_create (my xmin, my xmax, 1000);
	PRPointProcess *thee = new PRPointProcess(pitch->m_xmin, pitch->m_xmax, 3000);
	double area = 0.5;   /* Imagine an event half a period before the beginning. */
	long interval;
	//long size = pitch->m_points->size;
	long size = pitch->m_setSize;
	if (!thee) {
		printf("[WARNING] PRManipulation::PitchTier_to_PointProcess - No thee\n");
		return NULL;
	}
	//_pincmsg2("    size=%d", size);
	if (size == 0) 
		return thee;
	for (interval = 0; interval <= size; interval++) {
		//double t1 = interval == 0 ? pitch->m_xmin : ((RealPoint)pitch->points->item[interval])->time;
		double t1 = interval == 0 ? pitch->m_xmin : ((RealPoint*)pitch->m_points[interval])->m_time;
		//Melder_assert (NUMdefined (t1));
		//double t2 = interval == size ? pitch->m_xmax : ((RealPoint)pitch->points->item[interval + 1])->time;
		double t2 = interval == size ? pitch->m_xmax : ((RealPoint*)(pitch->m_points[interval + 1]))->m_time;

		//Melder_assert (NUMdefined (t2));
		//double f1 = ((RealPoint)pitch->m_points->item[interval == 0 ? 1 : interval])->value;
		double f1 = ((RealPoint*)(pitch->m_points[interval == 0 ? 1 : interval]))->m_value;
		//Melder_assert (NUMdefined (f1));
		//double f2 = ((RealPoint)pitch->m_points->item[interval == size ? size : interval + 1])->value;
		double f2 = ((RealPoint*)(pitch->m_points[interval == size ? size : interval + 1]))->m_value;
		//Melder_assert (NUMdefined (f2));
		area += (t2 - t1) * 0.5 * (f1 + f2);

		while (area >= 1.0) {
			double slope = (f2 - f1) / (t2 - t1);
			double discriminant;
			area -= 1.0;
			discriminant = f2 * f2 - 2.0 * area * slope;
			if (discriminant < 0.0) 
				discriminant = 0.0;   /* Catch rounding errors. */
			if (!thee->addPoint(t2 - 2.0 * area / (f2 + sqrt(discriminant)))) 
				return NULL;
		}
	}
	return thee;
}

//
// Invoked when playing
//
// static:
PRSound* PRManipulation::Sound_Point_Point_to_Sound(PRSound* snd, PRPointProcess* source, PRPointProcess* target) 
{
	//_pincmsg1("============ Sound_Point_Point_to_Sound");
	long i;
	PRSound *thee = new PRSound();
	thee->init(1, snd->m_xmin, snd->m_xmax, snd->m_nx, snd->m_dx, snd->m_x1);

	if (!thee) 
		return NULL;

	// TODO:
	if (source->m_nt < 2 || target->m_nt < 2) {   /* Almost completely voiceless? */
		NUMdvector_copyElements(snd->m_z[1], thee->m_z[1], 1, snd->m_nx);
		return thee;
	}
		
	for (i = 1; i <= target->m_nt; i++) {
		double tmid = target->m_t[i];
		double tleft = i > 1 ? target->m_t[i - 1] : snd->m_xmin;
		double tright = i < target->m_nt ? target->m_t[i + 1] : snd->m_xmax;
		double leftWidth = tmid - tleft;
		double rightWidth = tright - tmid;
		int leftVoiced = i > 1 && leftWidth <= MAX_T;
		int rightVoiced = i < target->m_nt && rightWidth <= MAX_T;

		long isource = source->getNearestIndex(tmid);

		/*
		_pincmsg7("   i=%d: tmid=%f, tleft=%f, tright=%f, leftWidth=%f, rightWidth=%f",
						  i, tmid, tleft, tright, leftWidth, rightWidth);
		_pincmsg4("   leftVoiced=%d, rightVoiced=%d, isource=%d",
						  leftVoiced, rightVoiced, isource);
						  */

		if (!leftVoiced) 
			leftWidth = rightWidth;   /* Symmetric bell. */
		if (!rightVoiced) 
			rightWidth = leftWidth;   /* Symmetric bell. */
		if (leftVoiced || rightVoiced) {
			PRManipulation::copyBell2(snd, source, isource, leftWidth, rightWidth, thee, tmid);
			if (!leftVoiced) {
				double startOfFlat = i == 1 ? tleft : (tleft + tmid) / 2;
				double endOfFlat = tmid - leftWidth;
				PRManipulation::copyFlat(snd, startOfFlat, endOfFlat, thee, startOfFlat);
				PRManipulation::copyFall(snd, endOfFlat, tmid, thee, endOfFlat);
			} else if (!rightVoiced) {
				double startOfFlat = tmid + rightWidth;
				double endOfFlat = i == target->m_nt ? tright : (tmid + tright) / 2;
				PRManipulation::copyRise(snd, tmid, startOfFlat, thee, startOfFlat);
				PRManipulation::copyFlat(snd, startOfFlat, endOfFlat, thee, startOfFlat);
			}
		} else {
			double startOfFlat = i == 1 ? tleft : (tleft + tmid) / 2;
			double endOfFlat = i == target->m_nt ? tright : (tmid + tright) / 2;
			PRManipulation::copyFlat(snd, startOfFlat, endOfFlat, thee, startOfFlat);
		}
	}
		
	return thee;
}

// static
void PRManipulation::copyRise(PRSound *snd, double tmin, double tmax, PRSound* thee, double tmaxTarget) 
{
	long imin, imax, imaxTarget, distance, i;
	double dphase;
	imin = snd->xToHighIndex(tmin);

	if (imin < 1) 
		imin = 1;
	imax = snd->xToHighIndex(tmax) - 1;   /* Not xToLowIndex: ensure separation of subsequent calls. */
	if (imax > snd->m_nx) 
		imax = snd->m_nx;
	if (imax < imin) 
		return;
	imaxTarget = thee->xToHighIndex(tmaxTarget) - 1;
	distance = imaxTarget - imax;
	dphase = NUMpi / (imax - imin + 1);

	if (!thee->m_z)
		printf("[ERROR] PRManipulation::copyRise - No thee->m_z. It will crash\n");

	for (i = imin; i <= imax; i++) {
		long iTarget = i + distance;
		if (iTarget >= 1 && iTarget <= thee->m_nx) {
			//thee->m_z[1][iTarget] += snd->m_z[1][i] * 0.5 * (1 - cos(dphase * (i - imin + 0.5)));
			thee->m_z[1][iTarget] += snd->getZ(1, i) * 0.5 * (1 - cos(dphase * (i - imin + 0.5)));
		}
	}
}

// static
void PRManipulation::copyFall(PRSound *snd, double tmin, double tmax, PRSound *thee, double tminTarget) 
{
	long imin, imax, iminTarget, distance, i;
	double dphase;
	imin = snd->xToHighIndex(tmin);
	if (imin < 1) 
		imin = 1;
	imax = snd->xToHighIndex(tmax) - 1;   /* Not xToLowIndex: ensure separation of subsequent calls. */
	if (imax > snd->m_nx) 
		imax = snd->m_nx;
	if (imax < imin) 
		return;
	iminTarget = thee->xToHighIndex(tminTarget);
	distance = iminTarget - imin;
	dphase = NUMpi / (imax - imin + 1);

	if (!thee->m_z)
		printf("[ERROR] PRManipulation::copyFall - No thee->m_z. It will crash\n");

	for (i = imin; i <= imax; i ++) {
		long iTarget = i + distance;
		if (iTarget >= 1 && iTarget <= thee->m_nx) {
			//thee->m_z[1][iTarget] += snd->m_z[1][i] * 0.5 * (1 + cos(dphase * (i - imin + 0.5)));
			thee->m_z[1][iTarget] += snd->getZ(1, i) * 0.5 * (1 + cos(dphase * (i - imin + 0.5)));
		}
	}
}

// static
void PRManipulation::copyBell(PRSound *snd, double tmid, double leftWidth, double rightWidth, PRSound *thee, double tmidTarget) 
{
	PRManipulation::copyRise(snd, tmid - leftWidth, tmid, thee, tmidTarget);
	PRManipulation::copyFall(snd, tmid, tmid + rightWidth, thee, tmidTarget);
}

// static
void PRManipulation::copyBell2(PRSound *snd, PRPointProcess *source, long isource, double leftWidth, double rightWidth, PRSound *thee, double tmidTarget)
{
	/*
	 * Replace 'leftWidth' and 'rightWidth' by the lengths of the intervals in the source (instead of target),
	 * if these are shorter.
	 */
	double tmid = source->m_t[isource];
	if (isource > 1 && tmid - source->m_t[isource - 1] <= MAX_T) {
		double sourceLeftWidth = tmid - source->m_t[isource - 1];
		if (sourceLeftWidth < leftWidth) 
			leftWidth = sourceLeftWidth;
	}
	if (isource < source->m_nt && source->m_t[isource + 1] - tmid <= MAX_T) {
		double sourceRightWidth = source->m_t[isource + 1] - tmid;
		if (sourceRightWidth < rightWidth) 
			rightWidth = sourceRightWidth;
	}
	PRManipulation::copyBell(snd, tmid, leftWidth, rightWidth, thee, tmidTarget);
}

// static
void PRManipulation::copyFlat(PRSound *snd, double tmin, double tmax, PRSound* thee, double tminTarget) 
{
	long imin, imax, iminTarget;
	imin = snd->xToHighIndex(tmin);
	if (imin < 1) 
		imin = 1;
	imax = snd->xToHighIndex(tmax) - 1;   /* Not xToLowIndex: ensure separation of subsequent calls. */
	if (imax > snd->m_nx) 
		imax = snd->m_nx;
	if (imax < imin) 
		return;
	iminTarget = thee->xToHighIndex(tminTarget);

	// TODO:
	NUMdvector_copyElements(snd->m_z[1] + imin, thee->m_z[1] + iminTarget, 0, imax - imin);
}

PRSound* PRManipulation::Sound_Point_Pitch_Duration_to_Sound(PRSound* snd, PRPointProcess* pulses,
													PRPitchTier* pitch, PRDurationTier* duration)
{
	PRSound* thee = NULL;
	long ipointleft, ipointright = 1;
	double deltat = 0;
	double handledTime = snd->m_xmin;
	double startOfSourceNoise, endOfSourceNoise, startOfTargetNoise, endOfTargetNoise;
	double durationOfSourceNoise, durationOfTargetNoise;
	double startOfSourceVoice, endOfSourceVoice, startOfTargetVoice, endOfTargetVoice;
	double durationOfSourceVoice, durationOfTargetVoice;
	double startingPeriod, finishingPeriod, ttarget, voicelessPeriod;
	if (duration->m_setSize == 0) {
		printf("[ERROR] Sound_Point_Pitch_Duration_to_Sound: No duration points.\n");
		return NULL;
	}

	/*
	 * Create a Sound long enough to hold the longest possible duration-manipulated sound.
	 */
	//thee = Sound_create (1, my xmin, my xmin + 3 * (my xmax - my xmin), 3 * my nx, my dx, my x1);
	thee = new PRSound();
	thee->init(1, snd->m_xmin, snd->m_xmin + 3 * (snd->m_xmax - snd->m_xmin), 3 * snd->m_nx, snd->m_dx, snd->m_x1);

	if (!thee) {
		printf("[ERROR] Sound_Point_Pitch_Duration_to_Sound: No thee (PRSound).\n");
		return NULL;
	}

	/*
	 * Below, I'll abbreviate the voiced interval as "voice" and the voiceless interval as "noise".
	 */
	if (pitch && pitch->m_setSize) 
	{
		if (pulses->m_nt < 1)
			printf("Warning - Sound_Point_Pitch_Duration_to_Sound: No pulses\n");

		for (ipointleft = 1; ipointleft <= pulses->m_nt; ipointleft = ipointright + 1) 
		{
		/*
		 * Find the beginning of the voice.
		 */
		startOfSourceVoice = pulses->m_t[ipointleft];   /* The first pulse of the voice. */
		startingPeriod = 1.0 / pitch->getValueAtTime(startOfSourceVoice);
		startOfSourceVoice -= 0.5 * startingPeriod;   /* The first pulse is in the middle of a period. */

		/*
		 * Measure one noise.
		 */
		startOfSourceNoise = handledTime;
		endOfSourceNoise = startOfSourceVoice;
		durationOfSourceNoise = endOfSourceNoise - startOfSourceNoise;
		startOfTargetNoise = startOfSourceNoise + deltat;
		endOfTargetNoise = startOfTargetNoise + duration->getArea(startOfSourceNoise, endOfSourceNoise);
		durationOfTargetNoise = endOfTargetNoise - startOfTargetNoise;

		/*
		 * Copy the noise.
		 */
		voicelessPeriod = NUMrandomUniform (0.008, 0.012);
		//voicelessPeriod = NUMrandomUniform (0.0008, 0.0012); // TEST
		ttarget = startOfTargetNoise + 0.5 * voicelessPeriod;
		while (ttarget < endOfTargetNoise) {
			double tsource;
			double tleft = startOfSourceNoise, tright = endOfSourceNoise;
			int i;
			for (i = 1; i <= 15; i ++) {
				double tsourcemid = 0.5 * (tleft + tright);
				double ttargetmid = startOfTargetNoise + duration->getArea(startOfSourceNoise, tsourcemid);
				if (ttargetmid < ttarget) 
					tleft = tsourcemid; 
				else 
					tright = tsourcemid;
			}
			tsource = 0.5 * (tleft + tright);
			PRManipulation::copyBell(snd, tsource, voicelessPeriod, voicelessPeriod, thee, ttarget);
			voicelessPeriod = NUMrandomUniform (0.008, 0.012); 
			//voicelessPeriod = NUMrandomUniform (0.0008, 0.0012); // TEST
			ttarget += voicelessPeriod;
		}
		deltat += durationOfTargetNoise - durationOfSourceNoise;

		/*
		 * Find the end of the voice.
		 */
		for (ipointright = ipointleft + 1; ipointright <= pulses->m_nt; ipointright++)
			if (pulses->m_t[ipointright] - pulses->m_t[ipointright - 1] > MAX_T)
				break;
		ipointright--;
		endOfSourceVoice = pulses->m_t[ipointright];   /* The last pulse of the voice. */
		finishingPeriod = 1.0 / pitch->getValueAtTime(endOfSourceVoice);
		endOfSourceVoice += 0.5 * finishingPeriod;   /* The last pulse is in the middle of a period. */
		/*
		 * Measure one voice.
		 */
		durationOfSourceVoice = endOfSourceVoice - startOfSourceVoice;

		/*
		 * This will be copied to an interval with a different location and duration.
		 */
		startOfTargetVoice = startOfSourceVoice + deltat;
		endOfTargetVoice = startOfTargetVoice + duration->getArea(startOfSourceVoice, endOfSourceVoice);
		durationOfTargetVoice = endOfTargetVoice - startOfTargetVoice;

		/*
		 * Copy the voiced part.
		 */
		ttarget = startOfTargetVoice + 0.5 * startingPeriod;
		while (ttarget < endOfTargetVoice) {
			double tsource, period;
			long isourcepulse;
			double tleft = startOfSourceVoice, tright = endOfSourceVoice;
			int i;
			for (i = 1; i <= 15; i ++) {
				double tsourcemid = 0.5 * (tleft + tright);
				double ttargetmid = startOfTargetVoice + duration->getArea(startOfSourceVoice, tsourcemid);
				if (ttargetmid < ttarget) 
					tleft = tsourcemid; 
				else 
					tright = tsourcemid;
			}
			tsource = 0.5 * (tleft + tright);
			period = 1.0 / pitch->getValueAtTime(tsource);
			isourcepulse = pulses->getNearestIndex(tsource);
			PRManipulation::copyBell2(snd, pulses, isourcepulse, period, period, thee, ttarget);
			ttarget += period;
		}
		deltat += durationOfTargetVoice - durationOfSourceVoice;
		handledTime = endOfSourceVoice;
		}
	} else {
		printf("Warning - Sound_Point_Pitch_Duration_to_Sound: No pitch object\n");
	}

	/*
	 * Copy the remaining unvoiced part, if we are at the end.
	 */
	startOfSourceNoise = handledTime;
	endOfSourceNoise = snd->m_xmax;
	durationOfSourceNoise = endOfSourceNoise - startOfSourceNoise;
	startOfTargetNoise = startOfSourceNoise + deltat;
	endOfTargetNoise = startOfTargetNoise + duration->getArea(startOfSourceNoise, endOfSourceNoise);
	durationOfTargetNoise = endOfTargetNoise - startOfTargetNoise;
	//voicelessPeriod = NUMrandomUniform(0.008, 0.012);
	voicelessPeriod = NUMrandomUniform (0.0008, 0.0012); // TEST
	ttarget = startOfTargetNoise + 0.5 * voicelessPeriod;
	while (ttarget < endOfTargetNoise) {
		double tsource;
		double tleft = startOfSourceNoise;
		double tright = endOfSourceNoise;
		int i;
		for (i = 1; i <= 15; i ++) {
			double tsourcemid = 0.5 * (tleft + tright);
			double ttargetmid = startOfTargetNoise + duration->getArea(startOfSourceNoise, tsourcemid);
			if (ttargetmid < ttarget) 
				tleft = tsourcemid; 
			else 
				tright = tsourcemid;
		}
		tsource = 0.5 * (tleft + tright);
		PRManipulation::copyBell(snd, tsource, voicelessPeriod, voicelessPeriod, thee, ttarget);
		voicelessPeriod = NUMrandomUniform(0.008, 0.012);
		//voicelessPeriod = NUMrandomUniform (0.0008, 0.0012); // TEST
		ttarget += voicelessPeriod;
	}

	/*
	 * Find the number of trailing zeroes and hack the sound's time domain.
	 */
	thee->m_xmax = thee->m_xmin + duration->getArea(snd->m_xmin, snd->m_xmax);
	if (fabs(thee->m_xmax - snd->m_xmax) < 1e-12) 
		thee->m_xmax = snd->m_xmax;   /* Common situation. */
	thee->m_nx = thee->xToLowIndex(thee->m_xmax);
	if (thee->m_nx > 3 * snd->m_nx) 
		thee->m_nx = 3 * snd->m_nx;

	return thee;
}

//////////////////////////////////////////////////////////////////////////////////
//
//	From fon/Sound_to_Pitch.c
//
// all 'static'
//
/////////////////////////////////////////////////////////////////////////////////

PRPitch* PRManipulation::Sound_to_Pitch(PRSound *snd, 
													double timeStep, 
													double minimumPitch, 
													double maximumPitch,
													__progress_callback_t callback, 
													float progOffset, 
													float progChunk)
{
	// sensitive parameter:
	int maxnCandidates = MAX_NCANDID;		 // default = 15

	// sensitive parameter:
	double periodsPerWindow = PERIODS_PER_WINDOW;

	//printf("Sound_to_Pitch - start\n");
	double duration = snd->m_dx * snd->m_nx;
	if (duration <= 0.0) {
		printf("[WARNING] Sound_to_Pitch: duration=%f\n", duration);
	}
	//printf("Sound_to_Pitch: duration=%f, periodsPerWindow=%f\n", duration, periodsPerWindow);

	if (minimumPitch < periodsPerWindow / duration) {
		periodsPerWindow = minimumPitch * duration;
		//minimumPitch = periodsPerWindow / duration;
		printf("[WARNING] Sound_to_Pitch: changing minimumPitch to %f\n", minimumPitch);
	}

	return Sound_to_Pitch_ac(snd, 
									timeStep, 
									minimumPitch,
									periodsPerWindow,		// periodsPerWindow
									maxnCandidates, 		// maxnCandidates
									AC_HANNING,//	false, 	// accurate : 'false' == 'AC_HANNING'
									SILENCE_THRESHOLD, 	// silenceThreshold = 0.03
									VOICING_THRESHOLD,		// voicingThreshold = 0.45
									0.01, 	// octaveCost			= 0.01
									0.35, 	// octaveJumpCost		= 0.35
									0.14, 	// voicedUnvoicedCost = 0.14
									maximumPitch,
									callback,
									progOffset,
									progChunk);
}

PRPitch* PRManipulation::Sound_to_Pitch_any(PRSound *snd,
														double dt, 
														double minimumPitch, 
														double periodsPerWindow, 
														int maxnCandidates,
														int method, 		// 'acurate'
														double silenceThreshold, 
														double voicingThreshold,
														double octaveCost, 
														double octaveJumpCost, 
														double voicedUnvoicedCost, 
														double ceiling,
														__progress_callback_t callback,
														float progOffset,
														float progChunk)
{
	//printf("Sound_to_Pitch_any - start\n");

	long channel;
	if (snd->m_ny > MAXIMUM_NUMBER_OF_CHANNELS) {
		printf("[ERROR] Pitch analysis cannot yet work with more than two channels.\n");
		return NULL;
	}
	double duration, t1;
	PRPitch *thee = NULL;

	long i, j;
	double dt_window;   /* Window length in seconds. */
	long nsamp_window, halfnsamp_window;   /* Number of samples per window. */
	long nFrames, minimumLag, maximumLag;
	long iframe, nsampFFT, *imax = NULL;
	double **frame = NULL, *ac = NULL, *r = NULL, *window = NULL, *windowR = NULL, globalPeak;
	double interpolation_depth;
	long nsamp_period, halfnsamp_period;   /* Number of samples in longest period. */
	long brent_ixmax, brent_depth;
	double brent_accuracy;   /* Obsolete. */
	//struct NUMfft_Table fftTable = { 0 };
	struct _NUMfft_Table fftTable = { 0 };
	//NUMfft_Table fftTable = { 0 };

	//_pincmsg2("## maxnCandidates=%d", maxnCandidates);
	//Melder_assert (maxnCandidates >= 2);
	//Melder_assert (method >= AC_HANNING && method <= FCC_ACCURATE);
	if (maxnCandidates < 2) {
		printf("[ERROR] Sound_to_Pitch_any: maxnCandidates=%d\n",maxnCandidates);
		return NULL;
	}

	if (maxnCandidates < ceiling / minimumPitch) 
		maxnCandidates = ceiling / minimumPitch;

	if (dt <= 0.0) 
		dt = periodsPerWindow / minimumPitch / 4.0;   /* e.g. 3 periods, 75 Hz: 10 milliseconds. */

	//Melder_progress1 (0.0, L"Sound to Pitch...");
	switch (method) {
		case AC_HANNING:
			brent_depth = NUM_PEAK_INTERPOLATE_SINC70;
			brent_accuracy = 1e-7;
			interpolation_depth = 0.5;
			break;
		case AC_GAUSS:
			periodsPerWindow *= 2;   /* Because Gaussian window is twice as long. */
			brent_depth = NUM_PEAK_INTERPOLATE_SINC700;
			brent_accuracy = 1e-11;
			interpolation_depth = 0.25;   /* Because Gaussian window is twice as long. */
			break;
		case FCC_NORMAL:
			brent_depth = NUM_PEAK_INTERPOLATE_SINC70;
			brent_accuracy = 1e-7;
			interpolation_depth = 1.0;
			break;
		case FCC_ACCURATE:
			brent_depth = NUM_PEAK_INTERPOLATE_SINC700;
			brent_accuracy = 1e-11;
			interpolation_depth = 1.0;
			break;
	}
	duration = snd->m_dx * snd->m_nx;
	//printf("## duration=%f\n", duration);
	if (minimumPitch < periodsPerWindow / duration) {
		printf("[ERROR] For this Sound, the parameter 'minimum pitch' may not be less than %fHz: minimumPitch=%f, duration=%f\n", (periodsPerWindow / duration), minimumPitch, duration);
		return NULL;
	}

	/*
	 * Determine the number of samples in the longest period.
	 * We need this to compute the local mean of the sound (looking one period in both directions),
	 * and to compute the local peak of the sound (looking half a period in both directions).
	 */
	nsamp_period = floor(1 / snd->m_dx / minimumPitch);
	halfnsamp_period = nsamp_period / 2 + 1;

	if (ceiling > 0.5 / snd->m_dx) 
		ceiling = 0.5 / snd->m_dx;

	/*
	 * Determine window length in seconds and in samples.
	 */
	dt_window = periodsPerWindow / minimumPitch;
	nsamp_window = floor (dt_window / snd->m_dx);
	halfnsamp_window = nsamp_window / 2 - 1;
	if (halfnsamp_window < 2) {
		printf("[ERROR] Analysis window too short.\n");
		return NULL;
	}
	nsamp_window = halfnsamp_window * 2;

	//_pincmsg3("## nsamp_period=%d, halfnsamp_period=%d", nsamp_period, halfnsamp_period);
	//_pincmsg4("## dt_window=%f, nsamp_window=%d, halfnsamp_window=%d", dt_window, nsamp_window, halfnsamp_window);
	/*
	 * Determine the minimum and maximum lags.
	 */
	minimumLag = floor(1 / snd->m_dx / ceiling);
	if (minimumLag < 2) 
		minimumLag = 2;
	maximumLag = floor(nsamp_window / periodsPerWindow) + 2;
	if (maximumLag > nsamp_window) 
		maximumLag = nsamp_window;

	//_pincmsg3("## minimumLag=%d, maximumLag=%d", minimumLag, maximumLag);
	/*
	 * Determine the number of frames.
	 * Fit as many frames as possible symmetrically in the total duration.
	 * We do this even for the forward cross-correlation method,
	 * because that allows us to compare the two methods.
	 */
	//printf("    shortTermAnalysis\n");
	//if (! Sampled_shortTermAnalysis (me, method >= FCC_NORMAL ? 1 / minimumPitch + dt_window : dt_window, dt, & nFrames, & t1))
	if (!snd->shortTermAnalysis(method >= FCC_NORMAL ? 1 / minimumPitch + dt_window : dt_window, dt, &nFrames, &t1)) {
		printf("[ERROR] Sound_to_Pitch_any: Failed in shortTermAnalysis.\n");
		goto end;
	}

	//_pincmsg4("## after shortTermAnalysis: nFrames=%d, t1=%f, dt=%f", nFrames, t1, dt);
	/*
	 * Create the resulting pitch contour.
	 */
	//thee = Pitch_create (snd->m_xmin, snd->m_xmax, nFrames, dt, t1, ceiling, maxnCandidates); cherror
	//printf("    PRPitch\n");
	thee = new PRPitch();
	thee->init(snd->m_xmin, snd->m_xmax, nFrames, dt, t1, ceiling, maxnCandidates); 

	/*
	 * Compute the global absolute peak for determination of silence threshold.
	 */
	globalPeak = 0.0;
	for (channel = 1; channel <= snd->m_ny; channel++) {
		double mean = 0.0;
		for (i = 1; i <= snd->m_nx; i++) {
			//mean += snd->m_z[channel][i];
			mean += snd->getZ(channel, i);
		}
		mean /= snd->m_nx;
		for (i = 1; i <= snd->m_nx; i ++) {
			//double value = fabs(snd->m_z[channel][i] - mean);
			double value = fabs(snd->getZ(channel, i) - mean);
			if (value > globalPeak) 
				globalPeak = value;
		}
	}
	if (globalPeak == 0.0) { 
		//Melder_progress1 (1.0, NULL); 
		printf("[WARNING] globalPeak == 0.0\n");
		return thee; 
	}

	//_pincmsg2("## globalPeak=%f", globalPeak);

	if (method >= FCC_NORMAL) {   /* For cross-correlation analysis. */

		/*
		* Create buffer for cross-correlation analysis.
		*/
		frame = NUMdmatrix (1, snd->m_ny, 1, nsamp_window);

		brent_ixmax = nsamp_window * interpolation_depth;

	} else {   /* For autocorrelation analysis. */

		/*
		* Compute the number of samples needed for doing FFT.
		* To avoid edge effects, we have to append zeroes to the window.
		* The maximum lag considered for maxima is maximumLag.
		* The maximum lag used in interpolation is nsamp_window * interpolation_depth.
		*/
		nsampFFT = 1; 
		while (nsampFFT < nsamp_window * (1 + interpolation_depth)) 
			nsampFFT *= 2;

		//_pincmsg2("## nsampFFT=%d", nsampFFT);
		/*
		* Create buffers for autocorrelation analysis.
		*/
		frame = NUMdmatrix(1, snd->m_ny, 1, nsampFFT);
		windowR = NUMdvector(1, nsampFFT);
		window = NUMdvector(1, nsamp_window);
		NUMfft_Table_init(&fftTable, nsampFFT);
		ac = NUMdvector(1, nsampFFT);

		//
		// taegoo:
		// In order to apply phase effect, skip this part?
		//
		/*
		* A Gaussian or Hanning window is applied against phase effects.
		* The Hanning window is 2 to 5 dB better for 3 periods/window.
		* The Gaussian window is 25 to 29 dB better for 6 periods/window.
		*/
		if (method == AC_GAUSS) {   /* Gaussian window. */
			double imid = 0.5 * (nsamp_window + 1), edge = exp (-12.0);
			for (i = 1; i <= nsamp_window; i ++)
				window [i] = (exp (-48.0 * (i - imid) * (i - imid) /
								(nsamp_window + 1) / (nsamp_window + 1)) - edge) / (1 - edge);
		} else {   /* Hanning window. */
			for (i = 1; i <= nsamp_window; i++)
				window [i] = 0.5 - 0.5 * cos(i * 2 * NUMpi / (nsamp_window + 1));
		}
#if 0
		for (i = 1; i <= nsamp_window; i++)
			window [i] = 1.0;
#endif

		/*
		* Compute the normalized autocorrelation of the window.
		*/
		for (i = 1; i <= nsamp_window; i ++) 
			windowR[i] = window[i];
		NUMfft_forward(&fftTable, windowR);
		windowR[1] *= windowR[1];   /* DC component. */
		for (i = 2; i < nsampFFT; i += 2) {
			windowR[i] = windowR[i] * windowR[i] + windowR[i+1] * windowR[i+1];
			windowR[i + 1] = 0.0;   /* Power spectrum: square and zero. */
		}
		windowR[nsampFFT] *= windowR[nsampFFT];   /* Nyquist frequency. */
		NUMfft_backward(&fftTable, windowR);   /* Autocorrelation. */

		for (i = 2; i <= nsamp_window; i ++) 
			windowR[i] /= windowR[1];   /* Normalize. */
		windowR[1] = 1.0;   /* Normalize. */

		brent_ixmax = nsamp_window * interpolation_depth;

		//_pincmsg2("## brent_ixmax=%d", brent_ixmax);
	}

	if (!(r = NUMdvector(- nsamp_window, nsamp_window)) ||
	    !(imax = NUMlvector(1, maxnCandidates))) { 
		if (thee) 
			delete thee;
		goto end; 
	}

	//
	// Build pitch-frames
	//
	//_pincmsg2("   nFrames=%d", nFrames);
	//printf("    building pitch-frames: nFrames=%d\n", nFrames);
	for (iframe = 1; iframe <= nFrames; iframe++) {
		if (callback) {
			callback(progOffset + progChunk*((float)iframe/(float)nFrames), (char*)"\0");
		}
		//Pitch_Frame* pitchFrame = & thee->m_frame [iframe];
		Pitch_Frame* pitchFrame = thee->m_frame[iframe];
		//double t = Sampled_indexToX (thee, iframe), localPeak;
		double t = thee->indexToX(iframe);
		double localPeak;
		//long leftSample = Sampled_xToLowIndex (me, t), rightSample = leftSample + 1;
		long leftSample = snd->xToLowIndex(t);
		long rightSample = leftSample + 1;
		long startSample;
		long endSample;
		/*
		if (! Melder_progress4 (0.1 + (0.8 * iframe) / (nFrames + 1),
			L"Sound to Pitch: analysis of frame ", Melder_integer (iframe), L" out of ", Melder_integer (nFrames))) { forget (thee); goto end; }
			*/

		//printf("  ++++ iframe=%d: t=%f, leftSample=%d\n", iframe, t, leftSample);

		double localMean[1+MAXIMUM_NUMBER_OF_CHANNELS];
		for (channel = 1; channel <= snd->m_ny; channel++) {
			/*
			 * Compute the local mean; look one longest period to both sides.
			 */
			startSample = rightSample - nsamp_period;
			endSample = leftSample + nsamp_period;
			/*
			Melder_assert (startSample >= 1);
			Melder_assert (endSample <= snd->m_nx);
			*/
			if (startSample < 1 || endSample > snd->m_nx) {
				printf("[WARNING] #1 startSample=%ld, endSample=%ld\n", startSample, endSample);
				continue; // ??
			}
			//_pincmsg3("  ++++++++ #1 startSample=%d, endSample=%d", startSample, endSample);
			localMean[channel] = 0.0;
			for (i = startSample; i <= endSample; i ++) {
				//localMean[channel] += snd->m_z[channel][i];
				localMean[channel] += snd->getZ(channel, i);
			}
			localMean[channel] /= 2 * nsamp_period;

			/*
			 * Copy a window to a frame and subtract the local mean.
			 * We are going to kill the DC component before windowing.
			 */
			startSample = rightSample - halfnsamp_window;
			endSample = leftSample + halfnsamp_window;
			/*
			Melder_assert (startSample >= 1);
			Melder_assert (endSample <= snd->m_nx);
			*/
			if (startSample < 1 || endSample > snd->m_nx) {
				printf("[ERROR] #2 startSample=%ld, endSample=%ld\n", startSample, endSample);
				continue; // ??
			}
			//_pincmsg3("  ++++++++ #2 startSample=%d, endSample=%d", startSample, endSample);
			if (method < FCC_NORMAL) {
				for (j = 1, i = startSample; j <= nsamp_window; j++) {
					//frame[channel][j] = (snd->m_z[channel][i++] - localMean[channel]) * window[j];
					frame[channel][j] = (snd->getZ(channel, i++) - localMean[channel]) * window[j];
				}
				for (j = nsamp_window + 1; j <= nsampFFT; j++)
					frame[channel][j] = 0.0;
			} else {
				for (j = 1, i = startSample; j <= nsamp_window; j++) {
					//frame[channel][j] = snd->m_z[channel][i++] - localMean[channel];
					frame[channel][j] = snd->getZ(channel, i++) - localMean[channel];
				}
			}
		}

		/*
		 * Compute the local peak; look half a longest period to both sides.
		 */
		localPeak = 0.0;
		if ((startSample = halfnsamp_window + 1 - halfnsamp_period) < 1) 
			startSample = 1;
		if ((endSample = halfnsamp_window + halfnsamp_period) > nsamp_window) 
			endSample = nsamp_window;
		for (channel = 1; channel <= snd->m_ny; channel++) {
			for (j = startSample; j <= endSample; j ++) {
				double value = fabs(frame[channel][j]);
				if (value > localPeak) 
					localPeak = value;
			}
		}
		//printf("  ++++ localPeak=%f\n", localPeak);

		//
		// Assign pitchFrame->m_intensity
		//
		pitchFrame->m_intensity = localPeak > globalPeak ? 1.0 : localPeak / globalPeak;

		/*
		 * Compute the correlation into the array 'r'.
		 */
		if (method >= FCC_NORMAL) {
			double startTime = t - 0.5 * (1.0 / minimumPitch + dt_window);
			long localSpan = maximumLag + nsamp_window;
			long localMaximumLag, offset;

			if ((startSample = snd->xToLowIndex (startTime)) < 1) 
				startSample = 1;

			if (localSpan > snd->m_nx + 1 - startSample) 
				localSpan = snd->m_nx + 1 - startSample;
			localMaximumLag = localSpan - nsamp_window;
			offset = startSample - 1;
			double sumx2 = 0;   /* Sum of squares. */

			if (!snd->m_z) 
				printf("PRManipulation::Sound_to_Pitch_any - No snd->m_z!\n");

			for (channel = 1; channel <= snd->m_ny; channel++) {
				double *amp = snd->m_z[channel] + offset;
				for (i = 1; i <= nsamp_window; i ++) {
					double x = amp [i] - localMean[channel];
					sumx2 += x * x;
				}
			}
			double sumy2 = sumx2;   /* At zero lag, these are still equal. */
			r[0] = 1.0;
			for (i = 1; i <= localMaximumLag; i++) {
				double product = 0.0;
				for (channel = 1; channel <= snd->m_ny; channel++) {
					double *amp = snd->m_z[channel] + offset;
					double y0 = amp [i] - localMean[channel];
					double yZ = amp[i + nsamp_window] - localMean[channel];
					sumy2 += yZ * yZ - y0 * y0;
					for (j = 1; j <= nsamp_window; j ++) {
						double x = amp[j] - localMean[channel];
						double y = amp[i + j] - localMean[channel];
						product += x * y;
					}
				}
				r[-i] = r[i] = product / sqrt (sumx2 * sumy2);
			}
		} else {

			/*
			 * The FFT of the autocorrelation is the power spectrum.
			 */
			for (i = 1; i <= nsampFFT; i ++) {
				ac[i] = 0.0;
			}
			for (channel = 1; channel <= snd->m_ny; channel++) {
				NUMfft_forward(&fftTable, frame[channel]);   /* Complex spectrum. */
				ac [1] += frame[channel][1] * frame[channel][1];   /* DC component. */
				for (i = 2; i < nsampFFT; i += 2) {
					ac[i] += frame[channel][i] * frame[channel][i] + frame[channel][i+1] * frame[channel][i+1]; /* Power spectrum. */
				}
				ac[nsampFFT] += frame[channel][nsampFFT] * frame[channel][nsampFFT];   /* Nyquist frequency. */
			}
			NUMfft_backward(&fftTable, ac);   /* Autocorrelation. */

			/*
			 * Normalize the autocorrelation to the value with zero lag,
			 * and divide it by the normalized autocorrelation of the window.
			 */
			r[0] = 1.0;
			for (i = 1; i <= brent_ixmax; i++)
				r[- i] = r[i] = ac[i + 1] / (ac[1] * windowR[i + 1]);
		}

		/*
		 * Create (too much) space for candidates.
		 */
		//printf("    PRPitch::Frame_init: maxnCandidates=%d\n", maxnCandidates);
		PRPitch::Frame_init(pitchFrame, maxnCandidates);

		/*
		 * Register the first candidate, which is always present: voicelessness.
		 */
		pitchFrame->m_nCandidates = 1;
		pitchFrame->m_candidate[1]->m_frequency = 0.0;   /* Voiceless: always present. */
		pitchFrame->m_candidate[1]->m_strength = 0.0;

		/*
		 * Shortcut: absolute silence is always voiceless.
		 * Go to next frame.
		 */
		if (localPeak == 0) 
			continue;

		/*
		 * Find the strongest maxima of the correlation of this frame, 
		 * and register them as candidates.
		 */
		imax[1] = 0;
		for (i = 2; i < maximumLag && i < brent_ixmax; i++)
		    if (r[i] > 0.5 * voicingThreshold && /* Not too unvoiced? */
	 	        r[i] > r[i-1] && r[i] >= r[i+1])   /* Maximum? */
		{
			int place = 0;

			/*
			 * Use parabolic interpolation for first estimate of frequency,
			 * and sin(x)/x interpolation to compute the strength of this frequency.
			 */
			double dr = 0.5 * (r [i+1] - r [i-1]), d2r = 2 * r [i] - r [i-1] - r [i+1];
			double frequencyOfMaximum = 1 / snd->m_dx / (i + dr / d2r);
			long offset = - brent_ixmax - 1;
			double strengthOfMaximum = /* method & 1 ? */
				NUM_interpolate_sinc(r + offset, brent_ixmax - offset, 1 / snd->m_dx / frequencyOfMaximum - offset, 30)
				/* : r [i] + 0.5 * dr * dr / d2r */;
			/* High values due to short windows are to be reflected around 1. */
			if (strengthOfMaximum > 1.0) 
				strengthOfMaximum = 1.0 / strengthOfMaximum;

			/*
			_pincmsg4("  ++++ i=%d: frequencyOfMaximum=%f, strengthOfMaximum=%f",
						i, frequencyOfMaximum, strengthOfMaximum);
						*/

			/*
			 * Find a place for this maximum.
			 */
			if (pitchFrame->m_nCandidates < thee->m_maxnCandidates) { /* Is there still a free place? */
				place = ++pitchFrame->m_nCandidates;
			} else {
				/* Try the place of the weakest candidate so far. */
				double weakest = 2;
				for (int iweak = 2; iweak <= thee->m_maxnCandidates; iweak ++) {
					/* High frequencies are to be favoured */
					/* if we want to analyze a perfectly periodic signal correctly. */
					double localStrength = pitchFrame->m_candidate[iweak]->m_strength 
													- octaveCost * NUMlog2(minimumPitch / pitchFrame->m_candidate[iweak]->m_frequency);
					if (localStrength < weakest) { 
						weakest = localStrength; 
						place = iweak; 
					}
				}
				/* If this maximum is weaker than the weakest candidate so far, give it no place. */
				if (strengthOfMaximum - octaveCost * NUMlog2 (minimumPitch / frequencyOfMaximum) 
						<= weakest) {
					place = 0;
				}
			}
			//_pincmsg2("  ++++ place=%d", place);

			if (place) {   /* Have we found a place for this candidate? */
				pitchFrame->m_candidate[place]->m_frequency = frequencyOfMaximum;
				pitchFrame->m_candidate[place]->m_strength = strengthOfMaximum;
				imax [place] = i;
			}
		}

		/*
		 * Second pass: for extra precision, maximize sin(x)/x interpolation ('sinc').
		 */
		for (i = 2; i <= pitchFrame->m_nCandidates; i ++) {
			if (method != AC_HANNING || pitchFrame->m_candidate[i]->m_frequency > 0.0 / snd->m_dx) {
				double xmid, ymid;
				long offset = - brent_ixmax - 1;
				ymid = NUMimproveMaximum(r + offset, brent_ixmax - offset, imax [i] - offset,
					pitchFrame->m_candidate[i]->m_frequency > 0.3 / snd->m_dx ? NUM_PEAK_INTERPOLATE_SINC700 : brent_depth, & xmid);
				xmid += offset;
				pitchFrame->m_candidate[i]->m_frequency = 1.0 / snd->m_dx / xmid;
				if (ymid > 1.0) 
					ymid = 1.0 / ymid;
				pitchFrame->m_candidate[i]->m_strength = ymid;
			}
		}
	}   /* Next frame. */

	//Melder_progress1 (0.95, L"Sound to Pitch: path finder");
	//_pincmsg1("======== Calling pathFinder");
	//printf("    pathFinder\n");
	thee->pathFinder(silenceThreshold, 
							voicingThreshold,
							octaveCost, 
							octaveJumpCost, 
							voicedUnvoicedCost, 
							ceiling, 
							false);
							//octaveCost, octaveJumpCost, voicedUnvoicedCost, ceiling, Melder_debug == 31 ? true : false);

end:
	//Melder_progress1 (1.0, NULL);   /* Done. */
	NUMdmatrix_free (frame, 1, 1);
	NUMdvector_free (ac, 1);
	NUMdvector_free (window, 1);
	NUMdvector_free (windowR, 1);
	NUMdvector_free (r, - nsamp_window);
	NUMlvector_free (imax, 1);
	NUMfft_Table_free (& fftTable);
	//iferror forget (thee);
	//printf("Sound_to_Pitch_any - end\n");
	return thee;
}

PRPitch* PRManipulation::Sound_to_Pitch_ac(PRSound *snd,
														double dt, 
														double minimumPitch, 
														double periodsPerWindow, 
														int maxnCandidates, 
														int accurate, 
														double silenceThreshold, 
														double voicingThreshold, 
														double octaveCost, 
														double octaveJumpCost, 
														double voicedUnvoicedCost, 
														double ceiling,
														__progress_callback_t callback,
														float progOffset,
														float progChunk)
{
	return Sound_to_Pitch_any(snd, 
										dt, 
										minimumPitch, 
										periodsPerWindow, 
										maxnCandidates, 
										accurate, 
										silenceThreshold, 
										voicingThreshold, 
										octaveCost, 
										octaveJumpCost, 
										voicedUnvoicedCost, 
										ceiling,
										callback,
										progOffset,
										progChunk);
}

//
// NOT USING:
//
PRPitch* PRManipulation::Sound_to_Pitch_cc(PRSound *snd, 
														double dt, 
														double minimumPitch, 
														double periodsPerWindow, 
														int maxnCandidates, 
														int accurate, 
														double silenceThreshold, 
														double voicingThreshold, 
														double octaveCost, 
														double octaveJumpCost, 
														double voicedUnvoicedCost, 
														double ceiling)
{
	return Sound_to_Pitch_any(snd, 
										dt, 
										minimumPitch, 
										periodsPerWindow, 
										maxnCandidates, 
										2 + accurate, 
										silenceThreshold, 
										voicingThreshold, 
										octaveCost, 
										octaveJumpCost, 
										voicedUnvoicedCost, 
										ceiling,
										NULL, 0, 0);
}


//////////////////////////////////////////////////////////////////////////////////
//
//	From fon/Pitch_to_PointProcess.c
//
// all 'static'
//
/////////////////////////////////////////////////////////////////////////////////

double PRManipulation::findExtremum_3(double *channel1_base, double *channel2_base, 
												long d, long n, int includeMaxima, int includeMinima) 
{
	double *channel1 = channel1_base + d, *channel2 = channel2_base ? channel2_base + d : NULL;
	int includeAll = includeMaxima == includeMinima;
	long imin = 1, imax = 1, i, iextr;
	double minimum, maximum;
	if (n < 3) {
		if (n <= 0) 
			return 0.0;   /* Outside. */
		else if (n == 1) 
			return 1.0;
		else {   /* n == 2 */
			double x1 = channel2 ? 0.5 * (channel1[1] + channel2[1]) : channel1[1];
			double x2 = channel2 ? 0.5 * (channel1[2] + channel2[2]) : channel1[2];
			double xleft = includeAll ? fabs (x1) : includeMaxima ? x1 : - x1;
			double xright = includeAll ? fabs (x2) : includeMaxima ? x2 : - x2;
			if (xleft > xright) 
				return 1.0;
			else if (xleft < xright) 
				return 2.0;
			else 
				return 1.5;
		}
	}
	minimum = maximum = channel2 ? 0.5 * (channel1[1] + channel2[1]) : channel1[1];
	for (i = 2; i <= n; i ++) {
		double value = channel2 ? 0.5 * (channel1[i] + channel2[i]) : channel1[i];
		if (value < minimum) { 
			minimum = value; 
			imin = i; 
		}
		if (value > maximum) { 
			maximum = value; 
			imax = i; 
		}
	}
	if (minimum == maximum) {
		return 0.5 * (n + 1.0);   /* All equal. */
	}
	iextr = includeAll ? ( fabs (minimum) > fabs (maximum) ? imin : imax ) : includeMaxima ? imax : imin;
	if (iextr == 1) 
		return 1.0;
	if (iextr == n) 
		return (double) n;

	/* Parabolic interpolation. */
	/* We do NOT need fabs here: we look for a genuine extremum. */
	double valueMid = channel2 ? 0.5 * (channel1 [iextr] + channel2 [iextr]) : channel1 [iextr];
	double valueLeft = channel2 ? 0.5 * (channel1 [iextr - 1] + channel2 [iextr - 1]) : channel1 [iextr - 1];
	double valueRight = channel2 ? 0.5 * (channel1 [iextr + 1] + channel2 [iextr + 1]) : channel1 [iextr + 1];
	return iextr + 0.5 * (valueRight - valueLeft) / (2 * valueMid - valueLeft - valueRight);
}

double PRManipulation::Sound_findExtremum(PRSound *snd, double tmin, double tmax, int includeMaxima, int includeMinima) 
{
	long imin = snd->xToLowIndex(tmin);
	long imax = snd->xToHighIndex(tmax);
	double iextremum;
	/*
	Melder_assert (NUMdefined (tmin));
	Melder_assert (NUMdefined (tmax));
	*/
	if (imin < 1) 
		imin = 1;
	if (imax > snd->m_nx) 
		imax = snd->m_nx;

	if (!snd->m_z) 
		printf("PRManipulation::Sound_findExtremum - No snd->m_z!\n");

	iextremum = findExtremum_3(snd->m_z[1], snd->m_ny > 1 ? snd->m_z[2] : NULL, imin - 1, imax - imin + 1, includeMaxima, includeMinima);
	if (iextremum)
		return snd->m_x1 + (imin - 1 + iextremum - 1) * snd->m_dx;
	else
		return (tmin + tmax) / 2;
}

int PRManipulation::Pitch_getVoicedIntervalAfter(PRPitch* pitch, double after, double *tleft, double *tright) 
{
	long ileft = pitch->xToHighIndex(after);
	long iright;
	if (ileft > pitch->m_nx) 
		return 0;   /* Offright. */
	if (ileft < 1) 
		ileft = 1;   /* Offleft. */

	/* Search for first voiced frame. */
	for (; ileft <= pitch->m_nx; ileft ++)
		if (pitch->isVoiced_i(ileft)) 
			break;
	if (ileft > pitch->m_nx) 
		return 0;   /* Offright. */

	/* Search for last voiced frame. */
	for (iright = ileft; iright <= pitch->m_nx; iright ++)
		if (! pitch->isVoiced_i(iright)) break;
	iright --;

	*tleft = pitch->indexToX (ileft) - 0.5 * pitch->m_dx;   /* The whole frame is considered voiced. */
	*tright = pitch->indexToX (iright) + 0.5 * pitch->m_dx;
	if (*tleft >= pitch->m_xmax - 0.5 * pitch->m_dx) 
		return 0;
	if (*tleft < pitch->m_xmin) 
		*tleft = pitch->m_xmin;
	if (*tright > pitch->m_xmax) 
		*tright = pitch->m_xmax;
	return 1;
}

//
// The most time-consuming part (when voiced sample are the majority)
//
double PRManipulation::Sound_findMaximumCorrelation(PRSound* snd, 
																	double t1, 
																	double windowLength, 
																	double tmin2, 
																	double tmax2, 
																	double *tout, 
																	double *peak) 
{
	double maximumCorrelation = -1.0, r1 = 0.0, r2 = 0.0, r3 = 0.0, r1_best, r3_best, ir;
	double halfWindowLength = 0.5 * windowLength;
	long i1, i2, ileft2;
	long ileft1 = snd->xToNearestIndex(t1 - halfWindowLength);
	long iright1 = snd->xToNearestIndex(t1 + halfWindowLength);
	long ileft2min = snd->xToLowIndex(tmin2 - halfWindowLength);
	long ileft2max = snd->xToHighIndex(tmax2 - halfWindowLength);

	*peak = 0.0;   /* Default. */
	for (ileft2 = ileft2min; ileft2 <= ileft2max; ileft2++) {
		//
		// TWEAK!!!
		//	For faster process - trade-off against quality
		////////////////////////
		if (ileft2 % gCorStrideFactorL != 0)
			continue;
		////////////////////////
		double norm1 = 0.0, norm2 = 0.0, product = 0.0, localPeak = 0.0;
		if (snd->m_ny == 1) {
			for (i1 = ileft1, i2 = ileft2; i1 <= iright1; i1++, i2++) {
				//
				// TWEAK!!!
				//	For faster process - trade-off against quality
				////////////////////////
				if (i1 % gCorStrideFactorH != 0)
					continue;
				////////////////////////
				if (i1 < 1 || i1 > snd->m_nx || i2 < 1 || i2 > snd->m_nx) 
					continue;
				//double amp1 = snd->m_z[1][i1];
				double amp1 = snd->getZ(1, i1);
				//double amp2 = snd->m_z[1][i2];
				double amp2 = snd->getZ(1, i2);
				norm1 += amp1 * amp1;
				norm2 += amp2 * amp2;
				product += amp1 * amp2;
				if (fabs(amp2) > localPeak)
					localPeak = fabs(amp2);
			}
		} else {
			for (i1 = ileft1, i2 = ileft2; i1 <= iright1; i1 ++, i2 ++) {
				//
				// TWEAK!!!
				//	For faster process - trade-off against quality
				////////////////////////
				if (i1 % gCorStrideFactorH != 0)
					continue;
				////////////////////////

				if (i1 < 1 || i1 > snd->m_nx || i2 < 1 || i2 > snd->m_nx) 
					continue;
				//double amp1 = 0.5 * (snd->m_z[1][i1] + snd->m_z[2][i1]), amp2 = 0.5 * (snd->m_z[1][i2] + snd->m_z[2][i2]);
				double amp1 = 0.5 * (snd->getZ(1, i1) + snd->getZ(2,i1)), amp2 = 0.5 * (snd->getZ(1, i2) + snd->getZ(2, i2));
				norm1 += amp1 * amp1;
				norm2 += amp2 * amp2;
				product += amp1 * amp2;
				if (fabs(amp2) > localPeak)
					localPeak = fabs(amp2);
			}
		}
		r1 = r2;
		r2 = r3;
		r3 = product ? product / (sqrt(norm1 * norm2)) : 0.0;
		if (r2 > maximumCorrelation && r2 >= r1 && r2 >= r3) {
			r1_best = r1;
			maximumCorrelation = r2;
			r3_best = r3;
			ir = ileft2 - 1;
			*peak = localPeak;  
		}
	}
	/*
	 * Improve the result by means of parabolic interpolation.
	 */
	if (maximumCorrelation > -1.0) {
		double d2r = 2 * maximumCorrelation - r1_best - r3_best;
		if (d2r != 0.0) {
			double dr = 0.5 * (r3_best - r1_best);
			maximumCorrelation += 0.5 * dr * dr / d2r;
			ir += dr / d2r;
		}
		*tout = t1 + (ir - ileft1) * snd->m_dx;
	}
	return maximumCorrelation;
}

PRPointProcess* PRManipulation::Sound_Pitch_to_PointProcess_cc(
														PRSound* snd, 
														PRPitch* pitch,
														__progress_callback_t callback,
														float progOffset,
														float progChunk)
{
	//printf("Sound_Pitch_to_PointProcess_cc - start\n");
	double addedRight = -1e300;
	double peak;
	//printf("Sound_Pitch_to_PointProcess_cc - #0\n");
	double t = pitch->m_xmin;

	PRPointProcess* point = new PRPointProcess(snd->m_xmin, snd->m_xmax, 10);
	double globalPeak = snd->getAbsoluteExtremum(snd->m_xmin, snd->m_xmax, 0);
	
	/*
	 * Cycle over all voiced intervals.
	 */
	double prevT = t;
	if (callback)
		callback(progOffset + progChunk*(t/pitch->m_xmax), (char*)"\0");

	for (;;) {
		double tleft, tright, tmiddle, f0middle, tmax, tsave;
		//double temp_tleft, temp_tright;

		//printf("  t = %f\n", t);
		if (!PRManipulation::Pitch_getVoicedIntervalAfter(pitch, t, &tleft, &tright)) 
			break;
		/*
		if (tright - t < 1.5) {
			if (!PRManipulation::Pitch_getVoicedIntervalAfter(pitch, tright, &temp_tleft, &temp_tright)) 
				break;
			printf("  temp_tleft=%f, temp_tright=%f\n", temp_tleft, temp_tright);
			tright = temp_tright;
		}
		*/

		if (callback) {
			if (prevT < tright) { // Don't go back and forth
				callback(progOffset + progChunk*(tright/pitch->m_xmax), (char*)"\0");
				prevT = tright;
			}
		}
		/*
		 * Go to the middle of the voice stretch.
		 */
		tmiddle = (tleft + tright) / 2;
		//printf("  tmiddle=%f\n", tmiddle);
		/*
		if (! Melder_progress1 ((tmiddle - snd -> xmin) / (snd -> xmax - snd -> xmin), L"Sound & Pitch to PointProcess"))
			goto end;
			*/
		f0middle = pitch->getValueAtTime(tmiddle, kPitch_unit_HERTZ, Pitch_LINEAR);

		/*
		 * Our first point is near this middle.
		 */
		if (f0middle == NUMundefined) {
			printf("[WARNING] Sound_Pitch_to_PointProcess_cc: f0middle undefined!\n");
			return NULL;
		}
		tmax = PRManipulation::Sound_findExtremum(snd, tmiddle - 0.5 / f0middle, tmiddle + 0.5 / f0middle, TRUE, TRUE);
		//Melder_assert (NUMdefined (tmax));
		if (!point->addPoint(tmax)) {
			printf("[WARNING] Sound_Pitch_to_PointProcess_cc #1: tmax=%f\n", tmax);
			return NULL;
		}

		tsave = tmax;
		// 
		// TODO: auto-correlation
		//
		for (;;) {
			double f0 = pitch->getValueAtTime(tmax, kPitch_unit_HERTZ, Pitch_LINEAR);
			double correlation;
			if (f0 == NUMundefined) 
				break;
			correlation = PRManipulation::Sound_findMaximumCorrelation(snd, 
																					tmax, 
																					1.0 / f0, 
																					tmax - 1.25 / f0, 
																					tmax - 0.8 / f0, 
																					&tmax, 
																					&peak);
			if (correlation == -1) /*break*/ 
				tmax -= 1.0 / f0;   /* This one period will drop out. */
			if (tmax < tleft) {
				if (correlation > 0.7 && peak > 0.023333 * globalPeak && tmax - addedRight > 0.8 / f0)
					if (! point->addPoint(tmax)) {
						printf("[WARNING] Sound_Pitch_to_PointProcess_cc #2: tmax=%f\n", tmax);
						return NULL;
					}
				break;
			}
			if (correlation > 0.3 && (peak == 0.0 || peak > 0.01 * globalPeak)) {
				if (tmax - addedRight > 0.8 / f0)   /* Do not fill in a short originally unvoiced interval twice. */
					if (! point->addPoint(tmax)) {
						printf("[WARNING] Sound_Pitch_to_PointProcess_cc #3: tmax=%f\n", tmax);
						return NULL;
					}
			}
		}
		tmax = tsave;
		for (;;) {
			double f0 = pitch->getValueAtTime(tmax, kPitch_unit_HERTZ, Pitch_LINEAR);
			double correlation;
			if (f0 == NUMundefined) 
				break;
			correlation = PRManipulation::Sound_findMaximumCorrelation(snd, 
																				tmax, 1.0 / f0, 
																				tmax + 0.8 / f0, 
																				tmax + 1.25 / f0, 
																				&tmax, 
																				&peak);
			if (correlation == -1) /*break*/ 
				tmax += 1.0 / f0;
			if (tmax > tright) {
				if (correlation > 0.7 && peak > 0.023333 * globalPeak) {
					if (! point->addPoint(tmax)){
						printf("[WARNING] Sound_Pitch_to_PointProcess_cc #4: tmax=%f\n", tmax);
						return NULL;
					}
					addedRight = tmax;
				}
				break;
			}
			if (correlation > 0.3 && (peak == 0.0 || peak > 0.01 * globalPeak)) {
				if (!point->addPoint(tmax)) {
					printf("[WARNING] Sound_Pitch_to_PointProcess_cc #5: tmax=%f\n", tmax);
					return NULL;
				}
				addedRight = tmax;
			}
		}
		t = tright;
	}
	//Melder_progress1 (1.0, NULL);
	return point;
}

//////////////////////////////////////////////////////////////////////////////////
//
//	From fon/Pitch_to_PitchTier.c
//
// all 'static'
//
/////////////////////////////////////////////////////////////////////////////////

PRPitchTier* PRManipulation::Pitch_to_PitchTier(PRPitch* pitch) 
{
	//PRPitchTier* thee = PitchTier_create (my xmin, my xmax);
	PRPitchTier* thee = new PRPitchTier(pitch->m_xmin, pitch->m_xmax);   /* Same domain, for synchronization. */
	long i;
	if (!thee)  {
		//_pincmsg1("[ERROR] Pitch_to_PitchTier: No PitchTier!!");
		return NULL;
	}
	for (i = 1; i <= pitch->m_nx; i++) {
		double frequency = pitch->m_frame[i]->m_candidate[1]->m_frequency;

		/* Count only voiced frames.
		 */
		if (frequency > 0.0 && frequency < pitch->m_ceiling) {
			//double time = pitch->m_x1 + (i*pitch->m_dx);
			double time = pitch->indexToX(i);

			//if (!RealTier_addPoint (thee, time, frequency)) 
			if (!thee->addPoint(time, frequency)) {
				//_pincmsg1("[ERROR] Pitch_to_PitchTier: Couldn't add point!!");
				return NULL;
			}
		}
	}
	return thee;
}
	
