/*************************************************************************
 *  PRRealTier.h
 *
 *  Taegoo : Migration from fon/RealTier.h
 *
 *		- The superclass of 'PitchTier' , 'DurationTier'
 *
 **************************************************************************/

#ifndef __PRREAL_TIER_H__
#define __PRREAL_TIER_H__

#include "PRPointProcess.h"

typedef struct {
	double m_time;
	double m_value;
} RealPoint;

class PRRealTier
{
public:
	PRRealTier();
	PRRealTier(double tmin, double tmax);
	virtual ~PRRealTier();

	double m_xmin;		
	double m_xmax;		
	long m_setSize;
	RealPoint **m_points; // must be sorted all the time

	// inherited:
	/*
	long m_nx;		
	double m_dx;
	double m_x1;
	double m_ymin;	
	double m_ymax;	
	long m_ny;	
	double m_dy; 
	double m_y1;	
	double **m_z;	
	*/

	void init(double tmin, double tmax);
	int addPoint(double t, double value);
	void removeTail(double lenSec);
	PRRealTier* copyPart(double tmin, double tmax);

	double getValueAtTime(double t);
	/* Inside points: linear intrapolation. */
	/* Outside points: constant extrapolation. */
	/* No points: NUMundefined. */

	double getArea(double tmin, double tmax);

	long timeToLowIndex (double time);
	long timeToHighIndex(double time);
	double getMean_points(double tmin, double tmax) ;
	long getWindowPoints(double tmin, double tmax, long *imin, long *imax);

	static void appendRealTierToRealTier(PRRealTier *src, PRRealTier *dest);
#if 0
	double getValueAtIndex(long point);
	/* No points or 'point' out of range: NUMundefined. */

	double getMinimumValue();
	double getMaximumValue();
	double getMean_curve(double tmin, double tmax);
	double getStandardDeviation_curve(double tmin, double tmax);
	double getStandardDeviation_points(double tmin, double tmax);

	int interpolateQuadratically(long numberOfPointsPerParabola, int logarithmically);

	PRRealTier* Vector_to_RealTier(long channel);
	PRRealTier* Vector_to_RealTier_peaks(long channel);
	PRRealTier* Vector_to_RealTier_valleys(long channel);
	PRRealTier* PointProcess_upto_RealTier(PRPointProcess *pointProc, double value);

	void multiplyPart(double tmin, double tmax, double factor);
	void removePointsBelow(double level);
	/* TODO?:
	int formula(const wchar_t *expression, Interpreter interpreter, thou);
	*/
#endif

	void pointSet_create();
	int pointSet_compare(RealPoint* p1, RealPoint* p2);
	long pointSet_position(RealPoint* data);
	int pointSet_hasPoint(RealPoint* item);
	int pointSet_insertPoint(RealPoint* data, long pos);
	int pointSet_addPoint(RealPoint* data);
	void pointSet_removePoint(long pos);
	void pointSet_removeAllPoints();

private:
	// 
	// Methods to handle 'm_points'
	// Adopted from Collection.c
	//

	long m_setCapacity;

};

#endif
